def calculate_neighbors(x, y, side_length):
	neighbor_index = 0
	neighbors_array = []

	for j in range(-1,2):
		for i in range(-1,2):

			if j == 0 and i == 0:
				continue

			neighbors_array.append( to_id(torus(x+i, side_length), torus(y+j, side_length), side_length) )

	return neighbors_array

def torus(index, side):
    if index < 0:
        index += side
    
    elif index >= side:
        index -= side

    return index

# array of blood vessels' Place ids
def blood_vessels(side_length):

	blood_vessel_ids = [];
	grid_range = range(side_length)

	low_index = (side_length/2)/2
	high_index = grid_range[-(side_length/2)/2 - 1]

	blood_vessel_ids.append(to_id(low_index, low_index, side_length))
	blood_vessel_ids.append(to_id(low_index, high_index, side_length))
	blood_vessel_ids.append(to_id(high_index, high_index, side_length))
	blood_vessel_ids.append(to_id(high_index, low_index, side_length))

	return blood_vessel_ids

def bacteria_spawn_points(side_length):

	bacteria_spawn_ids = [];
	
	high_index = side_length/2;
	low_index = high_index-1;

	bacteria_spawn_ids.append(to_id(low_index, low_index, side_length))
	bacteria_spawn_ids.append(to_id(low_index, high_index, side_length))
	bacteria_spawn_ids.append(to_id(high_index, high_index, side_length))
	bacteria_spawn_ids.append(to_id(high_index, low_index, side_length))

	return bacteria_spawn_ids

def to_id(x,y,n):
	return x + (y * n)
				
