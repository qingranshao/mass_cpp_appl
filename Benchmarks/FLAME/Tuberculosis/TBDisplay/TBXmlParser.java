import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Sarah Panther
 * @since 8-19-19
 */

enum AgentType {PLACE, MACROPHAGE, TCELL, AGENTFACTORY, DAYTRACKER}

public class TBXmlParser {

    private Place[] places;

    public Place[] getPlaces() {
        return places;
    }

    TBXmlParser(int gridSize, File file) {
        try {
            NodeList nodes = fileToNodes(file);
            places = new Place[gridSize * gridSize];

            // instantiate all the Places, Macrophages, and T-cells
            for (int i = 0; i < nodes.getLength(); i++) {
                Element e = (Element) nodes.item(i);

                NodeList name = e.getElementsByTagName("name");
                Element line = (Element) name.item(0);
                String agentType = getCharDataFromElement(line);

                AgentType agent = AgentType.valueOf(agentType.toUpperCase());

                switch (agent) {
                    case PLACE:
                        int p_id = Integer.parseInt(elementToString(e, "p_id"));

                        boolean bacteria = Integer.parseInt(elementToString(
                                e, "bacteria")) == 1;

                        int chemokine = Integer.parseInt(
                                elementToString(e, "chemokine"));

                        boolean bloodVessel = Integer.parseInt(
                                elementToString(e, "blood_vessel")) == 1;

                        places[p_id] = new Place(p_id, bacteria, chemokine,
                                bloodVessel);
                        break;

                    case MACROPHAGE:
                        int m_id = Integer.parseInt(elementToString(e, "m_id"));

                        int m_x = Integer.parseInt(elementToString(e, "m_x"));

                        int m_y = Integer.parseInt(elementToString(e, "m_y"));

                        int state = Integer.parseInt(
                                elementToString(e, "state"));

                        int intracellularBact = Integer
                                .parseInt(elementToString(e,
                                        "intracellular_bacteria"));

                        Macrophage m = new Macrophage(m_id,
                                toPlaceID(m_x, m_y, gridSize), state,
                                intracellularBact);

                        places[m.getOnPlaceID()].setMacrophage(m);
                        break;

                    case TCELL:
                        int t_x = Integer.parseInt(elementToString(e, "t_x"));
                        int t_y = Integer.parseInt(elementToString(e, "t_y"));
                        places[toPlaceID(t_x, t_y, gridSize)]
                                .settCell(true);
                        break;

                    default:
                        break;
                }
            }
        } catch (ParserConfigurationException e) {
            System.out.println("Parser Configuration Exception in TBXmlParser");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("IO Exception in TBXmlParser");
            System.out.println(e.getMessage());
        } catch (SAXException e) {
            System.out.println("SAX Exception in TBXmlParser");
            System.out.println(e.getMessage());
        }
    }

    private NodeList fileToNodes(File f)
            throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new InputStreamReader(new FileInputStream(f)));

        Document doc = db.parse(is);
        return doc.getElementsByTagName("xagent");
    }

    private String getCharDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            return ((CharacterData) child).getData();
        } else {
            return "\nCannot get character data";
        }
    }

    private String elementToString(Element e, String tag) {
        NodeList n = e.getElementsByTagName(tag);
        return getCharDataFromElement((Element) n.item(0));
    }

    public static int toPlaceID(int x, int y, int gridSize) {
        return x + (y * gridSize);
    }

}
