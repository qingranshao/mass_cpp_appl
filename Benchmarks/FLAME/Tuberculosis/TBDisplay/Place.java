import java.util.Comparator;

/**
 * @author Sarah Panther
 * @since 8-19-19
 */

public class Place extends Agent implements Comparable<Agent>,
        Comparator<Agent> {

    private boolean bacteria;
    private int chemokine;
    private boolean bloodVessel;
    private Macrophage macrophage = null;
    private boolean tCell;

    Place(int p_id, boolean bacteria, int chemokine, boolean bloodVessel) {
        super(p_id);
        this.bacteria = bacteria;
        this.chemokine = chemokine;
        this.bloodVessel = bloodVessel;
    }

    public int getChemokine() {
        return chemokine;
    }

    public Macrophage getMacrophage() {
        return macrophage;
    }

    public void setMacrophage(Macrophage macrophage) {
        this.macrophage = macrophage;
    }

    public void settCell(boolean tCell) {
        this.tCell = tCell;
    }

    public boolean isBact() {
        return bacteria;
    }

    public boolean isMacro() {
        return macrophage != null;
    }

    public boolean isTCell() {
        return tCell;
    }

    public boolean isBVessel() {
        return bloodVessel;
    }

    public boolean isChemo() {
        return chemokine > 0;
    }

    @Override
    public String toString() {
        return bacteria + "\n" + chemokine + "\n" + bloodVessel + "\n"
                + macrophage + tCell;
    }

}
