#include "header.h"
#include "Place_agent_header.h"
#include "Neuron_agent_header.h"
#include "IterationTracker_agent_header.h"

#include "functions.h"

// Place functions

int approve_growth_requests() {
    bool granted = false;
    growth_request_message = get_first_growth_request_message();

    while (growth_request_message) {
        if (!granted && OCCUPYING_N_ID == DOES_NOT_EXIST) {
            add_growth_approval_message(P_ID, growth_request_message->from_n_id, growth_request_message->parent, TRUE,
                                        growth_request_message->part_type);
            granted = true;

            OCCUPYING_N_ID = growth_request_message->from_n_id;
            PART_TYPE = growth_request_message->part_type;
        }
            // a request has already been granted to this place - deny all others
        else {
            add_growth_approval_message(P_ID, growth_request_message->from_n_id, growth_request_message->parent, FALSE,
                                        growth_request_message->part_type);
        }
        growth_request_message = get_next_growth_request_message(growth_request_message);
    }
    return 0;
}

int occupied_post_state() {
    if (PART_TYPE == SYNAP || PART_TYPE == DENDRITE) {
        add_occupied_state_message(P_ID, OCCUPYING_N_ID, PART_TYPE);
    }
    return 0;
}

// Neuron functions

/**
 * Check the following cell parts and request them to grow as necessary:
 * - dendrites
 * - axon or synaptic terminals
 *
 * @return
 */
int request_to_grow() {
    // get the iteration number
    int iteration = -1;
    iteration_message = get_first_iteration_message();
    if (iteration_message) {
        iteration = iteration_message->this_iter;
    }

    request_axon_synapse_growth(iteration);
    request_dendrite_growth(iteration);

    return 0;
}

void request_axon_synapse_growth(int iter) {
    switch (THIS_AXON.axon_state) {
        case AXON_NOT_CREATED:
            if (iter >= THIS_AXON.axon_creation_time) {
                create_axon();
            }
            break;

        case CONTINUOUS_GROWTH:
            axon_continuous_growth();
            break;

        case SYNAPSE_GEN_AND_GROWTH:
            synapse_growth();
            break;

        case STOP_AXON_AND_SYNAPSE:
            return;

        default:
            printf("AXON ERROR: incorrect axon state during request_axon_synapse_growth()\n");
            return;
    }
    // add growth requests to message board
    for (int i = 0; i < THIS_AXON.axon_grow_requests.size; i++) {
        add_growth_request_message(THIS_AXON.axon_grow_requests.array[i].loc_req, N_ID,
                                   THIS_AXON.axon_grow_requests.array[i].parent,
                                   THIS_AXON.axon_grow_requests.array[i].n_part_type);
    }

    return;
}

void create_axon() {
    for (int i = 0; i < NUM_DIRECTIONS; i++) {
        if (AVAIL_DIR_AROUND_SOMA[i] == TRUE) {
            int dir = i;

            add_grow_req(&THIS_AXON.axon_grow_requests, directionToNeighbor(SOMA_PLACE, dir), dir, DOES_NOT_EXIST,
                         AXON, DOES_NOT_EXIST, FALSE);
            return;
        }
    }
}

void axon_continuous_growth() {
    // grow the growing ends in the same direction or +- 45 degrees
    for (int i = 0; i < THIS_AXON.growing_ends.size; i++) {
        int origin = THIS_AXON.growing_ends.array[i].end_loc;
        int newDir = new_growth_direction(THIS_AXON.growing_ends.array[i].grow_dir);
        int growReq = directionToNeighbor(origin, newDir);
        add_grow_req(&THIS_AXON.axon_grow_requests, growReq, newDir, origin, AXON, DOES_NOT_EXIST, FALSE);
    }
}

/**
 * Assumes all growing ends are synaptic terminals/ start points for synaptic terminals due to current Axon_State
 */
void synapse_growth() {
    for (int i = 0; i < THIS_AXON.growing_ends.size; i++) {
        int origin = THIS_AXON.growing_ends.array[i].end_loc;
        int oldDir = THIS_AXON.growing_ends.array[i].grow_dir;

        // request new branches if branches are less than R with a B% probability
        if (THIS_AXON.synap_branches < R) {
            ((rand() % 100 + 1) <= B) ? synapse_grow_request(true, origin, oldDir) :
            synapse_grow_request(false, origin, oldDir);
        }
            // just grow existing branch
        else {
            synapse_grow_request(false, origin, oldDir);
        }
    }
}

void synapse_grow_request(bool branch, int origin, int oldDir) {
    if (branch) {
        int newDir = new_branch_direction(oldDir);
        int growReq = directionToNeighbor(origin, newDir);

        add_grow_req(&THIS_AXON.axon_grow_requests, growReq, newDir, origin, SYNAP, DOES_NOT_EXIST, TRUE);

        // grow existing branch in the same direction - ensures growth of existing branch doesn't conflict with
        // new branch
        add_grow_req(&THIS_AXON.axon_grow_requests, directionToNeighbor(origin, oldDir), oldDir, origin,
                     SYNAP, DOES_NOT_EXIST, FALSE);
    }

        // just grow existing branch
    else {
        int newDir = new_growth_direction(oldDir);
        int growReq = directionToNeighbor(origin, newDir);

        add_grow_req(&THIS_AXON.axon_grow_requests, growReq, newDir, origin, SYNAP, DOES_NOT_EXIST, FALSE);
    }
}

/**
 * Takes the current growth direction and returns either the same direction, +45 degrees, or -45 degrees with an
 * equal probability for each
 *
 * @param dir
 * @return
 */
int new_growth_direction(int dir) {
    int randChange = rand() % 3 - 1;
    return fix_dir(dir + randChange);
}

int new_branch_direction(int dir) {
    int random = rand() % 2;
    int newDir = (random == 0) ? (dir - 1) : (dir + 1);
    return fix_dir(newDir);
}

/**
 * fixes direction to a valid cardinal direction (because NORTH = 0, NORTH_EAST = 1, etc, therefore -1 is NW, 8 is N,
 * , etc.)
 * @param newDir - direction that may be negative or > 7
 * @return newDir - integer direction fixed to 0 - 7 value
 */
int fix_dir(int newDir) {
    if (newDir < 0) {
        newDir += NUM_DIRECTIONS;
    } else if (newDir > NUM_DIRECTIONS - 1) {
        newDir -= NUM_DIRECTIONS;
    }
    return newDir;
}

void request_dendrite_growth(int iter) {
    // create dendrites as needed and grow existing dendrites
    for (int i = 0; i < DENDRITES.size; i++) {
        int state = DENDRITES.array[i].dendrite_state;

        switch (state) {

            case DENDRITE_NOT_CREATED:
                if (iter >= DENDRITES.array[i].dendrite_creation_time) {
                    create_dendrite(i);
                }
                break;

            case GROW:
                dendrite_growth(i);
                break;

            case STOP_DENDRITE:
                continue;

            default:
                printf("DENDRITE ERROR: incorrect dendrite state during request_dendrite_growth()\n");
                continue;
        }
    }
    // add growth request messages for this dendrite
    for (int j = 0; j < DENDR_GROW_REQUESTS.size; j++) {
        add_growth_request_message(DENDR_GROW_REQUESTS.array[j].loc_req, N_ID, DENDR_GROW_REQUESTS.array[j].parent,
                                   DENDRITE);
    }
}

void create_dendrite(int dendriteNum) {
    for (int i = 0; i < NUM_DIRECTIONS; i++) {
        if (AVAIL_DIR_AROUND_SOMA[i] == TRUE) {
            int dir = i;
            // dendrite doesn't branch when it first grows out of the soma
            add_grow_req(&DENDR_GROW_REQUESTS, directionToNeighbor(SOMA_PLACE, dir), dir, DOES_NOT_EXIST,
                         DENDRITE, dendriteNum, FALSE);
            return;
        }
    }
}

void dendrite_growth(int dendriteNum) {
    int branchesLeft = R - DENDRITES.array[dendriteNum].dendr_branches;

    for (int i = 0; i < DENDRITES.array[dendriteNum].growing_ends.size; i++) {
        int origin = DENDRITES.array[dendriteNum].growing_ends.array[i].end_loc;
        int dir = DENDRITES.array[dendriteNum].growing_ends.array[i].grow_dir;

        // request new branches if branches are less than R with a B% probability
        if (branchesLeft > 0) {
            if ((rand() % 100 + 1) <= B) {
                dendrite_grow_request(true, dendriteNum, origin, dir);
                branchesLeft--;
            } else {
                dendrite_grow_request(false, dendriteNum, origin, dir);
            }
        }
            // just grow existing branch
        else {
            dendrite_grow_request(false, dendriteNum, origin, dir);
        }
    }
    return;
}

/**
 * Note that dendrites don't change directions once they have branched off of the soma or an existing branch
 * @param branch
 * @param dendriteNum
 * @param origin
 * @param dir
 */
void dendrite_grow_request(bool branch, int dendriteNum, int origin, int dir) {
    if (branch) {
        int newDir = new_branch_direction(dir);
        int growReq = directionToNeighbor(origin, newDir);

        add_grow_req(&DENDR_GROW_REQUESTS, growReq, newDir, origin, DENDRITE, dendriteNum, TRUE);
    }
    // grow existing branch in the same direction - ensures growth of existing branch doesn't conflict with
    // new branch
    add_grow_req(&DENDR_GROW_REQUESTS, directionToNeighbor(origin, dir), dir, origin, DENDRITE, dendriteNum, FALSE);
    return;
}


/**
 * Gives the neighbor
 * @param originID
 * @param dir
 * @return
 */
int directionToNeighbor(int origin, int dir) {
    // from 1D Place index to col, row index
    int row = origin / S;
    int col = origin % S;

    switch (dir) {

        case NORTH:
            return torus(row - 1) * S + col;

        case NORTH_EAST:
            return torus(row - 1) * S + torus(col + 1);

        case EAST:
            return row * S + torus(col + 1);

        case SOUTH_EAST:
            return torus(row + 1) * S + torus(col + 1);

        case SOUTH:
            return torus(row + 1) * S + col;

        case SOUTH_WEST:
            return torus(row + 1) * S + torus(col - 1);

        case WEST:
            return row * S + torus(col - 1);

        case NORTH_WEST:
            return torus(row - 1) * S + torus(col - 1);

        default:
            return -1;
    }
}

/**
 * Grid of Places is a torus (left overflows to right, top overflows to bottom, etc.)
 *
 * helps calculate neighboring indices of Places
 *
 * @param rcIndex : corresponding row or column index (doesn't matter if row or column because simulation space is
 * a                  square)
 * @return toroidal rcIndex
 */
int torus(int rcIndex) {

    if (rcIndex < 0) {
        rcIndex += S;
    } else if (rcIndex >= S) {
        rcIndex -= S;
    }
    return rcIndex;
}

/**
 * Process growth approval messages and update Neuron parts (axon, synaptic terminals, dendrites) accordingly
 *
 * @return
 */
int grow() {
    bool axonalApproval = false;
    bool aDendriteApproval = false;

    growth_approval_message = get_first_growth_approval_message();
    // iterate over growth approval messages for this neuron
    while (growth_approval_message) {
        // Axon or synaptic terminal approval message

        int part_type = growth_approval_message->part_type;

        switch (part_type) {
            case AXON:
                // fall down to SYNAP - do the same thing for both

            case SYNAP:
                if (process_axonal_approval(growth_approval_message->this_p_id,
                                            growth_approval_message->parent, growth_approval_message->approval)) {
                    axonalApproval = true;
                }
                break;

            case DENDRITE:
                if (process_dendrite_approval(growth_approval_message->this_p_id,
                                              growth_approval_message->parent, growth_approval_message->approval)) {
                    aDendriteApproval = true;
                }
                break;

            default:
                printf("ERROR: Invalid neuron part type %d in grow()\n", part_type);
                break;
        }

        growth_approval_message = get_next_growth_approval_message(growth_approval_message);
    }

    if (axonalApproval) {
        change_axon_and_synap_state();
    }

    if (aDendriteApproval) {
        change_dendrites_state();
    }

    // reset grow request arrays for axon, synaptic terminal, and dendrite parts
    reset_grow_requests();

    // clear and compute new neighbors for growing ends
    compute_new_neighbors_all_g_ends();

    return 0;
}

bool process_axonal_approval(int p_id, int parent, int approval) {
    // look through this axon's growth requests and match up the approval with the request
    for (int i = 0; i < THIS_AXON.axon_grow_requests.size; i++) {

        if (p_id == THIS_AXON.axon_grow_requests.array[i].loc_req &&
            parent == THIS_AXON.axon_grow_requests.array[i].parent) {

            if (approval == TRUE) {
                approve_axon_growth(i);
                return true;
            }
            else {
                // mark this direction as unvailable as the request wasn't approved
                AVAIL_DIR_AROUND_SOMA[THIS_AXON.axon_grow_requests.array[i].grow_dir] = FALSE;
                return false;
            }
        }
    }
    return false;
}

void approve_axon_growth(int requestIndex) {
    int newLoc = THIS_AXON.axon_grow_requests.array[requestIndex].loc_req;
    int newDir = THIS_AXON.axon_grow_requests.array[requestIndex].grow_dir;

    int state = THIS_AXON.axon_state;
    int parent = THIS_AXON.axon_grow_requests.array[requestIndex].parent;

    switch (state) {
        // create the first location of the axon
        case AXON_NOT_CREATED:
            add_growing_end(&THIS_AXON.growing_ends, newLoc, newDir);
            add_int(&THIS_AXON.axon_occupied_places, newLoc);
            // mark this spot by the soma taken
            AVAIL_DIR_AROUND_SOMA[newDir] = FALSE;
            return;

        case CONTINUOUS_GROWTH:
            // remove parent growing end
            remove_parent_end(parent);
            add_growing_end(&THIS_AXON.growing_ends, newLoc, newDir);
            add_int(&THIS_AXON.axon_occupied_places, newLoc);
            return;

        case SYNAPSE_GEN_AND_GROWTH:
            if (THIS_AXON.axon_grow_requests.array[requestIndex].new_branch == TRUE) {
                add_growing_end(&THIS_AXON.growing_ends, newLoc, newDir);
                add_int(&THIS_AXON.synap_occupied_places, newLoc);
                THIS_AXON.synap_branches++;
            } else {
                remove_parent_end(parent);
                add_growing_end(&THIS_AXON.growing_ends, newLoc, newDir);
                add_int(&THIS_AXON.synap_occupied_places, newLoc);
            }
            return;

        case STOP_AXON_AND_SYNAPSE:
            printf("AXON ERROR: approved growth request while axon is in STOP state.\n");
            return;

        default:
            printf("AXON ERROR: invalid Axon state during grow() function.\n");
            return;
    }
}

void change_axon_and_synap_state() {
    int state = THIS_AXON.axon_state;

    switch (state) {
        case AXON_NOT_CREATED:
            if (THIS_AXON.growing_ends.size > 0) {
                THIS_AXON.axon_state = CONTINUOUS_GROWTH;
            }
            break;

        case CONTINUOUS_GROWTH:
            if (rand() % 100 + 1 > G) {
                THIS_AXON.axon_state = SYNAPSE_GEN_AND_GROWTH;
            }
            break;

        case SYNAPSE_GEN_AND_GROWTH:
            if (THIS_AXON.synap_branches >= R) {
                THIS_AXON.growth_after_last_branch++;
            }

            if (THIS_AXON.growth_after_last_branch >= K || rand() % 100 + 1 <= S) {
                THIS_AXON.axon_state = STOP_AXON_AND_SYNAPSE;
            }
            break;

        default:
            printf("AXON ERROR: erroneous state reached in change_axon_state() %d\n", THIS_AXON.axon_state);
            break;
    }
}

/**
 * Note that new branches don't remove their parents because the existing branch continues to grow - only the
 * continuing, existing branch removes its parent
 *
 * @param parent_p_id
 */
void remove_parent_end(int parent_p_id) {
    for (int i = 0; i < THIS_AXON.growing_ends.size; i++) {
        // this is the requestor who has been succeeded by a new growing end
        if (THIS_AXON.growing_ends.array[i].end_loc == parent_p_id) {
            remove_growing_end(&THIS_AXON.growing_ends, i);
            return;
        }
    }
}

bool process_dendrite_approval(int p_id, int parent, int approval) {
    // look through the dendrite's growth requests and match up the approval with the request
    for (int i = 0; i < DENDR_GROW_REQUESTS.size; i++) {

        if (p_id == DENDR_GROW_REQUESTS.array[i].loc_req &&
            parent == DENDR_GROW_REQUESTS.array[i].parent) {

            if (approval == TRUE) {
                approve_dendrite_growth(i);
                return true;
            }
            else {
                // mark this direction as unvailable as the request wasn't approved
                AVAIL_DIR_AROUND_SOMA[DENDR_GROW_REQUESTS.array[i].grow_dir] = FALSE;
                return false;
            }
        }
    }
    return false;
}

void approve_dendrite_growth(int requestIndex) {
    int dendrNum = DENDR_GROW_REQUESTS.array[requestIndex].dendrite_num;
    int newLoc = DENDR_GROW_REQUESTS.array[requestIndex].loc_req;
    int newDir = DENDR_GROW_REQUESTS.array[requestIndex].grow_dir;

    int state = DENDRITES.array[DENDR_GROW_REQUESTS.array[requestIndex].dendrite_num].dendrite_state;
    int parent = DENDR_GROW_REQUESTS.array[requestIndex].parent;

    switch (state) {
        // create the first location of this Dendrite (Dendrite number requestIndex in the DENDRITES array)
        case DENDRITE_NOT_CREATED:
            add_growing_end(&(DENDRITES.array[dendrNum].growing_ends), newLoc, newDir);
            add_int(&DENDRITES.array[dendrNum].dendrite_occupied_places, newLoc);
            // mark this spot by the soma taken
            AVAIL_DIR_AROUND_SOMA[newDir] = FALSE;
            return;

        case GROW:
            if (DENDR_GROW_REQUESTS.array[requestIndex].new_branch == TRUE) {
                add_growing_end(&(DENDRITES.array[dendrNum].growing_ends), newLoc, newDir);
                add_int(&DENDRITES.array[dendrNum].dendrite_occupied_places, newLoc);
                DENDRITES.array[dendrNum].dendr_branches++;
            } else {
                remove_parent_end(parent);
                add_growing_end(&(DENDRITES.array[dendrNum].growing_ends), newLoc, newDir);
                add_int(&DENDRITES.array[dendrNum].dendrite_occupied_places, newLoc);
            }
            return;

        case STOP_DENDRITE:
            printf("DENDRITE ERROR: approved growth request while dendrite is in STOP state.\n");
            return;

        default:
            printf("DENDRITE ERROR: invalid Dendrite state during grow() function.\n");
            return;
    }
}

void change_dendrites_state() {
    // iterate over all of the dendrites of this neuron
    for (int i = 0; i < DENDRITES.size; i++) {
        int state = DENDRITES.array[i].dendrite_state;

        switch (state) {
            case DENDRITE_NOT_CREATED:
                if (DENDRITES.array[i].growing_ends.size > 0) {
                    DENDRITES.array[i].dendrite_state = GROW;
                }
                break;

            case GROW:
                if (DENDRITES.array[i].dendr_branches >= R) {
                    DENDRITES.array[i].growth_after_last_branch++;
                }

                if (DENDRITES.array[i].growth_after_last_branch >= K || rand() % 100 + 1 <= S) {
                    DENDRITES.array[i].dendrite_state = STOP_DENDRITE;
                }
                break;

            case STOP_DENDRITE:
                break;

            default:
                printf("DENDRITE ERROR: erroneous state reached in change_dendrites_state() %d\n",
                       DENDRITES.array[i].dendrite_state);
                break;
        }
    }
}

void reset_grow_requests() {
    reset_grow_req_array(&THIS_AXON.axon_grow_requests);
    reset_grow_req_array(&DENDR_GROW_REQUESTS);
    return;
}

/**
 * Compute place id's of the neighbhos of the growing ends in their new location
 */
void compute_new_neighbors_all_g_ends() {
    reset_int_array(&NEW_SYNAP_NEIGHBORS);
    reset_int_array(&NEW_DENDRITE_NEIGHBORS);

    // compute new neighbors for the synaptic terminals growing ends
    if (THIS_AXON.axon_state == SYNAPSE_GEN_AND_GROWTH) {
        for (int i = 0; i < THIS_AXON.growing_ends.size; i++) {
            add_to_new_neighbors(THIS_AXON.growing_ends.array[i].end_loc, SYNAP);
        }
    }
    // compute new neighbors for the dendrite growing ends
    for (int i = 0; i < DENDRITES.size; i++) {
        if (DENDRITES.array[i].dendrite_state == GROW) {
            for (int j = 0; j < DENDRITES.array[i].growing_ends.size; j++) {
                add_to_new_neighbors(DENDRITES.array[i].growing_ends.array[j].end_loc, DENDRITE);
            }
        }
    }
    return;
}

/**
 * adds Moore's area neighbors for input placeId to the new_place_neighbors array
 *
 * @param placeId
 */
void add_to_new_neighbors(int placeId, Part_Type partType) {
    if (partType == SYNAP) {
        for (int i = 0; i < NUM_DIRECTIONS; i++) {
            add_int(&NEW_SYNAP_NEIGHBORS, directionToNeighbor(placeId, i));
        }
    } else if (partType == DENDRITE) {
        for (int i = 0; i < NUM_DIRECTIONS; i++) {
            add_int(&NEW_DENDRITE_NEIGHBORS, directionToNeighbor(placeId, i));
        }
    } else {
        printf("ERROR: incompatible type in add_to_new_neighbors() - TYPE %d\n", partType);
    }
    return;
}

/**
 * Note that messages neurons to which this neuron is already connected to are filtered out
 *
 * Input occupied_state_messages from Places filtered on the following conditions:
 * - don't include if the occupied Place is occupied by this Neuron
 *
 * - don't include messages from Places with synapses (part_type = 1 SYNAP) from Neurons that we are
 *   already connected to (in_connections)
 *
 * - don't include messages from Places with dendrites (part_type = 2 DENDRITE) from Neurons we are already connected
 *   to (out_connections)
 *
 * - only include messages from Places that are in our new_place_neighbors array
 *
 * @return
 */
int process_connections() {

    occupied_state_message = get_first_occupied_state_message();

    while (occupied_state_message) {
        int neuron = occupied_state_message->occupying_n_id;
        Part_Type type = occupied_state_message->n_part_type;

        switch (type) {
            case SYNAP :
                add_int(&IN_CONNECTIONS, neuron);
                add_new_connection_message(N_ID, neuron, neuron);
                break;

            case DENDRITE:
                add_int(&OUT_CONNECTIONS, neuron);
                add_new_connection_message(N_ID, neuron, N_ID);
                break;

            default:
                printf("ERROR: process_connections() function has incompatible type %d\n", type);
                break;
        }
        occupied_state_message = get_next_occupied_state_message(occupied_state_message);
    }
    return 0;
}

int receive_new_connections() {
    new_connection_message = get_first_new_connection_message();
    while (new_connection_message) {

        int neuron = new_connection_message->from_neuron;
        bool our_dendrite = new_connection_message->dendrite_belongs_to_n_id == N_ID;

        if (our_dendrite && !in_array(IN_CONNECTIONS.array, IN_CONNECTIONS.size, neuron)) {
            add_int(&IN_CONNECTIONS, neuron);
        } else if (!our_dendrite && !in_array(OUT_CONNECTIONS.array, OUT_CONNECTIONS.size, neuron)) {
            add_int(&OUT_CONNECTIONS, neuron);
        }
        new_connection_message = get_next_new_connection_message(new_connection_message);
    }
    return 0;
}

bool in_array(int array[], int size, int num) {
    bool alreadyThere = false;
    for (int i = 0; i < size; i++) {
        if (num == array[i]) {
            alreadyThere = true;
        }
    }
    return alreadyThere;
}

int process_and_transmit_signal() {
    double signal = 0;
    // alter the sum of the received signals from the past iteration according to this Neuron type
    switch (NEURON_TYPE) {

        case EXCITATORY:
            signal = SUM_OF_RECVD_SIGNALS + SUM_OF_RECVD_SIGNALS * M * .01;
            if (rand() % 100 + 1 <= A) {
                signal += 1.0;
            }
            break;

        case INHIBITORY:
            signal = SUM_OF_RECVD_SIGNALS * M * .01;
            break;

        case NEUTRAL:
            signal = SUM_OF_RECVD_SIGNALS;
            break;

        default:
            printf("ERROR : invalid type in process_and_transmit_signal() of type %d\n", NEURON_TYPE);
            break;
    }
    // send signal
    if (signal >= T) {
        add_signal_message(N_ID, signal);
    }

    return 0;
}

int receive_signals() {
    // clear the signals received from the last iteration
    SUM_OF_RECVD_SIGNALS = 0.0;

    signal_message = get_first_signal_message();
    while (signal_message) {
        // add the receive signals to the total
        SUM_OF_RECVD_SIGNALS += signal_message->signal;

        signal_message = get_next_signal_message(signal_message);
    }
    return 0;
}

// Iteration Tracker functions

int post_iteration() {
    add_iteration_message(ITERATION++);
    return 0;
}
