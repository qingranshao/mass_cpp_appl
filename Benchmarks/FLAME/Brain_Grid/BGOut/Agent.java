import java.util.Comparator;

/**
 * @author Sarah Panther
 * @since 11-19-19
 */

public class Agent implements Comparator<Agent>, Comparable<Agent> {
    private int id;

    Agent() {
        id = -1;
    }

    Agent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Agent a) {
        return Integer.compare(this.getId(), a.getId());
    }

    @Override
    public int compare(Agent a1, Agent a2) {
        return a1.getId() - a2.getId();
    }
}
