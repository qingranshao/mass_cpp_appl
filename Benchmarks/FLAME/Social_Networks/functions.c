
#include "header.h"
#include "Person_agent_header.h"

#include "functions.h"

/**
 * author : Sarah Panther
 * since  : September 22nd, 2019
 *
 * CSS 499 - MASS C++ Benchmarking and Comparison
 *
 * Social Networks simulation function files
 */

/**
 * Posts this Person's social network that they are currently aware of to the message board using a friend message.
 * This message will be used by this Person's neighbors to enlarge their own social networks by one degree in the
 * next state function find_next_degree_friends()
 *
 * @return 0 - all Person agents remain in the simulation
 */
int post_found_friends() {

    // Each found friend message can hold FRIEND_MESSAGE_SIZE number of friend id's
    int friendMessage[FRIEND_MESSAGE_SIZE];
    int messageIndex = 0;
    int networkIndex = 0;
    while (networkIndex < SOCIAL_NETWORK.size) {
        if (messageIndex < FRIEND_MESSAGE_SIZE) {
            friendMessage[messageIndex++] = SOCIAL_NETWORK.array[networkIndex++].friend_id;
        }
        else {
            add_found_friends_message(ID, CURR_DEGREE, friendMessage);
            messageIndex = 0;
        }
    }
    while (messageIndex % FRIEND_MESSAGE_SIZE != 0){
        friendMessage[messageIndex++] = -1;
    }
    add_found_friends_message(ID, CURR_DEGREE, friendMessage);
    return 0;
}

/**
 * Examine this Person's first degree friends' (direct neighbors) social networks to add one more degree of friends to
 * this Person's social network
 *
 * Note that the found_friend messages are filtered to only include this Person's first degree friends
 *
 * @return 0 if this Person hasn't found the user input degree of connectedness in their social network
 *         1 if this Person has a completed social network to the user input degree (remove them from the simulation)
 */
int find_next_degree_friends() {
    CURR_DEGREE++;
    found_friends_message = get_first_found_friends_message();
    while (found_friends_message){
        for (int i = 0; i < FRIEND_MESSAGE_SIZE; i++) {
            int newFriend = found_friends_message->friends[i];
            if (newFriend != ID && newFriend != -1 && !alreadyFriends(newFriend)) {
                add_connection(&SOCIAL_NETWORK, newFriend, CURR_DEGREE);
            }
        }
        found_friends_message = get_next_found_friends_message(found_friends_message);
    }
    if (CURR_DEGREE == DEGREE) {  // print output and exit simulation if needed degree is found
        printOutput();
        return 1;
    }
    else {
        return 0;  // remain in the simulation
    }
}

/**
 * Checks to see if this id is already in this Person's social network array
 * @param id
 * @return true if this Person already has that id in their social network
 *         false if they don't have that id in their social network
 */
bool alreadyFriends(int id) {
    for (int i = 0; i < SOCIAL_NETWORK.size; i++) {
        if (id == SOCIAL_NETWORK.array[i].friend_id) {
            return true;
        }
    }
    return false;
}

/**
 * Prints the current Person agent's friends to the user input degree to the command line
 */
void printOutput() {
    printf("Person agent %d's friends\n", ID);
    int index = 0;
    for (int i = 1; i <= CURR_DEGREE; i++) {
        printf("%15d's degree %5d :", ID, i);
        while(SOCIAL_NETWORK.array[index].degree == i) {
            printf(" %d", SOCIAL_NETWORK.array[index++].friend_id);
        }
        printf("\n");
    }

}
