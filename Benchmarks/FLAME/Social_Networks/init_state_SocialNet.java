import java.io.*;

/**
 * author : Sarah Panther
 * since  : September 23rd, 2019
 *
 * CSS 499 - MASS C++ Benchmarking and Comparison
 *
 * Social Networks simulation - 0.xml initialization program
 *
 * Please see the Social Networks Simulation Specification for details.
 */

public class init_state_SocialNet {

    private static int numPeople;
    private static int degree;
    private static int kFirstDegree;
    private static int[][] adjacencyMatrix;

    public static void main(String[] args) {
        checkInput(args);
        createRegularGraph();
        writeToXML();
    }

    private static void checkInput(String[] args) {
        if (args.length != 4) {
            System.out.println(
                    "Usage: java init_state_SocialNet (n = total " +
                            "number of Person agents in simulation) " +
                            "(degree of friendship to find) (k or m) (k = " +
                            "number of first degree friends each Person agent" +
                            " has OR m = fraction of n where each Person has " +
                            "n / m first degree friends)");

            System.out.println("**Note**: \nWhen specifying k and n - at " +
                    "least one of those variables must be EVEN. " +
                    "\nWhen specifying n and m - if n is ODD ( n / m ) must " +
                    "be an integer and be EVEN ");

            System.exit(1);
        }

        numPeople = Integer.parseInt(args[0]);

        degree = Integer.parseInt(args[1]);

        String kOrM = args[2];

        if (kOrM.toUpperCase().equals("K")) {
            kFirstDegree = Integer.parseInt(args[3]);

            if (kFirstDegree >= numPeople) {
                System.out.println("ERROR: k must be less than n.\nExiting " +
                        "now");
                System.exit(-1);
            }

            // check that either k or n is even
            else if (kFirstDegree % 2 != 0) {
                if (numPeople % 2 != 0) {
                    System.out.println("ERROR: When specifying k and n - at " +
                            "least one of those variables must be EVEN. " +
                            "\nExiting now");
                    System.exit(-1);
                }
            }
        } else {
            int m = Integer.parseInt(args[3]);

            if (!((numPeople % m) == 0 &&
                    ((numPeople % 2) == 0 || (numPeople / m) % 2 == 0))) {
                System.out.println("ERROR: When specifying m and n, at " +
                        "least one of the following variables must be even " +
                        "and both must be an integer: n, n/m.\nExiting now");
                System.exit(-1);
            } else {
                kFirstDegree = numPeople / m;
            }
        }
    }

    /**
     * Creates an adjacency matrix for a k-regular graph for the input number
     * of people
     */
    private static void createRegularGraph() {
        adjacencyMatrix = new int[numPeople][numPeople];
        boolean kEven = kFirstDegree % 2 == 0;

        for (int i = 0; i < numPeople; i++) {
            // Give everyone kFirstDegree number of first degree friends
            for (int j = i - kFirstDegree / 2; j <= i + kFirstDegree / 2; j++) {
                // A Person isn't friends with themselves
                if (i == j) {
                    continue;
                }
                // Each Person is friends with the k/2 nearest neighbors on
                // either side if we put all the People in a circle (circular
                // array)
                adjacencyMatrix[i][toCircleI(j)] = 1;
            }
            // if k is odd, each Person is also friends with the Person
            // across from them in the circular array
            if (!kEven) {
                adjacencyMatrix[i][toCircleI(i + (numPeople / 2))] = 1;
            }
        }
    }

    /**
     * Converts the parameter index to the corresponding circular array index
     * @param index - index to be converted
     * @return corresponding circular array index
     */
    private static int toCircleI(int index) {
        int circleIndex = index % numPeople;
        if (circleIndex < 0) {
            circleIndex += numPeople;
        }
        return circleIndex;
    }

    /**
     * Writes the starting parameters of the simulation based on the user
     * input to a file in the same directory named "0.xml"
     */
    private static void writeToXML() {
        File file = new File("./0.xml");
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.write("<states>\n");
            pw.write("<itno>0</itno>\n");

            // write environmental constants
            pw.write("<environment>\n");
            pw.write("<network_size>" + numPeople +"</network_size>\n");
            pw.write("<k>" + kFirstDegree +"</k>\n");
            pw.write("<degree>" + degree +"</degree>\n");
            pw.write("</environment>");

            // write Person agents
            for (int i = 0; i < numPeople; i++) {
                pw.write("\t<xagent>\n");

                pw.write("\t\t<name>Person</name>\n");
                pw.write("\t\t<id>" + i + "</id>\n");
                write1stDegFriends(pw, i);
                writeSocialNet(pw, i);
                pw.write("\t\t<curr_degree>1</curr_degree>\n");
                pw.write("\t</xagent>\n");
            }
            pw.write("</states>\n");
            pw.flush();
            pw.close();
        } catch (IOException exc) {
            System.out.println("IO Exception: " + exc.getMessage());
        }
    }

    /**
     * Writes the first degree friends of this Person agent to the 0.xml file
     * @param pw - current print writer writing to 0.xml
     * @param i -  current Person agent we are operating on in the adjacency
     *             matrix
     */
    public static void write1stDegFriends(PrintWriter pw, int i) {
        pw.write("\t\t<first_degree_friends>{");
        boolean firstInList = true;
        for (int j = 0; j < numPeople; j++) {
            if (adjacencyMatrix[i][j] == 1) {
                if (!firstInList) {
                    pw.write(",");
                }
                pw.write(" " + j);
                firstInList = false;
            }
        }
        pw.write("}</first_degree_friends>\n");
    }

    /**
     * Writes the generated first degree friends to social_network dynamic
     * array consisting of the data type "connection" (this data type is
     * defined in the XML)
     * @param pw - current print writer writing to the file 0.xml
     * @param i - current agent in the adjacency matrix we are operating on
     */
    public static void writeSocialNet(PrintWriter pw, int i) {
        pw.write("\t\t<social_network>{");
        boolean firstInList = true;
        for (int j = 0; j < numPeople; j++) {
            if (adjacencyMatrix[i][j] == 1) {
                if (!firstInList) {
                    pw.write(",");
                }
                pw.write(" { " + j + ", 1}");
                firstInList = false;
            }
        }
        pw.write("}</social_network>\n");
    }
}
