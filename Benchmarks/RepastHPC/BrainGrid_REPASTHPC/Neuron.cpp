

#include "Neuron.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"


RepastHPCAgent::RepastHPCAgent(repast::AgentId id): id_(id){ }

RepastHPCAgent::~RepastHPCAgent(){ }


void RepastHPCAgent::set(int currentRank){
    id_.currentRank(currentRank);
    
    for (int i = 0; i < 8; i++) {
       neighborStageStatus[i] = -1;
       neighborTypeStatus[i] = -1;
       neighborSynapseStatus[i] = -1;
       neighborIncomingDirection[i] = -1;
    }

    currStage = 0;
    incomingDirection = -1;


    //0 = ACTIVE, 1 = INACTIVE, 2 = NEUTRAL
    int weight = rand() % 100;
    if (weight < 10) {
       ntype = 0;
    }
    else if (weight < 20) {
       ntype = 1;
    }
    else { 
       ntype = 3;
    }

    if (ntype == 0) {
       stage = 0;
       neuronSynapseNum = getId().id();
    }
    else {
       stage = 6;
       neuronSynapseNum = -1;
    }
}


void RepastHPCAgent::play(repast::SharedContext<RepastHPCAgent>* context,
                              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space){
    std::vector<RepastHPCAgent*> agentsToPlay;
    
    std::vector<int> agentLoc;
    space->getLocation(id_, agentLoc);
    repast::Point<int> center(agentLoc);
    repast::Moore2DGridQuery<RepastHPCAgent> moore2DQuery(space);
    moore2DQuery.query(center, 1, false, agentsToPlay);
    
    
    std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
    while(agentToPlay != agentsToPlay.end()){
       
       std::vector<int> otherLoc;
        space->getLocation((*agentToPlay)->getId(), otherLoc);
        repast::Point<int> otherPoint(otherLoc);

        int agentNum = (*agentToPlay)->getId().id() % 8;
        neighborStageStatus[agentNum] = (*agentToPlay)->getStage();
        neighborTypeStatus[agentNum] = (*agentToPlay)->getType();
        neighborIncomingDirection[agentNum] = (*agentToPlay)->getIncomingDirection();
        neighborSynapseStatus[agentNum] = (*agentToPlay)->getNeuronSynapseNum();

		
        agentToPlay++;

    }

    int direction = -1;
    int randStartDir = rand() % 8;
    for (int i = 0; i < 8; i++) {
       if (neighborSynapseStatus[(randStartDir + i) % 8] != -1) {
          if ((neighborTypeStatus[(randStartDir + i) % 8] == 3 && neighborIncomingDirection[(randStartDir + i) % 8] == (randStartDir + i) % 8) || neighborTypeStatus[(randStartDir + i) % 8] == 0) {
             incomingDirection = (randStartDir + i) % 8;
             direction = (randStartDir + i) % 8;
             break;
          }
       }
    }

    if (direction != -1) {
       if (ntype == 0) {
          //growActive
          if (stage != 3) {
             neuronSynapseNum = neighborSynapseStatus[direction];
             stage = 3;
          }
       }
       else if (ntype == 3) {
          //growNeutral
          //set to Stage of Neutral to currStage
          if (stage == 6) {
             neuronSynapseNum = neighborSynapseStatus[direction];
             stage = 3;
          }
          else {

          }
       }
       else { //INACTIVE (inhibitory)
              //stop growth.
          if (stage == 6) {
             neuronSynapseNum = neighborSynapseStatus[direction];
             stage = 5;
          }
          else {

          }
       }
    }

    std::cout << " AGENT " << id_ << " status: " << stage << std::endl;
	
}




/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }


RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _ntype, int _stage, bool _currStage, int _incomingDirection, int _neuronSynapseNum) :
id(_id), rank(_rank), type(_type), currentRank(_currentRank), ntype(_ntype), stage(_stage), currStage(_currStage), incomingDirection(_incomingDirection), neuronSynapseNum(_neuronSynapseNum) { }
