

#ifndef NEURON
#define NEURON

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"


/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    static const int neighbor[8][2];								//Array form of cardinals
    //enum eType { ACTIVE = 0, INACTIVE, NEUTRAL };
    int ntype; //ACTIVE = 0, INACTIVE = 1, NEUTRAL = 3
    //enum Stage { VISIT1 = 0, VISIT2, ENACTED, CONNECTED, STOPPED, INDEF };
    int stage; //VISIT1 = 0, VISIT2 = 1, ENACTED = 3, CONNECTED = 4, STOPPED = 5, INDEF = 6
    //Stage stage;
    int neighborStageStatus[8];											//An array to store int health status of neighbors
    int neighborTypeStatus[8];											//An array to store int health status of neighbors
    int neighborSynapseStatus[8];
    int neighborIncomingDirection[8];
    bool currStage;
    int incomingDirection;
    int neuronSynapseNum;
	
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}
	
    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }
	
    /* Getters specific to this kind of Agent */

    int getType() { return ntype; }
    int getStage() { return stage; }
    bool getCurrStage() { return currStage; }
    int getIncomingDirection() { return incomingDirection; }
    int getNeuronSynapseNum() { return neuronSynapseNum; }

	
    /* Setter */
    void set(int currentRank);
	
    /* Actions */
    void play(repast::SharedContext<RepastHPCAgent>* context,
              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);    // Choose three other agents from the given context and see if they cooperate or not
  
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type; 
    int    currentRank;
    //enum eType { ACTIVE = 0, INACTIVE, NEUTRAL };
    int ntype; //ACTIVE = 0, INACTIVE = 1, NEUTRAL = 3
    //enum Stage { VISIT1 = 0, VISIT2, ENACTED, CONNECTED, STOPPED, INDEF };
    int stage; //VISIT1 = 0, VISIT2 = 1, ENACTED = 3, CONNECTED = 4, STOPPED = 5, INDEF = 6
    //Stage stage;
    int neighborStageStatus[8];											//An array to store int health status of neighbors
    int neighborTypeStatus[8];											//An array to store int health status of neighbors
    int neighborSynapseStatus[8];
    int neighborIncomingDirection[8];
    bool currStage;
    int incomingDirection;
    int neuronSynapseNum;
	
    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _ntype, int _stage, bool _currStage, int _incomingDirection, int _neuronSynapseNum);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        //ar & c;
        ar & ntype;
        ar & stage;
        ar & currStage;
        ar & incomingDirection;
        ar & neuronSynapseNum;
    }
	
};


#endif