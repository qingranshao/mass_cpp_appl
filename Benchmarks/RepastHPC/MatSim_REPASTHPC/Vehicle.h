

#ifndef VEHICLE
#define VEHICLE

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include <string.h>


/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    int destination;

    int movementCounter;
	
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}
    RepastHPCAgent(repast::AgentId id, int destination);
	
    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }


    /* Getters specific to this kind of Agent */
    int  getDestination() { return destination; }
    int  getMovementCounter() { return movementCounter; }


	
    /* Setter */
    void set(int currentRank, int newDestination);
	
    /* Actions */
    void play(repast::SharedContext<RepastHPCAgent>* context,
       repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);    // Choose three other agents from the given context and see if they cooperate or not
    void move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type;
    int    currentRank;
    int destination;
    int movementCounter;

	
    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _destination, int _movementCounter);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & destination;
        ar & movementCounter;
    }
	
};


#endif