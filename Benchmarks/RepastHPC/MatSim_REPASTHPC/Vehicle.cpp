

#include "Vehicle.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"
#include <stdlib.h>  //rand
#include <sstream>     // ostringstream
#include <fstream>
#include <set>
#include <algorithm>
#include <stack>
#include <limits.h>
#include <cstdint>

RepastHPCAgent::RepastHPCAgent(repast::AgentId id): id_(id), destination(3){ }

RepastHPCAgent::RepastHPCAgent(repast::AgentId id, int newDestination): id_(id), destination(newDestination){ }

RepastHPCAgent::~RepastHPCAgent(){ }




void RepastHPCAgent::set(int currentRank, int newDestination){
    id_.currentRank(currentRank);
    movementCounter = 0;

    srand(time(NULL));
    int xDestination = rand() % 30;
    int yDestination = rand() % 30;
    destination = (30 - 1) * yDestination + yDestination + xDestination + 1;

}


void RepastHPCAgent::play(repast::SharedContext<RepastHPCAgent>* context,
                              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space){
   std::vector<RepastHPCAgent*> agentsToPlay;

   movementCounter++;
	
}


void RepastHPCAgent::move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space) {
   

   vector<int> agentLoc;
   space->getLocation(id_, agentLoc);

   int size = 900; //size of the grid (30x30)

   int adjGraph[901][901];
   srand(time(NULL));

   for (int n = 1; n <= size; n++)
   {
      for (int m = 1; m <= size; m++)
      {
         if (n == m) {
            adjGraph[n][m] = 0;
         }
         else {
            if (rand() % 2 == 1) {
               adjGraph[n][m] = rand() % 10;
            }
            else {
               adjGraph[n][m] = -1;
            }
         }
      }
   }

   int source = 29 * agentLoc[1] + agentLoc[1] + agentLoc[0] + 1;


   bool containsVertex;
   std::set<int> setOfVertexes;

   int dist[size];
   int prev[size];

   int u = -1;

   for (int i = 0; i < size; i++) {
      dist[i] = INT_MAX;
      prev[i] = -1;
      setOfVertexes.insert(i);
   }


   dist[source] = 0;


   while (!setOfVertexes.empty()) {
      int smallest = INT_MAX;
      for (auto itr = setOfVertexes.begin(); itr != setOfVertexes.end(); ++itr) {
         if (dist[*itr] <= smallest) {
            smallest = dist[*itr];
            u = *itr;
         }
      }
      setOfVertexes.erase(u);


      int alt = 0;
      for (int i = 0; i < size; i++) {
         if (adjGraph[u][i] != -1) {
            containsVertex = setOfVertexes.find(i) != setOfVertexes.end();
            if (containsVertex) {
               alt = dist[u] + adjGraph[u][i];
               if (alt < dist[i]) {
                  dist[i] = alt;
                  prev[i] = u;
               }
            }
         }
      }
   }

   stack<int> shortestPathStack;

   u = destination;
   while (prev[u] != -1) {
      shortestPathStack.push(u);
      u = prev[u];
   }
   shortestPathStack.push(u);
   int stackSize = shortestPathStack.size();
   shortestPathStack.push(stackSize);


   int shortestPath[shortestPathStack.size()];


   int counter = 0;
   while (!shortestPathStack.empty()) {

      shortestPath[counter] = shortestPathStack.top();
      counter++;

      shortestPathStack.pop();
   }

   vector<int> shortestPathFinal;
   for (int i = 0; i < shortestPath[0]; i++) { 
      shortestPathFinal.push_back(shortestPath[i + 1]);
   }


   int idNum;
   int x;
   int y;

   for (int i = 0; i < shortestPathFinal.size() - 1; i++) {
      // move agent to new location
      vector<int> dest;
      idNum = shortestPathFinal.at(i);
      x = (idNum - 1) % 900;
      y = (idNum - 1) / 900;


      dest.push_back(x);
      dest.push_back(y);

      space->moveTo(id_, dest);
      std::cout << "car" << getId().id() << " has moved to: " << idNum << std::endl;
   }
}

/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }

RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _destination, int _movementCounter) :
id(_id), rank(_rank), type(_type), currentRank(_currentRank), destination(_destination), movementCounter(_movementCounter) { }
