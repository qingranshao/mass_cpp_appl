#ifndef SNETWORK
#define SNETWORK

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include <unordered_set>
#include <vector>

/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    vector<int> totalFriends;
    int friendGroup;
	
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}
    RepastHPCAgent(repast::AgentId id, double newC, double newTotal);
	
    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }
	
    /* Getters specific to this kind of Agent */
    vector<int> getTotalFriends() { return totalFriends; }
	
    int getFriendGroup() { return friendGroup; }

    /* Setter */
    void set(int currentRank);
	
    /* Actions */
    void play(repast::SharedNetwork<RepastHPCAgent,
              repast::RepastEdge<RepastHPCAgent>,
              repast::RepastEdgeContent<RepastHPCAgent>,
              repast::RepastEdgeContentManager<RepastHPCAgent> > *network);
	

    void playAgain(repast::SharedNetwork<RepastHPCAgent,
       repast::RepastEdge<RepastHPCAgent>,
       repast::RepastEdgeContent<RepastHPCAgent>,
       repast::RepastEdgeContentManager<RepastHPCAgent> > *network, int degreesOfFreedom);
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type;
    int    currentRank;
    int    friendGroup;
    int    idNum;
	
    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & friendGroup;
        ar & idNum;
    }
	
};


#endif