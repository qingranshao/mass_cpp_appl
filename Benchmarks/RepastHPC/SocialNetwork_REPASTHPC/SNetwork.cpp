#include "SNetwork.h"
#include <unordered_set>
#include <vector>
#include <fstream>
#include <stdlib.h>

RepastHPCAgent::RepastHPCAgent(repast::AgentId id): id_(id){ }

RepastHPCAgent::RepastHPCAgent(repast::AgentId id, double newC, double newTotal): id_(id){ }

RepastHPCAgent::~RepastHPCAgent(){ }


void RepastHPCAgent::set(int currentRank){
    id_.currentRank(currentRank);
    int numNeighbors = 483;

    for (int i = 0; i < numNeighbors; i++) {
       friendGroup = rand() % 4;
    }
}

void RepastHPCAgent::play(repast::SharedNetwork<RepastHPCAgent,
                              repast::RepastEdge<RepastHPCAgent>,
                              repast::RepastEdgeContent<RepastHPCAgent>,
                              repast::RepastEdgeContentManager<RepastHPCAgent> > *network){
    std::vector<RepastHPCAgent*> agentsToPlay;
    network->successors(this, agentsToPlay);

   std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
   while(agentToPlay != agentsToPlay.end()){
      bool isFriend = rand() % 2;
      if(isFriend) {
         int tempId = (int)(*agentToPlay)->getId().id();
         totalFriends.push_back(tempId);
      }
		
      agentToPlay++;
   }

   std::cout << getId().id() << "'s Friends are: ";
   for (auto it = totalFriends.begin(); it != totalFriends.end(); ++it)
      std::cout << " " << *it;
   std::cout << std::endl;
	
}


void RepastHPCAgent::playAgain(repast::SharedNetwork<RepastHPCAgent,
   repast::RepastEdge<RepastHPCAgent>,
   repast::RepastEdgeContent<RepastHPCAgent>,
   repast::RepastEdgeContentManager<RepastHPCAgent> > *network, int degreesOfFreedom) {
   std::vector<RepastHPCAgent*> agentsToPlay;
   network->successors(this, agentsToPlay);


   std::unordered_set<int> setOfFriends;
   for (int i = 0; i < totalFriends.size(); i++) {
      setOfFriends.insert(totalFriends.at(i));
   }

   std::cout << getId().id() << "'s Friends in setOfFriends are: ";
   for (auto it = setOfFriends.begin(); it != setOfFriends.end(); ++it)
      std::cout << " " << *it;
   std::cout << std::endl;

   for (int i = 0; i < degreesOfFreedom; i++) {
      std::unordered_set<int> tempSetOfFriends(setOfFriends);
      setOfFriends.clear();
      std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
      while (agentToPlay != agentsToPlay.end()) {
         std::unordered_set<int>::const_iterator isFriend = tempSetOfFriends.find((*agentToPlay)->getId().id());
         if (isFriend != tempSetOfFriends.end()) {
            vector<int> friendsTotalFriends = (*agentToPlay)->getTotalFriends();
            for (int j = 0; j < friendsTotalFriends.size(); j++) {
               setOfFriends.insert(friendsTotalFriends.at(j));
            }
         }
         agentToPlay++;
      }
   }

   std::cout << getId().id() << "'s Friends " << degreesOfFreedom << " degrees of freedom away: " ;
   for (auto it = setOfFriends.begin(); it != setOfFriends.end(); ++it)
      std::cout << " " << *it;
   std::cout << std::endl;
 

}

/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }

RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank):
id(_id), rank(_rank), type(_type), currentRank(_currentRank){ }
