

#ifndef PROJECT
#define PROJECT

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"


/* Agents */
class RepastHPCAgent{
	
private:
    repast::AgentId   id_;
    bool          infected;
    int           agentType;
    int           agentEnergy;

    int numWorkers;
    int totResource;
    int totIntell;
    int durWork;
    int status;
    int locX;
    int locY;
    int jobIndex;
    int counter;
	
public:
    RepastHPCAgent(repast::AgentId id);
	RepastHPCAgent(){}
    RepastHPCAgent(repast::AgentId id, bool infected);
	
    ~RepastHPCAgent();
	
    /* Required Getters */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }
	
    /* Getters specific to this kind of Agent */
    bool getInfected() { return infected; }
    int  getAgentType() { return agentType; }
    int  getAgentEnergy() { return agentEnergy; }

    int  getNumWorkers() { return numWorkers; }
    int  getTotResource() { return totResource; }
    int  getTotIntell() { return totIntell; }
    int  getDurWork() { return durWork; }
    int  getStatus() { return status; }
    int  setMoveLocX(int thisX) { return locX = thisX; }
    int  setMoveLocY(int thisY) { return locY = thisY; }
    int  setDurWork(int thisDurWork) { return durWork = thisDurWork; }
    int  setJobIndex(int thisJobIndex) { return jobIndex = thisJobIndex; }

 

	
    /* Setter */
    void set(int currentRank, bool newinfected);
	
    /* Actions */
    void play(repast::SharedContext<RepastHPCAgent>* context,
              repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);    // Choose three other agents from the given context and see if they cooperate or not
    void playAgain(repast::SharedContext<RepastHPCAgent>* context,
       repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);
    void move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space);
};

/* Serializable Agent Package */
struct RepastHPCAgentPackage {
	
public:
    int    id;
    int    rank;
    int    type;
    int    currentRank;
    bool infected;
    int agentType;
    int agentEnergy;
    int numWorkers;
    int totResource;
    int totIntell;
    int durWork;
    int status;
    int locX;
    int locY;
    int jobIndex;

    /* Constructors */
    RepastHPCAgentPackage(); // For serialization
    RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, bool _infected, int _agentType, int _agentEnergy, int _numWorkers, int _totResource, int _totIntell, int _durWork, int _status);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & infected;
        ar & agentType;
        ar & agentEnergy;
        ar & numWorkers;
        ar & totResource;
        ar & totIntell;
        ar & durWork;
        ar & status;
        ar & locX;
        ar & locY;
        ar & jobIndex;
    }
	
};


#endif