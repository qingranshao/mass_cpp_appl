#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"

#include "Model.h"
#include <ctime>
#include <iostream>
#include <cstdio>


int main(int argc, char** argv){
	
	std::string configFile = argv[1]; // The name of the configuration file is Arg 1
	std::string propsFile  = argv[2]; // The name of the properties file is Arg 2
	
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;

	repast::RepastProcess::init(configFile);
	
	RepastHPCModel* model = new RepastHPCModel(propsFile, argc, argv, &world);
	repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
	
	model->init();
	model->initSchedule(runner);
	
  	



    	std::clock_t start;
    	double duration;

    	start = std::clock();


	runner.run();

   
	duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

   if (repast::RepastProcess::instance()->rank() == 0)
      printf("\nEnd of simulation. Elasped time using RepastHPC framework:: %ld microseconds.\n", duration );
	
	delete model;
	
	repast::RepastProcess::instance()->done();
	
}