

#include "Bailin.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Point.h"

RepastHPCAgent::RepastHPCAgent(repast::AgentId id) : id_(id) { }


RepastHPCAgent::~RepastHPCAgent(){ }


void RepastHPCAgent::set(int currentRank){
    id_.currentRank(currentRank);

    financeType = rand() % 100 + 1;
    if (financeType < 81) {
       financeType = 0;
    }
    else {
       financeType = 1;
    }


    if (financeType == 1) { //bank
       amountOfMoney = rand() % 1000 + 1;			// set up initial money of banks and firms
       std::cout << "bank: " << getId().id() << "s initial money = " << amountOfMoney << std::endl;
    }
    else { //firm
       amountOfMoney = -(rand() % 100 + 1);			// set up initial money of banks and firms
       std::cout << "firm: " << getId().id() << "s initial money = " << amountOfMoney << std::endl;
    }

    firmsServiced = vector<int>();
}


void RepastHPCAgent::play(repast::SharedContext<RepastHPCAgent>* context,
   repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space) {
   std::vector<RepastHPCAgent*> agentsToPlay;


   std::vector<int> agentLoc;
   space->getLocation(id_, agentLoc);

   if (financeType == 1) {

      int firmCount = 0;
      int bankCount = 0;

      std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
      while (agentToPlay != agentsToPlay.end()) {
         std::vector<int> otherLoc;
         space->getLocation((*agentToPlay)->getId(), otherLoc);

         if (otherLoc[0] == agentLoc[0] && otherLoc[1] == agentLoc[1]) {
            if ((*agentToPlay)->getFinanceType() == 1) {
               bankCount++;
            }
            else {
               firmCount++;
            }
         }

         agentToPlay++;
      }

      if (firmCount > 0) {
         std::vector<RepastHPCAgent*>::iterator agentToPlay = agentsToPlay.begin();
         while (agentToPlay != agentsToPlay.end()) {
         
            if ((*agentToPlay)->getFinanceType() == 0) {
               int tempFirmMoney = (*agentToPlay)->getAmountOfMoney();
               if (tempFirmMoney < 0 && (amountOfMoney > abs(tempFirmMoney))) {

                  (*agentToPlay)->setAmountOfMoney(0);
                  amountOfMoney -= abs(tempFirmMoney);

                  firmsServiced.push_back((*agentToPlay)->getId().id());
               }
            }


            agentToPlay++;
         }

      }

   }
}

void RepastHPCAgent::move(repast::SharedDiscreteSpace<RepastHPCAgent, repast::WrapAroundBorders, repast::SimpleAdder<RepastHPCAgent> >* space) {
   if (financeType == 1) {

      std::cout << "bank: " << getId().id() << "s money = " << amountOfMoney << endl;
      std::cout << " and serviced banks: ";
      for (int i = 0; i < firmsServiced.size(); i++) {
         std::cout << firmsServiced.at(i) << " ";
      }

      std::vector<int> agentNewLoc;
      agentNewLoc.push_back(rand() % 20); 
      agentNewLoc.push_back(rand() % 20); 
      space->moveTo(id_, agentNewLoc);

   }
   else {	// if fish
      std::cout << "firm: " << getId().id() << "s money = " << amountOfMoney << endl;
   }


}

/* Serializable Agent Package Data */

RepastHPCAgentPackage::RepastHPCAgentPackage(){ }

RepastHPCAgentPackage::RepastHPCAgentPackage(int _id, int _rank, int _type, int _currentRank, int _financeType, int _amountOfMoney):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), financeType(_financeType), amountOfMoney(_amountOfMoney){ }
