//
// Created by sarah on 8/10/20.
//

#ifndef LIFEOBSERVER_H
#define LIFEOBSERVER_H

#include "relogo/Observer.h"
#include "relogo/Patch.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Properties.h"
#include "relogo/grid_types.h"

#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include "AgentPackage.h"
#include "LifePlace.h"

using namespace std;
using namespace repast;
using namespace relogo;

const string NUM_LIFE_PLACES = "life.count";

class LifeObserver : public Observer {

public:

    // Constructor / deconstructor
    LifeObserver()= default;

    virtual ~LifeObserver(){}

    // Initialization
    virtual void setup(Properties& prop);

    // Run
    void go();

    // Begin necessary functions for migration - must be included for compilation

    // create and provide for agents moving between processes
    RelogoAgent* createAgent(const AgentPackage& content);
    void provideContent(const AgentRequest& request, std::vector<AgentPackage>& out);

    // Agent serialization - for buffer
    void createAgents(vector<AgentPackage>& contents, vector<RelogoAgent*>& outgoing);

    void provideContent(RelogoAgent* relogoAgent, vector<AgentPackage>& outgoing);

    void updateAgent(AgentPackage agentPackage);

private:

    Properties properties;
    //int lifeType;

};


#endif //LIFEOBSERVER_H
