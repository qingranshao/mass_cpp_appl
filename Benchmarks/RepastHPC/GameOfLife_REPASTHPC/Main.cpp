#include <iostream>

#include <boost/mpi.hpp>

#include "repast_hpc/io.h"
#include "repast_hpc/RepastProcess.h"
#include "relogo/SimulationRunner.h"
#include "relogo/Patch.h"

#include "LifeObserver.h"

using namespace std;
using namespace repast;
using namespace relogo;

void runConwaysGoL(string propertiesFile, int argc, char** argv);

int main(int argc, char **argv) {
    boost::mpi::environment env(argc, argv);
    string config, properties;
    boost::mpi::communicator world;

    if (argc >= 3) {
        config = argv[1];
        properties = argv[2];
    } else {
        if (world.rank() == 0) { cout << "Need at least 3 arguments : X, path to repast props, path to model props" <<
        endl; }
        return -1;
    }

    if (config.size() > 0 && properties.size() > 0) {
        RepastProcess::init(config, &world);
        runConwaysGoL(properties, argc, argv);
    } else {
        if (world.rank() == 0) { cout << "Need configuration and/or properties files" << endl; }
        return -1;
    }

    RepastProcess::instance()->done();

    if (world.rank() == 0) { cout << " RepastHPC finished; simulation completed " << endl; }

    return 0;
}

void runConwaysGoL(string propertiesFile, int argc, char** argv) {
    boost::mpi::communicator world;
    Properties properties(propertiesFile, argc, argv, &world);

    string time;
    repast::timestamp(time);
    properties.putProperty("date_time.run", time);
    properties.putProperty("process.count", world.size());

    SimulationRunner runner(&world);

    if (world.rank() == 0) { cout << "Initialized and starting simulation ..." << endl; }

    repast::Timer timer;
    timer.start();

    runner.run<LifeObserver, LifePlace>(properties);

    properties.putProperty("run.time", timer.stop());

    if (world.rank() == 0) { cout << "********************* End of Simulation ******************************" << endl; }
}
