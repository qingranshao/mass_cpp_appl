//
// Created by sarah
//

#ifndef TUBERCULOSISOBSERVER_H
#define TUBERCULOSISOBSERVER_H

#include "unordered_map"
#include <algorithm>
#include <fstream>

#include "relogo/Observer.h"
#include "relogo/Patch.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Properties.h"
#include "relogo/RandomMove.h"
#include "relogo/grid_types.h"

#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include "AgentPackage.h"
#include "LungPlace.h"
#include "Macrophage.h"
#include "TCell.h"

using namespace std;

class TuberculosisObserver : public repast::relogo::Observer {

public:

    // Constructor / deconstructor
    TuberculosisObserver() : lungPlaceType(-1), macrophageType(-1), tCellType(-1) {}

    virtual ~TuberculosisObserver()= default;

    // Initialization
    void setup(repast::Properties& prop);

    // Run
    void go();

    // Begin necessary functions for serialization/migration

    repast::relogo::RelogoAgent* createAgent(const AgentPackage& content);
    void createAgents(vector<AgentPackage>& contents, vector<repast::relogo::RelogoAgent*>& outgoing);

    void provideContent(const repast::AgentRequest& request, vector<AgentPackage>& out);
    void provideContent(repast::relogo::RelogoAgent* agent, vector<AgentPackage>& outgoing);
    AgentPackage provideContentHelp(repast::AgentId id);

    void updateAgent(AgentPackage content);

    const int MACROPHAGE_COUNT = 100;
    const int TCELL_SPAWNER_COUNT = 4;

    static const int tCellEntrance = 10;

private:

    repast::Properties properties;

    int lungPlaceType;
    int macrophageType;
    int tCellType;

    // AgentSet of LungPlaces that are blood vessels
    repast::relogo::AgentSet<LungPlace> bloodVessels;

    // File writing
    streambuf* stream_buff_out; // Backup streambuffers of  cout
    fstream file;

    // Helper
    void macroStartPositions();

    void setBloodVessels();

    void spawnInitialBacteria();

    void spawnThroughVessels();

    void spawnHelper(LungPlace *place);

    void createMacroSpawners();

    void createTCellSpawners();

    void startStateFile(int day);

    void endStateFile(int day);

};

#endif //TUBERCULOSISOBSERVER_H
