//
// Created by sarah on 9/12/20.
//

#include "Macrophage.h"
#include "TuberculosisObserver.h"


// Helper struct to find max chemokine in Moore neighborhood so the Macrophage can move there
// - must return a double
struct chemokineOnLungPlace {
    double operator()(const LungPlace *lungPlace) const {
        return static_cast<double>(lungPlace->getChemokine());
    }
};

void Macrophage::move() {
    if (!_spawner && _state != DEAD) {
        // store old location
        previousPlaceCoords[0] = pxCor();
        previousPlaceCoords[1] = pyCor();

        // move to neighbor with the highest chemokine level
        repast::relogo::AgentSet<LungPlace> neighborPlaces = patchHere<LungPlace>()->neighbors<LungPlace>();
//        // debug
//        if (_id.id() == 1) {
//            cout << "Macrophage 1 located at " << pxCor() << " , " << pyCor();
//            cout << " and has the following places to jump to: ";
//            for (auto place : neighborPlaces) {
//                cout << " (" << place->pxCor() << " , " << place->pyCor() << ") ";
//            }
//        }
        LungPlace *mostChemokine = neighborPlaces.maxOneOf(chemokineOnLungPlace());
//        // debug
//        if (_id.id() == 1) {
//            cout << " -> chose " << mostChemokine->pxCor() << " , " << mostChemokine->pyCor() << endl;
//        }

        moveTo(mostChemokine);
    }
}

/**
 * If there is more than one macrophage on the current Patch, jump back to previous location if this Macro doesn't
 * have the lowest ID # here
 */
void Macrophage::collisionFreeJumpBack() {
    if (!_spawner && _state != DEAD) {
        // check to see if there are other Macrophages here (that aren't spawners)
        repast::relogo::AgentSet<Macrophage> otherMacros = turtlesHere<Macrophage>();

        for (Macrophage *macro : otherMacros) {
            // jump back if other macrophages are here with a lower id than yours
            if (!macro->isSpawner() && macro->_id.id() < _id.id()) {
                setxy(static_cast<double>(previousPlaceCoords[0]), static_cast<double>(previousPlaceCoords[1]));

                previousPlaceCoords[0] = -1;
                previousPlaceCoords[1] = -1;
                return;
            }
        }
    }
}

void Macrophage::react() {
    if (!_spawner && _state != DEAD) {
        // check the day to see if there are any T-cells in the simulation
        int today = patchHere<LungPlace>()->getCurrentTick();

        bool onBloodVessel = patchHere<LungPlace>()->isBloodVessel();
        bool onTCell = false;
        bool onBacteria = patchHere<LungPlace>()->hasBacteria();

        // check to see if macrophage is on the same Place as a T-cell
        if (today >= TuberculosisObserver::tCellEntrance) {
            repast::relogo::AgentSet<TCell> tCellsHere = turtlesHere<TCell>();
            onTCell = (onBloodVessel) ? (tCellsHere.size() > 1) : (tCellsHere.size() > 0);
        }

        switch (_state) {
            case RESTING:
                if (onBacteria) {
                    _state = INFECTED;
                    _dayInfected = today;
                    growInfectedIntraCBact(_dayInfected);
                }
                break;

            case INFECTED:
                // check if INFECTED macrophage will be activated by T-cell
                if (onTCell) {
                    _state = ACTIVATED;
                    _intracellularBacteria = 0;
                    break;
                }

                // grow intra-cellular bacteria at the infected macrophage rate
                growInfectedIntraCBact(today);

                // if not activated, check to see if internal bacterial level will cause this macrophage to become
                // CHRONICALLY_INFECTED
                if (_intracellularBacteria >= chronicInfection) {
                    _state = CHRONICALLY_INFECTED;
                    //printf("Macrophage %d became CHRONICALLY INFECTED", M_ID);
                }
                break;

            case ACTIVATED:
                // do nothing - Place will react to this macrophage's presence accordingly
                break;

            case CHRONICALLY_INFECTED:
                // grow intra-cellular bacteria at the chronically infected macrophage rate
                growChronicIntraCBact();
                if (_intracellularBacteria >= bacterialDeath || onTCell) {
                    _state = DEAD;
                }
                break;

            default:
                printf("Invalid state in macrophage_react() for Macrophage %d at ( %d, %d )", _id.id(), pxCor(),
                       pyCor());
                break;
        }
    }
}

void Macrophage::growInfectedIntraCBact(int today) {
    int t = today - _dayInfected;
    _intracellularBacteria = (2 * t) + 1;
}


void Macrophage::growChronicIntraCBact() {
    _intracellularBacteria += 2;
}

void Macrophage::removeDeadMacrophages() {
    if (_state == DEAD) {
        die();
    }
}

// Getters and setters

int Macrophage::getDayInfected() const {
    return _dayInfected;
}

void Macrophage::setDayInfected(int dayInfected) {
    _dayInfected = dayInfected;
}

int Macrophage::getIntracellularBacteria() const {
    return _intracellularBacteria;
}

void Macrophage::setIntracellularBacteria(int intracellularBacteria) {
    _intracellularBacteria = intracellularBacteria;
}

Macrophage::State Macrophage::getState() const {
    return _state;
}

void Macrophage::setState(Macrophage::State state) {
    _state = state;
}

bool Macrophage::isSpawner() const {
    return _spawner;
}

void Macrophage::writeState() {
    cout << "\n<xagent>";
    cout << "\n<name>Macrophage</name>";
    cout << "\n<m_id>" << _id.id() << "</m_id>";
    cout << "\n<m_x>" << pxCor() << "</m_x>";
    cout << "\n<m_y>" << pyCor() << "</m_y>";
    int writeState = static_cast<int>(_state);
    cout << "\n<state>" << writeState << "</state>";
    cout << "\n<intracellular_bacteria>" << _intracellularBacteria << "</intracellular_bacteria>";
    cout << "\n</xagent>" << endl;
}







