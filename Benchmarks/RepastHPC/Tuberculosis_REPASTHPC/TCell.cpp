//
// Created by sarah on 9/12/20.
//

#include "TCell.h"

// Helper struct to find max chemokine in Moore neighborhood so the Macrophage can move there
// - must return a double
struct chemokineOnLungPlace {
    double operator()(const LungPlace *lungPlace) const {
        return static_cast<double>(lungPlace->getChemokine());
    }
};

void TCell::move() {
    if (!_spawner) {
        // store old location
        previousPlaceCoords[0] = patchHere<LungPlace>()->pxCor();
        previousPlaceCoords[1] = patchHere<LungPlace>()->pyCor();

        // move to neighbor with the highest chemokine level
        repast::relogo::AgentSet<LungPlace> moore = patchHere<LungPlace>()->neighbors<LungPlace>();
        LungPlace *mostChemokine = moore.maxOneOf(chemokineOnLungPlace());
        moveTo(mostChemokine);
    }
}

void TCell::collisionFreeJumpBack() {
    if (!_spawner) {
        // check to see if there are other TCell here (that aren't spawners)
        repast::relogo::AgentSet<TCell> otherTCells = turtlesHere<TCell>();

        for (TCell *tCell : otherTCells) {
            if (!tCell->isSpawner() && tCell->_id.id() < _id.id()) {
                setxy(static_cast<double>(previousPlaceCoords[0]), static_cast<double>(previousPlaceCoords[1]));

                previousPlaceCoords[0] = -1;
                previousPlaceCoords[1] = -1;
                break;
            }
        }
    }
}

void TCell::writeState() {
    cout << "\n<xagent>";
    cout << "\n<name>TCell</name>";
    cout << "\n<t_x>" << pxCor() << "</t_x>";
    cout << "\n<t_y>" << pyCor() << "</t_y>";
    int spawnerWrite = (_spawner) ? 1 : 0;
    cout << "\n<spawner>" << spawnerWrite << "</spawner>";
    cout << "\n</xagent>" << endl;
}


