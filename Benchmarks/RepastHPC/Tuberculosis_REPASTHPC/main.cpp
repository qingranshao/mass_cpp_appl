#include <iostream>

#include <boost/mpi.hpp>

#include "repast_hpc/io.h"
#include "repast_hpc/RepastProcess.h"
#include "relogo/SimulationRunner.h"
#include "relogo/Patch.h"

#include "TuberculosisObserver.h"
#include "LungPlace.h"

using namespace std;
using namespace repast;
using namespace relogo;

void runTuberculosis(string propertiesFile, int argc, char **argv);

int main(int argc, char **argv) {
    boost::mpi::environment env(argc, argv);
    string config, properties;
    boost::mpi::communicator world;

    if (argc >= 3) {
        config = argv[1];
        properties = argv[2];
    } else {
        if (world.rank() == 0) {
            cout << "Need at least 3 arguments : X, path to repast props, path to model props" <<
                 endl;
        }
        return -1;
    }

    if (config.size() > 0 && properties.size() > 0) {
        RepastProcess::init(config, &world);
        runTuberculosis(properties, argc, argv);
    } else {
        if (world.rank() == 0) { cout << "Need configuration and/or properties files" << endl; }
        return -1;
    }

    RepastProcess::instance()->done();

    if (world.rank() == 0) { cout << " RepastHPC finished; simulation completed " << endl; }

    return 0;
}

void runTuberculosis(string propertiesFile, int argc, char **argv) {
    boost::mpi::communicator world;
    Properties properties(propertiesFile, argc, argv, &world);

    string time;
    repast::timestamp(time);
    properties.putProperty("date_time.run", time);
    properties.putProperty("process.count", world.size());

    SimulationRunner runner(&world);

    repast::Timer timer;
    timer.start();
/**
    // Debug - attach mpi process to gdb

    volatile int i = 0;
    char hostname[256];
    gethostname(hostname, sizeof(hostname));
    printf("PID %d on %s ready for attach\n", getpid(), hostname);
    fflush(stdout);
    while (0 == i) {
        sleep(5);
    }

    // End debug
**/
    if (world.rank() == 0) { cout << "Initialized and starting simulation ..." << endl; }

    runner.run<TuberculosisObserver, LungPlace>(properties);

    properties.putProperty("run.time", timer.stop());

    if (world.rank() == 0) { cout << "********************* End of Simulation ******************************" << endl; }

}
