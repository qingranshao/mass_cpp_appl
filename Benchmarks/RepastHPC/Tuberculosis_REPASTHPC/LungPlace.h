//
// Created by sarah
//

#ifndef LUNGPLACE_H
#define LUNGPLACE_H

#include "relogo/Patch.h"

#include "AgentPackage.h"
#include "Macrophage.h"
#include "TCell.h"

class LungPlace : public repast::relogo::Patch {

public:

// LungPlace constants

    // Number of days between points of time when extra-cellular bacteria grows radially out by one unit
    const int bacterialGrowth = 10;

    // Max chemokine level dispersed by an INFECTED and CHRONICALLY_INFECTED macrophages
    // - also how many days the signal lasts at the current Place
    const int maxChemokine = 2;

// State functions

    void decayChemokineAndGrowBacteria();

    void updateNextState();

    void reactToMacrophages();

    void writeState();

// Constructors and deconstructor

    // Default constructor
    LungPlace(const repast::AgentId& id, repast::relogo::Observer* obs):
            repast::relogo::Patch(id,obs), _bacteria(false), _bloodVessel(false), _chemokine(0), _currentTick(0),
            _bacteriaNext(false), _chemokineNext(0) {}

    // For LungPlace reconstruction after serialization and shadow space sync using AgentPackage
    LungPlace(const repast::AgentId id, repast::relogo::Observer* obs, const AgentPackage& package) :
            repast::relogo::Patch(id, obs),
            _bacteria(package.bacteria), _bloodVessel(false), _chemokine(package.chemokine), _currentTick(-1),
            _bacteriaNext(package.bacteria), _chemokineNext(package.chemokine) {}
            // note: we don't care if the neighboring LungPlace is a bloodVessel or not
            //       or what current tick the neighbor is on (should be the same as local LungPlaces)

    virtual ~LungPlace() {}

// Getters and setters

    bool hasBacteria() const;

    void setBacteria(bool bacteria);

    void spawnBacteria();

    void killBacteria();

    bool isBloodVessel() const;

    void setBloodVessel();

    int getChemokine() const;

    void setChemokine(int chemokine);

    int getCurrentTick() const;

private:

     bool _bacteria; // bacterial flag

     bool _bacteriaNext; // next state bacteria

     bool _bloodVessel; // spawn point flag

     int _chemokine; // level of chemokine chemical signal; values of 0, 1, and 2

     int _chemokineNext; // level of chemokine for the next state

     int _currentTick; // current iteration tracker

};


#endif // LUNGPLACE_H
