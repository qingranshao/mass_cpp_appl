//
// Created by sarah on 9/12/20.
//

#include "LungPlace.h"

// State functions

void LungPlace::decayChemokineAndGrowBacteria() {
    _chemokineNext =  (_chemokineNext > 0) ? (_chemokineNext - 1) : (0);

    if (_currentTick > 0 && _currentTick % bacterialGrowth == 0 && !_bacteria) {
        // check neighbors for bacteria
        for (LungPlace *neighbor : neighbors<LungPlace>()) {
            if (neighbor->hasBacteria()) {
                _bacteriaNext = true;
                break;
            }
        }
    }
    _currentTick++;
}

void LungPlace::updateNextState() {
    _bacteria = _bacteriaNext;
    _chemokine = _chemokineNext;
}

void LungPlace::reactToMacrophages() {

    // because an ACTIVATED or a RESTING macrophage kills or eats, respectively, any bacteria it's
    // located on and becomes infected, this tracks if any are located on the current Place
    bool activatedOrResting = false;

    // react to macro here and others in Moore neighborhood
    repast::relogo::AgentSet<Macrophage> vicinity;
    for (LungPlace *place : neighbors<LungPlace>()) { // add neighboring non-spawner macrophages

        for (Macrophage* macrophage : place->turtlesHere<Macrophage>()) {
            if (!macrophage->isSpawner()) {
                vicinity.add(macrophage);
            }
        }
    }

    // add non-spawner turtles that are on this LungPlace
    for (Macrophage* macrophage : turtlesHere<Macrophage>()) {
        if (!macrophage->isSpawner()) {
            vicinity.add(macrophage);
        }
    }

    bool maxedChemokine = _chemokine == maxChemokine;
    // Look at each macrophage in the vicinity and react accordingly
    for (Macrophage* macrophage : vicinity) {

        Macrophage::State state = macrophage->getState();

        if (!maxedChemokine && (state == Macrophage::State::INFECTED ||
                                state == Macrophage::State::ACTIVATED ||
                                state == Macrophage::State::CHRONICALLY_INFECTED)) {

            _chemokineNext = maxChemokine;
            _chemokine = maxChemokine;
            maxedChemokine = true;
        }

        // If an activated/resting macrophage is on this Place, any bacteria here is killed
        if ( (state == Macrophage::State::ACTIVATED || state == Macrophage::State::RESTING) &&
             (macrophage->pxCor() == pxCor() && macrophage->pyCor() == pyCor()) ) {
            activatedOrResting = true;
            _bacteria = false;
            _bacteriaNext = _bacteria;
        }

        // Macrophage has died due to a bacterial or T-cell death (either on this Place or from its neighbors) and
        // there is no activated or resting macrophage on this Place to kill the bacteria - spread bacteria here
        if (!_bacteria && !activatedOrResting && state == Macrophage::State::DEAD) {
            _bacteria = true;
            _bacteriaNext = _bacteria;
        }
    }
}


// Getters and setters

bool LungPlace::hasBacteria() const {
    return _bacteria;
}

void LungPlace::setBacteria(bool bacteria) {
    _bacteria = bacteria;
    _bacteriaNext = bacteria;
}

void LungPlace::spawnBacteria() {
    _bacteria = true;
    _bacteriaNext = _bacteria;
}

void LungPlace::killBacteria() {
    _bacteria = false;
    _bacteriaNext = _bacteria;
}

bool LungPlace::isBloodVessel() const {
    return _bloodVessel;
}

void LungPlace::setBloodVessel() {
    _bloodVessel = true;
}

int LungPlace::getChemokine() const {
    return _chemokine;
}

void LungPlace::setChemokine(int chemokine) {
    _chemokine = chemokine;
    _chemokineNext = _chemokine;
}

int LungPlace::getCurrentTick() const {
    return _currentTick;
}

void LungPlace::writeState() {
    cout << "\n<xagent>";
    cout << "\n<name>Place</name>";
    cout << "\n<p_id>" << _id.id() << "</p_id>";
    cout << "\n<x>" << pxCor()  << "</x>";
    cout << "\n<y>" << pyCor()  << "</y>";

    int bactWrite = (_bacteria) ? 1 : 0;
    cout << "\n<bacteria>" <<  bactWrite << "</bacteria>";
    cout << "\n<chemokine>" <<  _chemokine << "</chemokine>";
    int bloodVWrite = (_bloodVessel) ? 1 : 0;
    cout << "\n<blood_vessel>" <<  bloodVWrite << "</blood_vessel>";
    int macrophageWrite = (turtlesHere<Macrophage>().size() > 0) ? 1 : 0;
    cout << "\n<macrophage>" <<  macrophageWrite << "</macrophage>";
    int tCellWrite = (turtlesHere<TCell>().size() > 0) ? 1 : 0;
    cout << "\n<t_cell>" <<  tCellWrite << "</t_cell>";
    cout << "\n</xagent>" << endl;
}











