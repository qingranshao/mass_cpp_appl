#ifndef GRAPH_H
#define GRAPH_H
#include <iostream>
#include <sstream>
#include <queue>
#include <vector>


// Graph is directed (i.e. one-way direction'd edges)
class Graph {
  public:
    Graph(int graphSize);
    ~Graph();

    void printAdjGraph();
    void printGraph();

    void initAgents(int maxAgents);
    void runAgents(int TIME_MAX, int maxAgents);
  private:
    // Manages Dijkstra
    struct Table {
      bool visited = false;
      int path = -1;
      int dist = -1;
    };

    // Representative of each Graph Vertex
    struct Link {
      std::queue<int> agentIds;
      static const int MAX_CAPACITY_AGENTS = 10;
      int stallTime = 0;
      int weight = 40;
    };
    // Dijkstra's Table
    Table** T;

    // Vertex Handle
    Link* links;

    // Stores the Graph's Structure
    int **adjGraph;
    int size;

    // A vector holding paths of the agents(i.e. cars)
    std::vector< std::vector<int> > agents;

    /*
      Dijkstra's Algorithm
      - Assumes only called once in initialization
      - Assumes Graph is of appropriate size
    */
    void findShortestPath();
    void buildGraph();
    std::vector<int> findPath(int src, int dest);
};

#endif
