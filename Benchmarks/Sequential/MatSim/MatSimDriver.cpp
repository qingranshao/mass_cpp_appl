#include "Graph.h"

int main(int argc, char* argv[]) {
  // Input validation
  if (argc != 4) {
    std::cerr << "Want GRAPH_SIZE, MAX_AGENTS, ITERATIONS" << std::endl;
    std::exit(-1);
  }

  // User Input
  int size = atoi(argv[1]);
  int maxAgents = atoi(argv[2]);
  int iter = atoi(argv[3]);

  // Initalize Graph
  std::cout << "Initializing...\n";
  Graph a(size);
  std::cout << "\n";

  // Displaying State of Graph
  std::cout << "Printing Adjacency...\n";
  a.printAdjGraph();
  std::cout << "\n";
  std::cout << "Printing Viable Paths...\n";
  a.printGraph();
  std::cout << "\n";

  // Run Simulation
  a.runAgents(iter,maxAgents);
  std::cin.ignore();
}
