#include "Cluster.h"

Cluster::Cluster(int type, int size) {
  clusterId = type;
  for (int i = 0; i < size; i++) {
    agents.push_back(Agent::make_agent(type));
    agents[i]->agentId = i;
  }
}

Cluster::Cluster(const Cluster& obj) {
  this->clusterId = obj.clusterId;
  this->inBuffer = obj.inBuffer;
  this->outBuffer = obj.outBuffer;

  // Deep Copy of Agents
  this->agents.reserve((obj.agents).size());
  for (int i = 0; i < (obj.agents).size(); i++) {
    std::vector<int> raw = (obj.agents[i])->stripCopy();
    agents.push_back(Agent::make_agent(raw));
  }
}

Cluster::~Cluster() {
  for (int i = 0; i < agents.size(); i++) {
    delete agents[i];
    agents[i] = NULL;
  }
  agents.clear();
}

// May change return type to string
bool Cluster::initMsg(const int phase) {
  for (int i = 0; i < agents.size(); i++) {
    std::string msg = agents[i]->initMsg(phase);
    if(msg != "") {
      outBuffer.push_back(msg);
    }
  }
  return outBuffer.empty();
}

void Cluster::runCluster() {
  // Assists in randomizing the seed for rand() call
  srand(time(NULL));
  // Check inBuffer and per msg
  while (!inBuffer.empty()) {
    // Obtain message randomly
    int index = rand()%inBuffer.size();
    std::string temp = inBuffer[index];
    inBuffer.erase(inBuffer.begin() + index);

    // Splitting message for convenience
    std::vector<std::string> parsed = Agent::split(temp, '_');

    // Broadcast Check
    if (parsed[0].substr(parsed[0].find_last_of('-')+1) == "0") {
      for (int i = 0; i < agents.size(); i++) {
        // Agent processes &/ sends
        std::string msg = agents[i]->runAgent(parsed);
        if (msg != "") {
          outBuffer.push_back(msg);
          std::cout << "Broadcast -> Multicast: " << msg << "\n";
        }
      }
    }
    else {
      // Searching for clusterId
      std::stringstream ss;
      ss << "C" << clusterId;
      std::size_t start = parsed[1].find(ss.str());

      if (start == std::string::npos) continue;

      // Searching for next
      std::size_t next = parsed[1].find('C', start+1);
      if (next == std::string::npos) {
        next = parsed[1].length();
      }

      // Search for AgentID with ClusterID
      std::vector<std::string> agentIDs = Agent::split(parsed[1].substr(start,next), 'A'); // CHECK
      for (int i = 1; i < agentIDs.size(); i++) {// CHECK
        // Determines agent index
        int agentIndex = atoi(agentIDs[i].c_str());

        // Agent processes &/ sends
        std::string msg = agents[agentIndex]->runAgent(parsed);
        if (msg != "") {
          outBuffer.push_back(msg);
          std::cout << "Multicast msg: " << msg << "\n";
        }
      }
    }
  }
}
