#include "Agent.h"
#include "Bank.h"
#include "Firm.h"

// Factory Implementation
Agent* Agent::make_agent(int type) {
  if (type == 0) return new Firm;
  else if (type == 1) return new Bank;
  else return NULL;
}

Agent* Agent::make_agent(std::vector<int> raw) {
  if (raw[0] == 0) return new Firm(raw);
  else if (raw[0] = 1) return new Bank(raw);
  else return NULL;
}

// Constructor
Agent::Agent() {
  recvFlag = false;
}

// Splits message
const std::vector<std::string> Agent::split(std::string msg, char delim) {
  // Inititalize retVal
  std::vector<std::string> retVal;

  // Parsing Prep
  char x;
  std::string word;

  // Initialize stringstream
  std::stringstream ss(msg);

  // String Parsing
  while (ss >> x) {
    word.push_back(x);
    if (ss.peek() == delim) {
      retVal.push_back(word);
      word.erase();
      ss.ignore();
    }
  }
  retVal.push_back(word);
  return retVal;
}
