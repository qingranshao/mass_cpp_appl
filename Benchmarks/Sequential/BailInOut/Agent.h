#ifndef AGENT_H
#define AGENT_H
#include <iostream>
#include <vector>   // std::vector
#include <string>   // std::string
#include <sstream>  // stringstream
#include <stdlib.h> // rand, srand
#include <time.h>   // time

// Note: virtual function control "override" only in c++11

class Agent {
  public:
    // Factory Methods
    static Agent* make_agent(int type);
    static Agent* make_agent(std::vector<int>);
    Agent();

    // Defined/Maintained by Cluster
    int agentId;

    int clusterId;
    bool recvFlag;

    // pure virtuals
    virtual std::string initMsg(const int) = 0;
    virtual std::string runAgent(const std::vector<std::string>&) = 0;
    virtual std::vector<int> stripCopy() const = 0;

    // Splits message
    static const std::vector<std::string> split(std::string msg, char delim);
};

#endif
