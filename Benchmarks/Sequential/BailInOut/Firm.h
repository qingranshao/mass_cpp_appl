#ifndef FIRM_H
#define FIRM_H

#include "Agent.h"

class Firm : public Agent {
  public:
    // Constructor/Destructor
    Firm();
    Firm(std::vector<int> raw);

    // Overridden functs
    std::string initMsg(const int) override;
    std::string runAgent(const std::vector<std::string>&) override;
    std::vector<int> stripCopy() const override;

  private:
    // Loan parameters
    std::vector<int> ActiveLoans;
    int totalCapital;
};

#endif
