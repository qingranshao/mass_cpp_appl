#ifndef CLUSTER_H
#define CLUSTER_H

#include <vector>
#include <string>
#include <stdlib.h> // atoi
#include "Agent.h"

//template <class T>
class Cluster {
  public:
    // Constructor/Deconstructor
    Cluster(int type, int size);
    Cluster(const Cluster& obj);
    ~Cluster();

    int clusterId;

    bool initMsg(const int);
    void runCluster();

    // Buffers
    std::vector<std::string> inBuffer; // Driver Pushes IN
    std::vector<std::string> outBuffer; // Driver Pops OUT
  private:
    std::vector<Agent*> agents;
};

#endif
