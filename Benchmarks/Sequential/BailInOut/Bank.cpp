#include "Bank.h"

//-------------------------Bank Implementation-------------------------------//
// Constructor/Destructor
Bank::Bank() : Agent() {
  clusterId = 1;
  recvFlag = false;
  srand(time(NULL));
  NetAssetValue = rand() % (int)5e5 + 1e6;
  TotalProposedLoans = 0;
}

Bank::Bank(std::vector<int> raw) {
  clusterId = raw[0];
  agentId = raw[1];
  NetAssetValue = raw[2];
  TotalProposedLoans = 0;
}

std::string Bank::initMsg(const int phase) {
  std::string retVal = "";
  if (phase == 0)
    TotalProposedLoans = 0;
  return retVal;
}

// Assumes msg was sent correctly
std::string Bank::runAgent(const std::vector<std::string>& msg) {
  // String Stream to hold values for a new message
  std::stringstream ss;

  // Recieved Request, or Recieved Confirmation
  if (msg[0] == "0-0-0") {
    ss << "0-1-1_" << msg[2] << "_C" << clusterId << "A" << agentId << "_" + msg[3];
    // Check
    if ( (NetAssetValue - TotalProposedLoans )/2 > atoi(msg[3].c_str()) ) {
      TotalProposedLoans += atoi(msg[3].c_str());
      std::cout << "Loan Recieved: " << ss.str() << "\n";
      return ss.str();
    }
    else return "";
  }
  else if (msg[0] == "0-2-1") {
    int firmLoan = atoi( (msg[3].substr(0,msg[3].length() -2)).c_str() );

    //Check if Firm has already been serviced
    if (msg[3].at(msg[3].length() - 1) == 'N') {
      TotalProposedLoans -= firmLoan;
    }
    else {
      std::size_t agentIdOffset = msg[2].find('A');
      int firmId = atoi( (msg[2].substr(agentIdOffset+1)).c_str() ); // CHECK

      // Update Assests/Loan
      NetAssetValue -= firmLoan;
      FirmAccounts.resize(firmId+1);

      // Finialize Loan
      FirmAccounts[firmId] = firmLoan;
      std::cout << agentId << " Loan Accepted: " << firmLoan << "\n";
      std::cout << agentId << " Net Asset Value " << NetAssetValue << "\n";
    }
    return "";
  }
}

std::vector<int> Bank::stripCopy() const {
  std::vector<int> retVal;
  retVal.push_back(clusterId);
  retVal.push_back(agentId);
  retVal.push_back(NetAssetValue);
  return retVal;
}
