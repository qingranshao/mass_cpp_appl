#ifndef SUBFUNCTIONLEADER_H
#define SUBFUNCTIONLEADER_H

#include "SubFunctionMember.h"
#include "Graph.h"
#include <vector>
#include <tuple>

class SubFunctionLeader : public SubFunctionMember {
  public:
    // Nested Inner-Class
    class Team {
      public:
        Team(int totalPeople, int avgIntel, int avgWage);
        ~Team();

        //void addMember(SubFunctionMember* member);
        //void addMember(int avgIntel, int avgWage);

        // Team Class Members
        int totalWages = 0;
        int totalIntel = 0;
        std::vector<SubFunctionMember*> members;
    };

    // Constructor & Destructor
    SubFunctionLeader(int id, int wageNum, int intel, int resources)
      : SubFunctionMember(wageNum, intel) {
      teamId = id;
      totalTeamResources = resources;
    }
    ~SubFunctionLeader() {
      delete team;
      currentProcess = nullptr;
    }

    // Class Members
    Team* team;
    int teamId;
    int totalTeamResources;
    Graph::Process* currentProcess = nullptr;

    Person::Message* runTeam();
    // tuple<reqRsc, reqWork, avgWage, avgIntel>
    std::tuple<int, int, int, int> processReqCond();
};
#endif