#ifndef SUBFUNCTIONMEMBER_H
#define SUBFUNCTIONMEMBER_H

#include "Person.h"

class SubFunctionMember : public Person {
  public:
    SubFunctionMember(int wageNum, int avgIntel) : Person(wageNum) {
      knowledgeNum = avgIntel;  // Modify for range of intelligence
    }
    int knowledgeNum;
};

#endif