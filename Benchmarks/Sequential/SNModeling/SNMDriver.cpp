#include "Graph.h"
#include <iostream>
int main(int argc, char* argv[]) {
  // Error Handling
  if (argc != 3) {
    std::cerr << " Want TABLE_SIZE, ITERATIONS" << "\n";
    std::exit(-1);
  }

  // Size check
  int SIZE = atoi(argv[1]);
  if (SIZE < 2) {
    std::cerr << "Not enough Agents" << "\n";
    std::exit(-1);
  }

  // Iteration check
  int ITERATION = atoi(argv[2]);
  if (ITERATION < 5) {
    std::cerr << "Not enough iterations" << "\n";
    std::exit(-1);
  }

  // Initialization
  Graph a(SIZE);
  a.displayDegrees();

  // Simulation
  for (int iter = 0; iter < ITERATION; iter++) {
     a.runAgents();
  }

  std::cin.ignore();

  return 0;
}
