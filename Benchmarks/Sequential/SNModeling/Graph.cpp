#include "Graph.h"

Graph::Graph(int size) {
  graphSize = size;
  agents = new Agent[graphSize];
  buildGraph();
  displayDegrees();
  degree_BFS();
}

void Graph::displayDegrees() const {
  for (int i = 0; i < graphSize; i++) {
    for (int j = 0; j < graphSize; j++) {
       std::cout << getVal(i,j) << ", ";
    }
    std::cout << "\n";
  }
}

void Graph::setVal(int src, int dest,const int val) {
  // Determines location of node, since matrix is triangular
  if (src < dest) adjGraph[src][dest-src] = val;
  else if (src > dest) adjGraph[dest][src-dest] = val;
}

int Graph::getVal(int src, int dest) const {
  // Determines location of node, since matrix is triangular
  if (src < dest) return adjGraph[src][dest-src];
  else if (src > dest) return adjGraph[dest][src-dest];
  else return -1;
}

bool Graph::isValid(int src, int dest) const {
   return (getVal(src,dest) == 0 );
}

// Runs a single run of communication
void Graph::runAgents() {
   srand(time(NULL));
   // Go through all agents
   for (int i = 0; i < graphSize; i++) {
      // Choose a random degree
      int degree = rand()%graphSize/8;

      // Find agents that is of that degree from Agent src
      for (int j = 0; j < graphSize; j++) {
         if (getVal(i,j) == degree) {
            // i -> j, first message in bidirectional messaging
            int msg = agents[i].sendMsgTo(j,degree);
            std::cout << msg << "\n";
            agents[j].recieveMsgFrom(i,degree,msg);

            // j -> i, second message in bidirectional messaging
            msg = agents[j].sendMsgTo(i,degree);
            std::cout << msg << "\n";
            agents[i].recieveMsgFrom(j,degree,msg);
            break;
         }
      }
   }
}

void Graph::buildGraph() {
  int size = graphSize;
  // Initializing rows
  adjGraph = new int*[size];
  // Initializing Symmetric --> Triangular Matrix
  for (int i = 0; i < size; i++) {
     adjGraph[i] = new int[size-i];
  }

  // Randomly Generate 
  srand(time(NULL));
  for (int m = 0; m < size; m++) {
     int spots = rand()%(size);
     for (int n = 0; n < spots; n++) {
        int spot = rand()%size;
        // Sets node if valid
        if (isValid(m,spot)) setVal(m,spot,1);
     }
  }
}

void Graph::degree_BFS() {
  std::cout << "Begun BFS \n";

  // Run per Node
  for (int i = 0; i < graphSize; i++) {
    std::cout << "Agent: " << i << "\n";

    int degree = 1;
    std::queue<int> process;
    std::queue<int> next;

    // Initialization of the modified Breadth-First Search
    process.push(i);

    // A modified Breadth-First Search
    while (!process.empty()) {
      // Acquire Reference to an Agent (for its neighbors)
      int current = process.front();
      process.pop();

      // Search for Adjacent Agents
      for (int m = 0; m < graphSize; m++) {
        // Adjacent Agent and Redundancy Check
        if (getVal(current,m) == 1) {
          if (current == i) next.push(m);
          else if (isValid(i,m)) {
            // Update for Agent i
            setVal(i,m,degree);
            next.push(m);
          }
        }
      }

      // Check for next degree
      if (process.empty()) {
        // Swapped inorder distinguish same degree'd neighbors
        process.swap(next);
        degree++;
      }
    }
  }
}
