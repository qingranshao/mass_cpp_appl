#include <iostream> // cin/cout/cerr/endl
//#include <stdlib.h> // exit
#include <cstdlib> // exit not in c++11
#include "BrainGrid.h"
#include "Neuron.h"

int main(int argc, char* argv[])
{
  if(argc != 3)
  {
    std::cerr << "Want TABLE_SIZE, ITERATIONS" << std::endl;
    std::exit(-1);
  }
  else if(atoi(argv[1]) < 9)
  {
    std::cerr << "Want TABLE_SIZE to be at least 9" << std::endl;
    std::exit(-1);
  }

  // User Input
  int size = atoi(argv[1]);
  int iter = atoi(argv[2]);

  // Initialization
  BrainGrid Grid(size);

  // Print Out Stage
  Grid.DisplayStage();
  std::cout << std::endl;
  // Print Out Type
  Grid.DisplayType();
  std::cout << std::endl;

  // Simulate
  Grid.Simulate(iter);

  // Print Out Stage
//  Grid.DisplayStage();
//  std::cout << std::endl;
  // Print Out Type
//  Grid.DisplayType();
//  std::cout << std::endl;

  return 0;
}// End of main
