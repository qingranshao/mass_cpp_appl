#include "BrainGrid.h"

// Constructor
BrainGrid::BrainGrid(const int Size) {
  size = Size;
  Grid = new Neuron*[size];
  for (int i = 0; i < size; i++) {
    Grid[i] = new Neuron[size];
  }
}
// Destructor
BrainGrid::~BrainGrid() {
  for (int i = 0; i < size; i++) {
    delete[] Grid[i];
  }
  delete[] Grid;
}

// Display Stage
void BrainGrid::DisplayStage() const {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      switch (Grid[i][j].neuronStage) {
        case 0:
        case 1:
          std::cout << " " << Grid[i][j].neuronStage;
          break;
        case 2:
          std::cout << " E";
          break;
        case 3:
          std::cout << " C";
          break;
        case 4:
          std::cout << " S";
          break;
        case 5:
          std::cout << " I";
          break;
      }
    }
    std::cout << std::endl;
  }
}

// Display Type
void BrainGrid::DisplayType() const{
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      switch (Grid[i][j].neuronType) {
        case 0:
          std::cout << " A";
          break;
        case 1:
          std::cout << " I";
          break;
        case 2:
          std::cout << " N";
          break;
      }
    }
    std::cout << std::endl;
  }
}
// Torus Safe (modified to accomodate longer jumps)
int BrainGrid::TorusSafe(int cell) {
  if (cell < 0) return size - 1;
  else if (cell > size - 1) return 0;
  return cell;
}

// Assumptions ACTIVE to ACTIVE pre-handled
// Returns -1 not safe, 0 safe, 1 connect
BrainGrid::GrowthFlag BrainGrid::SafeGrowth(Neuron& activeSrc, Neuron& dest) {
  // Basic Stop / Safe or Indepth Safety Search
  if (dest.neuronType == Neuron::INACTIVE
    || dest.neuronType == Neuron::ACTIVE
    || dest.neuronStage == Neuron::STOPPED
    || dest.neuronStage == Neuron::CONNECTED) return BrainGrid::STOP;
  else if (dest.neuronStage == Neuron::INDEF) return BrainGrid::SAFE;
  else {
    // Finding dest's Active
    Neuron* next = dest.neighbors[0];
    while (next->neuronType != Neuron::ACTIVE) next = next->neighbors[0];

    int cntCont = 0;
    for (int i = 0; i < 8; i++) {
      // Skip Unused Neighbor & Redundant Path
      if (next->neighbors[i] == nullptr || i == dest.nextCell) {
        cntCont++;
        continue;
      }

      // Trivial / Complex / No Connection to activeSrc
      if (next->neighbors[i] == &activeSrc) return BrainGrid::STOP;
      else if (next->neighbors[i]->neuronStage == Neuron::CONNECTED) {
        Neuron* tmp = next->neighbors[i];

        // Traverse the Neuron for Connected ACTIVE Neuron
        while (tmp->neuronType != Neuron::ACTIVE) {
          if (tmp->neighbors[2] != nullptr) tmp = tmp->neighbors[2];
          else tmp = tmp->neighbors[1];
        }

        // Determined redundant connection
        if (tmp == &activeSrc) return BrainGrid::STOP;
      } // End check for next->neighbor[i]'s ACTIVE
      else cntCont++;
    } // End CONNECTED check of dest's ACTIVE Neuron

    // Determine if Connection is viable, or unviable (i.e. to self)
    if (cntCont == 8) return BrainGrid::STOP;
    else return BrainGrid::CONNECT;
  }
} // End Safe Growth

// Growth pattern to initiate during the first iteration
void BrainGrid::GrowActive(int i, int j) {
  int index = 0;
  for (int row = i - 1; row <= i + 1; row++) {
    for (int col = j - 1; col <= j + 1; col++) {
      // Skip Self
      if(row == i && col == j) continue;

      // Torus
      int tmpRow = TorusSafe(row);
      int tmpCol = TorusSafe(col);

      // Trivial ACTIVE CONNECTION
      if (Grid[tmpRow][tmpCol].neuronType == Neuron::ACTIVE
        && Grid[tmpRow][tmpCol].neuronStage != Neuron::ENACTED) {
        // Bidirectional Growth
        Grid[i][j].neighbors[index] = &Grid[tmpRow][tmpCol];
        Grid[tmpRow][tmpCol].neighbors[7 - index] = &Grid[i][j];
      }
      else if (Grid[tmpRow][tmpCol].neuronType == Neuron::NEUTRAL) {
         switch (SafeGrowth(Grid[i][j],Grid[tmpRow][tmpCol])) {
           case BrainGrid::SAFE:
             // Bidirectional Growth
             Grid[i][j].neighbors[index] = &Grid[tmpRow][tmpCol];
             Grid[tmpRow][tmpCol].neighbors[0] = &Grid[i][j];
             // Next Growth Phase Prep
             Grid[tmpRow][tmpCol].nextCell = index;
             Grid[tmpRow][tmpCol].neuronStage = Neuron::VISIT2;
             break;
           case BrainGrid::CONNECT:
             // Bidirectional Growth
             if (Grid[tmpRow][tmpCol].neighbors[1] == nullptr) {
               Grid[i][j].neighbors[index] = &Grid[tmpRow][tmpCol];
               Grid[tmpRow][tmpCol].neighbors[1] = &Grid[i][j];
             }
             else {
               Grid[i][j].neighbors[index] = &Grid[tmpRow][tmpCol];
               Grid[tmpRow][tmpCol].neighbors[2] = &Grid[i][j];
             }
             // Set CONNECTED
             Grid[tmpRow][tmpCol].neuronStage = Neuron::CONNECTED;
             break;
           case BrainGrid::STOP:
             // Leave neighbor NULL
           default: break;
         }
      } // End NEUTRAL Growth

      index++;
    } //End Col
  } // End Row
  Grid[i][j].neuronStage = Neuron::ENACTED;
} // End GrowActive

// Growth pattern to perpetuate after first iteration
void BrainGrid::GrowNeutral(int i, int j) {
  int tmpRow, tmpCol;
  // Find Dest Row
  switch (Grid[i][j].nextCell) {
    case 0: case 1: case 2:
      tmpRow = TorusSafe(i-1);
      break;
    case 5: case 6: case 7:
      tmpRow = TorusSafe(i+1);
      break;
    case 3: case 4:
      tmpRow = i;
    default: break;
  }
  // Find Dest Col
  switch (Grid[i][j].nextCell) {
    case 0: case 3: case 5:
      tmpCol = TorusSafe(j-1);
      break;
    case 2: case 4: case 7:
      tmpCol = TorusSafe(j+1);
      break;
    case 1: case 6:
      tmpCol = j;
    default: break;
  }

  // Find src's Active
  Neuron* srcActive = Grid[i][j].neighbors[0];
  while (srcActive->neuronType != Neuron::ACTIVE) srcActive = srcActive->neighbors[0];

  // Grid[tmpRow][tmpCol] (i.e. dest)
  switch (SafeGrowth(*srcActive,Grid[tmpRow][tmpCol])) {
    case BrainGrid::STOP:
      Grid[i][j].SetStop();
      break;
    case BrainGrid::SAFE:
      // Bidirectional Growth
      Grid[i][j].neighbors[1] = &Grid[tmpRow][tmpCol];
      Grid[tmpRow][tmpCol].neighbors[0] = &Grid[i][j];
      // Next Growth Phase Prep
      Grid[tmpRow][tmpCol].nextCell = Grid[i][j].nextCell;
      Grid[tmpRow][tmpCol].neuronStage = (Neuron::Stage) ((Grid[i][j].neuronStage + 1) % 2);
      break;
    case BrainGrid::CONNECT:
      // Bidirectional Growth
      if (Grid[tmpRow][tmpCol].neighbors[1] == nullptr) {
        Grid[i][j].neighbors[1] = &Grid[tmpRow][tmpCol];
        Grid[tmpRow][tmpCol].neighbors[1] = &Grid[i][j];
      }
      else {
        Grid[i][j].neighbors[1] = &Grid[tmpRow][tmpCol];
        Grid[tmpRow][tmpCol].neighbors[2] = &Grid[i][j];
      }
      // Connect all directions
      Grid[tmpRow][tmpCol].SetConnect();
      break;
    default: break;
  }
  Grid[i][j].neuronStage = Neuron::ENACTED;
} // End GrowNeutral

// Simulate
void BrainGrid::Simulate(const int iter) {
  // Simulation
  for (int ITER = 0; ITER < iter; ITER++) {
    Neuron::Stage curStage = (Neuron::Stage)(ITER % 2);
    Neuron::Stage nextStage = (Neuron::Stage)((ITER + 1) % 2);
    // Traverse 2D Array
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        // Check pre-existing synapes
        if (Grid[i][j].neuronStage == curStage) {
          // Choose appropriate Growth Pattern
          if (Grid[i][j].neuronType == Neuron::ACTIVE) GrowActive(i,j);
          else if (Grid[i][j].neuronType == Neuron::NEUTRAL) GrowNeutral(i,j);
        }
      }
    } // Finished Iteration Traversal

  std::cout << "Iteration: " << ITER << std::endl;
  DisplayStage();
  std::cout << std::endl;

  } // Finished Iterations runs
} // Finished Simulation Method
