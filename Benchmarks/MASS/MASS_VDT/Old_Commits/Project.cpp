
#include "Project.h"
#include "MASS_base.h"
#include <stdlib.h>  //rand
#include <sstream>     // ostringstream
#include <fstream>


extern "C" Place* instantiate( void *argument ) {
  return new Project( argument );
}

extern "C" void destroy( Place *object ) {
  delete object;
}


/**
 * Initializes a Life object.
 */
void *Project::init( void *argument ) {
  
  //Define cardinals
  int north[2]  = {0, 1};  cardinals.push_back( north );
  int east[2] = {1, 0};  cardinals.push_back( east );
  int south[2]  = {0, -1}; cardinals.push_back( south );
  int west[2] = {-1, 0}; cardinals.push_back( west );
  int northwest[2] = { -1, 1 };  cardinals.push_back(northwest);
  int northeast[2] = { 1, 1 };  cardinals.push_back(northeast);
  int southwest[2] = { -1, -1 };  cardinals.push_back(southwest);
  int southeast[2] = { 1, -1 };  cardinals.push_back(southeast);

  for (int i = 0; i < NUM_NEIGHBORS; i++)
     neighborHealthStatus[i] = -1;


  totalProjectRequiredMoney = 0;

  status = -1;

  outMessage = NULL;
  outMessage_size = 0;
  
  return NULL;
}

  const int Project::neighbor[8][2] = {{0,1}, {1,0}, {0,-1}, {-1,0}, {-1,1}, {1,1}, {-1,-1}, {1,-1}};



/** (void)
*	Set the outmessage as health of this life
*/
void *Project::displayAgentInfectionStatus()
{


   int numAgents = agents.size();
   bool hasCleaning = false;

   // if agents exist on Grid
   if (numAgents > 0) {

      for (int i = 0; i < numAgents; i++) {
         Agts* ag = (Agts*)agents.at(i);
         hasCleaning = (ag->agentType == 0) ? true : false;
      }

      if (hasCleaning) {
         status = 0;	
      }
      else {
         status = 1; 
      }
   }
   else {	
      status = -1;	
   }

   // update Grid's outMessage with new status
   outMessage = new int();
   *(int*)outMessage = status;

   return NULL;
}


/** (void)
*	Set the outmessage as health of this life
*/
void *Project::displayHealthStatus()
{
   outMessage_size = sizeof(int);
   outMessage = new int();
   *(int *)outMessage = (int)status;

   ostringstream convert;
   
   int numAgents = agents.size();
   bool hasLeader = false;


   if (numAgents > 0) {
      for (int i = 0; i < numAgents; i++) {
         Agts* agts = (Agts*)agents.at(i);
         hasLeader = (agts->agentType == 1) ? true : false;
      }

      if (hasLeader) {
         status = 2;	
      }
      else {
         status = 1; 
      }
   }
   else {	
      status = 0;	
   }

   convert << "status of type of agents in project(2 = has leader, 1 = no leader, 0 = no agents): " << status << endl;

   MASS_base::log( convert.str( ) );

   return NULL;
}


/** (void)
*	Set the outmessage as health of this life
*/
void *Project::displayTotalProjectRequiredMoney()
{
   int numAgents = agents.size();

   // if agents exist on Grid
   if (numAgents > 0) {
      //  check for banks in Grid
      for (int i = 0; i < numAgents; i++) {
         Agts* agts = (Agts*)agents.at(i);
         if (agts->agentType == 1) {
            totalProjectRequiredMoney = agts->totalProjectRequiredMoney;
         }
      }
   }

   return NULL;
}


void *Project::getBoundaryHealthStatus()
{
   //Record neighbor population as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, North);
   neighborHealthStatus[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborHealthStatus[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborHealthStatus[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborHealthStatus[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northwest);
   neighborHealthStatus[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborHealthStatus[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborHealthStatus[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborHealthStatus[7] = (ptr == NULL) ? 0 : *ptr;


   return NULL;
}
