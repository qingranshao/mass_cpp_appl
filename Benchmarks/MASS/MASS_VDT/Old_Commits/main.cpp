
#include "MASS.h"
#include "Agts.h"
#include "Project.h"
#include "Timer.h"	//Timer
#include <stdlib.h> // atoi
#include <unistd.h>
#include <vector>

const int iterPerTurn = 9;							//Partitioned movespaces are 9 squares large
const bool printOutput = true;

Timer timer;

int main( int argc, char *args[] ) {

	// check that all arguments are present 
	// exit with an error if they are not
	if ( argc != 10 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads numTurns sizeX sizeY" << endl;
		return -1;
	}
  
	// get the arguments passed in
	char *arguments[4];
	arguments[0] = args[1]; // username
	arguments[1] = args[2]; // password
	arguments[2] = args[3]; // machinefile
	arguments[3] = args[4]; // port
	int nProc = atoi( args[5] ); // number of processes
	int nThr = atoi( args[6] );  // number of threads
  
	const int numTurns = atoi( args[7] );	//Run this simulation for numTurns
	const int sizeX = atoi( args[8] );
	const int sizeY = atoi( args[9] );
	const double rmSpawn = 0.2;
	const int myPopulation = sizeX * sizeY * rmSpawn * rmSpawn;	//Population{RMsize, RMSpawn}
 	static const int totalSize = sizeX * sizeY;							//Total size of map

	// initialize MASS with the machine information,
	// number of processes, and number of threads
	MASS::init( arguments, nProc, nThr ); 

	// prepare a message for the places (this is argument below)
	char *msg = (char *)("hello\0"); // should not be char msg[]
  
	/*  THIS SECTION OF CODE DEALS ONLY WITH PLACES  */
  
	// Create the places.
	// Arguments are, in order:
	//    handle, className, boundary_width, argument, argument_size, dim, ...
	Places *project = new Places( 1, "Project", 1, msg, 6, 2, sizeX, sizeY );

 
	// define the destinations, which represent the Places 
	// adjacent to a particular place (represented by [0, 0].
	//        [0, 1]                 [ north]
	// [-1, 0][0, 0][1, 0]  == [west][origin][east]
	//        [0,-1]                 [ south]
	// Each X is represent by an array containing its coordinates.
	// Note that you can have an arbritrary number of destinations.  For example,
	// northwest would be [-1,1].
	vector<int*> destinations;
	int north[2] = {0, 1};  destinations.push_back( north );
	int east[2]  = {1, 0};  destinations.push_back( east );
	int south[2] = {0, -1}; destinations.push_back( south );
	int west[2]  = {-1, 0}; destinations.push_back( west );
   int northwest[2] = { -1, 1 };  destinations.push_back( northwest );
   int northeast[2] = { 1, 1 };  destinations.push_back( northeast );
   int southwest[2] = { -1, -1 };  destinations.push_back( southwest );
   int southeast[2] = { 1, -1 };  destinations.push_back( southeast );
  
   Agents *normWorker = new Agents(2, "Agts", msg, 6, project, 3000);
   normWorker->callAll(Agts::initialize_);

   Agents *leader = new Agents(3, "Agts", msg, 6, project, 100);
   leader->callAll(Agts::initialize_);

   Agents *moneyAgent = new Agents(4, "Agts", msg, 6, project, 1);
   moneyAgent->callAll(Agts::initialize_);

	project->callAll(Project::init_);				//initialize life
  
	timer.start();
		
   project->callAll(Project::displayHealthStatus_);
   
   leader->callAll(Agts::updateLeaderInitialState_);
   leader->manageAll();

   leader->callAll(Agts::displayAgentStatus_);
   moneyAgent->callAll(Agts::displayAgentStatus_);

   moneyAgent->manageAll();

	for(int turn = 0; turn < numTurns; turn++)
	{    
      leader->callAll(Agts::verifyUpdateProjectResources_);
      normWorker->callAll(Agts::verifyUpdateProjectResources_);
      leader->manageAll();
      normWorker->manageAll();

      leader->callAll(Agts::updateAgentStatus_);

      normWorker->callAll(Agts::performWork_);
      leader->callAll(Agts::performWork_);

      moneyAgent->callAll(Agts::updateTotalMoney_);

      normWorker->callAll(Agts::moveAgent_);
      leader->callAll(Agts::moveAgent_);

      leader->manageAll();
      normWorker->manageAll();
      moneyAgent->manageAll();

      leader->callAll(Agts::obtainProjectRequirements_);
      leader->manageAll();
	}
	
	std::cerr << "Health Status after " << numTurns << std::endl;
	int* maxTurns = new int;
	*maxTurns = numTurns;
	
	long elaspedTime_END =  timer.lap();
	printf( "\nEnd of simulation. Elasped time using MASS framework with %i processes and %i thread and %i turns :: %ld \n",nProc,nThr,numTurns, elaspedTime_END);
  
	MASS::finish( );
}
