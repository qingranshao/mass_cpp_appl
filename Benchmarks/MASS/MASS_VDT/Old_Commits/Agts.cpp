#include "Agts.h"
#include "MASS_base.h"

extern "C" Agent* instantiate( void *argument ) {
  return new Agts( argument );
}

extern "C" void destroy( Agent *object ) {
  delete object;
}

// set up cardinal direction's relative indexes
int Agts::cardinals[8][2] = { { 0,1 },{ 1,0 },{ 0,-1 },{ -1,0 },{ -1,1 },{ 1,1 },{ -1,-1 },{ 1,-1 } };

// initialize
// initialize the Agent
void *Agts::initialize(void *argument) {

   if (agentsHandle == 4) {
      agentType = 2;
   }
   else {
      agentType = (agentsHandle == 2) ? 0 : 1;	
   }
   counter = 0;
   
   if (agentType == 2) {
      totalOverallMoney = 5500000;
   }

   agentEnergy = STARTING_ENERGY;			
   if (agentType == 1) {
      
      numWorkers = rand() % 10 + 21;
      totSalary = rand() % 10 + 11;

      durWork = rand() % 20 + 10;
      jobNum = rand() % 30; 
      jobIndex = rand() % 10; 
      totalProjectRequiredMoney = (3 * numWorkers) + totSalary;
   }
   else {
      numWorkers = -1;
      totSalary = 3;
      //totIntell = -1;
      durWork = -1;
      jobNum = -1;
      jobIndex = -1;
   }


   


	return NULL;
}

// updateAgentStatus
// update agent for current time period
void *Agts::updateAgentStatus(void *argument) {
   // if leader
   int numAgents = place->agents.size();

   if (agentType == 1) {

      int workerCount = 0;
      int leaderCount = 0;

      // if multiple agents in Grid
      //if (numAgents > 1) {
      for (int i = 0; i < numAgents; i++) {
         Agts* agts = (Agts*)place->agents.at(i);
         if (agts->agentType == 1) {	
            leaderCount++;
         }
         else {						
            workerCount++;
         }
      }

      if (workerCount > 0) {
         for (int i = 0; i < numAgents; i++) {
            Agts* agts = (Agts*)place->agents.at(i);
            if (agts->agentType != 1) {	//a worker
               int salary = rand() % 150 + 400;
               agts->callMethod(Agts::updateAgentWorkDuration_, (int*)durWork); 
               agts->callMethod(Agts::updateAgentIndexNum_, (int*)jobIndex);
               agts->callMethod(Agts::updateAgentSalaryNum_, (int*)salary);
            }
         }
      }
   }
   return NULL;
}


// updateAgentStatus
// update agent for current time period
void *Agts::obtainProjectRequirements(void *argument) {
   // if leader
   int numAgents = place->agents.size();

   if (agentType == 1 && place->index[1] == counter) {
      if (agentType == 1) {

         numWorkers = rand() % 10 + 21;
         totSalary = rand() % 10 + 11;


         durWork = rand() % 20 + 10;

         totalProjectRequiredMoney = (3 * numWorkers) + totSalary;
      }
   }


   return NULL;
}


// updateAgentStatus
// update agent for current time period
void *Agts::performWork(void *argument) {

   ostringstream convert;



   if (agentType != 1 && jobIndex == counter) {
      for (int i = 0; i < durWork; i++) {
      }
   }


   MASS_base::log(convert.str());
   counter++;
   return NULL;
}

// updateAgentStatus
// update agent for current time period
void *Agts::updateTotalMoney(void *argument) {

   if (agentType == 2) {

      for (int j = 0; j < 100; j++) {
         int numAgents = place->agents.size();
         for (int i = 0; i < numAgents; i++) {
            Agts* agts = (Agts*)place->agents.at(i);
            if (agts->agentType == 1) {
               totalOverallMoney -= agts->totalProjectRequiredMoney;
            }
         }



         int dx = cardinals[1][0];
         int dy = cardinals[0][1];

         // move agent to new location
         vector<int> dest;
         if (j == 99) {
            dest.push_back(0);
            dest.push_back(place->index[1] + dy);
            migrate(dest);
         }
         else {
            dest.push_back(place->index[0] + dx);
            dest.push_back(place->index[1]);
            migrate(dest);
         }
      }


      ostringstream convert;
      convert << "total remaining company money: " << totalOverallMoney << endl;
      MASS_base::log(convert.str());
   }
   return NULL;
}


// moveAgent
// move agent to new location in grid
void *Agts::moveAgent(void *argument) {
   if (alive && agentType != 2) {	// if agent is not dead

      // get cardinal direction's relative index
      int dy = cardinals[0][1];

      // move agent to new location
      vector<int> dest;
      dest.push_back(place->index[0]);
      dest.push_back(place->index[1] + dy);
      migrate(dest);


   }

   counter++;
	return NULL;
}

// checkAgentStatus
// check the status of agent to see if it should be dead
void *Agts::checkAgentStatus(void *argument) {

	// if agentEnergy has reached 0, kill agent
	if (agentEnergy <= 0) {
		kill();
	}

	return NULL;
}

// killAgent
// kill this agent
void *Agts::killAgent(void *argument) {
   if (agentEnergy <= 0) {
      kill();
   }
	return NULL;
}



//displays agent status
void *Agts::updateLeaderInitialState(void *argument) {
   // if leader
   if (agentType == 1) {
      int dx = cardinals[1][0];
      int dy = cardinals[0][1];

      vector<int> dest;

      dest.push_back((int)agentId);
      dest.push_back(0);
      migrate(dest);
   }
   return NULL;
}


//displays agent status
void *Agts::displayAgentStatus(void *argument) {
   ostringstream convert;
   // if leader
   if (agentType == 1) {

      //this is for standard console output
      convert << "total required workers: " << numWorkers
         << ". total required Salary: " << totSalary << ". total work duration: " << durWork << endl;

   } 

   if (agentType == 2) {
      vector<int> dest;
      dest.push_back(0);
      dest.push_back(0);
      migrate(dest);
   }

   MASS_base::log(convert.str());
   return NULL;
}


void *Agts::updateAgentWorkDuration(void *argument) {
   durWork = *((int*)(&argument));
   return NULL;
}

void *Agts::updateAgentIndexNum(void *argument) {
   jobIndex = *((int*)(&argument));
   return NULL;
}

void *Agts::updateAgentSalaryNum(void *argument) {
   totSalary = *((int*)(&argument));
   return NULL;
}



void *Agts::verifyUpdateProjectResources(void *argument) {
 
   // if leader
   int numAgents = place->agents.size();


   if (agentType == 1 && place->index[1] == counter) {

      int currWorkers = 0;
      int leaderCount = 0;

      // if multiple agents in Grid
      //if (numAgents > 1) {
      for (int i = 0; i < numAgents; i++) {
         Agts* agts = (Agts*)place->agents.at(i);
         if (agts->agentType == 1) {	
            leaderCount++;
         }
         else {						
            currWorkers++;
         }
      }
      int missingWorkers = 0;

      if (numWorkers > currWorkers) {
         missingWorkers = numWorkers - currWorkers;
      }

      int numToSpawn = missingWorkers;

      vector<void*> arguments;
      arguments.push_back((char *)("helloo\0"));   // give the message �hello� to each agent.
      spawn(numToSpawn, arguments, 7);  // spawn one child agent, with the message hello.
   }
   return NULL;
}