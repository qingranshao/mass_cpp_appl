#include "Team.h"

extern "C" Place* instantiate(void* argument) {
  return new Team(argument);
}

extern "C" void destroy(Place* teamObj) {
  delete teamObj;
}


//----------------------------------- init -------------------------------------
/** Initializes the Team to default values
 *  @param argument - a pair<int,int> where the first int tells how many 
 *         production tasks there will be, and the second int tells how many
 *         collaboration tasks there will be.                        
 */
void* Team::init(void* numTasks) {
  pair<int,int> numberOfTasks = *(pair<int,int>*)numTasks;
  numProductionTasks = numberOfTasks.first;
  numCollabTasks = numberOfTasks.second;
  day = hour = 0;
  productionTasks.resize(5);
  for (int i = 0; i < 5; i++) 
    members[i] = 0;
}


//------------------------------ initProduction --------------------------------
/** Initializes the Team's production Tasks.
 *  @param taskArgs - a Task* that ponts to an array of Tasks of size 
 *         numProductionTasks.                          
 */
void* Team::initProduction(void* tasks) {
  Task* tasklist = (Task*)tasks;
  for (int curTask = 0; curTask < numProductionTasks; curTask++) {
    Task* task = new Task(tasklist[curTask]); // Clone task
    bool taskInserted = false;
    for (int trayLvl = 0; trayLvl < 5; trayLvl++) {
      if (task->getEngrHours(trayLvl) > 0) {
        productionTasks[trayLvl].push_back(task);
        taskInserted = true;
        break;
      }
    }
    if (!taskInserted) {
      delete task;
    }
  }
}






//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Debug Methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void* Team::testConstruction() {
  logText.str("");
  logText << "Place " << index[0] << ", " << index[1] << " member data" << endl;
  logText << "#Production Tasks: " << numProductionTasks << endl;
  logText << "#Collaboration Tasks: " << numCollabTasks << endl;
  MASS_base::log(logText.str());
  return nullptr;
}


void* Team::testProdInit() {
  logText.str("");
  logText << "Place " << index[0] << ", " << index[1] << " production tasks" << endl;
  for (int engType = 0; engType < productionTasks.size(); engType++) {
    logText << "Engineer Tray: " << engType << endl;
    for (int curTask = 0; curTask < productionTasks[engType].size(); curTask++) {
      logText << "  Type: " << productionTasks[engType][curTask]->getType();
      logText << "  Priority: " << productionTasks[engType][curTask]->getPriority();
      logText << "  Total Hours: " << productionTasks[engType][curTask]->getTotalHours();
      logText << "  TaskId: " << productionTasks[engType][curTask]->getTaskId();
      logText << "  ProjectId: " << productionTasks[engType][curTask]->getProjectId() << endl;
    }
  }
  MASS_base::log(logText.str());
  return nullptr;
}

// End Debug Section