#include "Task.h"

using namespace std;


bool Task::makeProgress(int engType) {
  if (hours[engType] > 0) {
    hours[engType] -= 1;
    totalHours -= 1;
    return true;
  }
  else {
    return false;
  }
}