/*==============================================================================
  Team Class
  Author: Ian Dudder
================================================================================
  The Team Class takes the role of a Place in the simulation. Each Team will 
  have 25 Engineer Agents, 5 of each type. Each team will keep track of all the
  projects assigned to it and monitor them for completion. Additionally, this 
  Class will maintain the current hour and day for the simulation and keep it 
  synchronized across all Engineers.
==============================================================================*/

#ifndef TEAM_H
#define TEAM_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include "Task.h"
#include "Project.h"
#include <vector>
#include <unordered_map>


class Team : public Place {
public:
  //=============================== Function Ids ===============================
  static const int init_ = 0;         // Initializes the Team with projects
  static const int addEngineer_ = 1;  // Helps create the Engineer Hierarchy
  static const int act_ = 2;          // Acts a day in the simulation
  static const int getTime_ = 3;      // Returns the hour and day 
  static const int assignTask_ = 4;   // Assigns a new task to an Engineer

  //============================== Public Methods ==============================
  Team(void* argument) : Place(argument) {} 

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {      
      case init_ :
        return init(argument);

      case addEngineer_ :
        return addEngineer();

      case act_ :
        return act(argument);

      case getTime_ :
        return getTime();

      case assignTask_ :
        return assignTask(argument);

      default :
        return badFunctionId(functionId);
    }
  }

private:
  //=========================== CallMethod Helpers =============================

  //---------------------------------- init ------------------------------------
  /** Initializes the Team with all the tasks needed to set up the projects.
   *  The tasklist will be received as an array of Task objects. These Tasks
   *  will be grouped by their projectId and stored in the projects data member.
   *  @param tasklist - an array of Task* objects.                            
   */
  void* init(void* tasklist);


  //------------------------------- addEngineer --------------------------------
  /** Helps the Engineer Agents determine their type. This method will mutex 
   *  lock and search through the 'members' vector and stop at the first entry
   *  that is less than 5. Then it will increment the entry and return the index
   *  back to the Engineer to indicate the Engineer type they should be.      
   *  @return - returns an integer corresponding to an Engineer type.         
   */
  void* addEngineer();


  //---------------------------------- act -------------------------------------
  /** Carries out the necessary tasks for a day in the simulation.            
   *  @param finished - indicates if all projects are complete or not.        
   */
  void* act(void* finished);


  //-------------------------------- getTime -----------------------------------
  /** Returns the current hour and day to the caller.
   *  @return - returns a pair<int,int> with the first item as the hour and the
   *            second item as the day.                                       
   */
  void* getTime() const;

  
  //------------------------------- assignTask ---------------------------------
  /** Gets a Task of the calling engineer's level from any of the active 
   *  projects.
   *  @param engType - the type of the engineer making the call.
   *  @return - returns a Task* for the calling Engineer's level.                               
   */
  void* assignTask(void* engType);


  //----------------------------- badFunctionId --------------------------------
  /** Prints a MASS_log if an invalid functionId was called.                  
  */
  void* badFunctionId(int functionId) const;


  //============================= Private Methods ==============================

  //----------------------------- incrementTime --------------------------------
  /** Advances the current hour by 1 and increments the day as needed.        
  */
  void incrementTime();

  //------------------------------- isFinished ---------------------------------
  /** Checks through the status of all projects to see if all projects are 
   *  complete.
   *  @return - Returns true if all projects are complete. Returns false if any
   *            task still needs work.                                        
   */
  bool isFinished;
  

  //================================ Member Data ===============================
  int hour;                 // Current hour in the simulation
  int day;                  // Current day of the simulation
  vector<int> members;      // Size 5. Each index gives #engineers of that type.
  unordered_map<int /*projectId*/, Project*> projects; // Assigned projects
  ostringstream logText;    // Used to create a MASS log
};


#endif //TEAM_H