#include "Team.h"

extern "C" Place* instantiate(void* argument) {
  return new Team(argument);
}

extern "C" void destroy(Place* teamObj) {
  delete teamObj;
}


