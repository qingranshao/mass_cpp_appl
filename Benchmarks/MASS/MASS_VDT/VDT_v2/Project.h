/*==============================================================================
  Project Class
  Author: Ian Dudder
================================================================================
  The Project Class is comprised of a number of Tasks for a team of Engineer
  Agents to complete. This class maintains a tray of Production Tasks for each 
  Engineer type to allow tasks to move down the hierarchy. This class also has
  a list of Collaborative Tasks scheduled for Engineer Agents. When a Task has
  passed through the hierarchy, it is moved into the completed tasks out tray.
  Once all tasks are completed, the project is complete.
==============================================================================*/

#ifndef PROJECT_H
#define PROJECT_H

#include <iostream>
#include "Task.h"
#include <deque>
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;


class Project {
public:
  /*============================== Public Methods ============================*/
  Project();

  Project(int projId);

  Task* selectTask(int engType);

  Task* getNextCollab();

  bool sendToTray(int engType, Task* task);

  void sendToCompleted(Task* task);

  bool isFinished();

private:
  /*============================= Private Methods ============================*/

  /*================================ Member Data =============================*/
  int projectId;                  /* Unique Project Identifier */

  queue<Task*> collabTasks;       /* Queue of Collaborative Tasks */

  /* The in-trays of Tasks for each type of Engineer Agent. */
  unordered_map<int /*engr type*/, deque<Task*> /*Task Tray*/> productionTasks; 

  vector<Task*> completedTasks;   /* Vector of completed tasks */ 
};


#endif //PROJECT_H