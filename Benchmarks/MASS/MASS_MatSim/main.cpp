
#include "MASS.h"
#include "Street.h"
#include "Vehicle.h"
#include "Timer.h"	//Timer
#include <stdlib.h> // atoi
#include <unistd.h>
#include <vector>

const int iterPerTurn = 9;							//Partitioned movespaces are 9 squares large
const bool printOutput = true;

Timer timer;

int main( int argc, char *args[] ) {

	// check that all arguments are present 
	// exit with an error if they are not
	if ( argc != 10 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads numTurns sizeX sizeY" << endl;
		return -1;
	}
  
	// get the arguments passed in
	char *arguments[4];
	arguments[0] = args[1]; // username
	arguments[1] = args[2]; // password
	arguments[2] = args[3]; // machinefile
	arguments[3] = args[4]; // port
	int nProc = atoi( args[5] ); // number of processes
	int nThr = atoi( args[6] );  // number of threads
  
	const int numTurns = atoi( args[7] );	//Run this simulation for numTurns
	const int sizeX = atoi( args[8] );
	const int sizeY = atoi( args[9] );

	// initialize MASS with the machine information,
	// number of processes, and number of threads
	MASS::init( arguments, nProc, nThr ); 

	// prepare a message for the places (this is argument below)
	char *msg = (char *)("hello\0"); // should not be char msg[]
  
	/*  THIS SECTION OF CODE DEALS ONLY WITH PLACES  */
  
	// Create the places.
	// Arguments are, in order:
	//    handle, className, boundary_width, argument, argument_size, dim, ...
	Places *streets = new Places( 1, "Street", 1, msg, 6, 2, sizeX, sizeY );

 
	// define the destinations, which represent the Places 
	// adjacent to a particular place (represented by [0, 0].
	//        [0, 1]                 [ north]
	// [-1, 0][0, 0][1, 0]  == [west][origin][east]
	//        [0,-1]                 [ south]
	// Each X is represent by an array containing its coordinates.
	// Note that you can have an arbritrary number of destinations.  For example,
	// northwest would be [-1,1].
	vector<int*> destinations;
	int north[2] = {0, 1};  destinations.push_back( north );
	int east[2]  = {1, 0};  destinations.push_back( east );
	int south[2] = {0, -1}; destinations.push_back( south );
	int west[2]  = {-1, 0}; destinations.push_back( west );
   int northwest[2] = { -1, 1 };  destinations.push_back( northwest );
   int northeast[2] = { 1, 1 };  destinations.push_back( northeast );
   int southwest[2] = { -1, -1 };  destinations.push_back( southwest );
   int southeast[2] = { 1, -1 };  destinations.push_back( southeast );
   
   int totalSize = sizeX;
   streets->callAll(Street::init_);				//initialize cluster
	
   
   Agents *cars = new Agents(2, "Vehicle", msg, 6, streets, 7200);
   cars->callAll(Vehicle::initialize_);

	timer.start();
	

   cars->callAll(Vehicle::updateCarInitialState_);
   cars->manageAll();

   cars->callAll(Vehicle::displayAgentStatus_);
   cars->callAll(Vehicle::updateAgentStatus_);

   for (int turn = 0; turn < numTurns; turn++)
   {
      cars->callAll(Vehicle::moveAgent_);
      cars->manageAll();
   }

	
	std::cerr << "Health Status after " << numTurns << std::endl;
	int* maxTurns = new int;
	*maxTurns = numTurns;
	
	long elaspedTime_END =  timer.lap();
	printf( "\nEnd of simulation. Elasped time using MASS framework with %i processes and %i thread and %i turns :: %ld \n",nProc,nThr,numTurns, elaspedTime_END);
  
	MASS::finish( );
}
