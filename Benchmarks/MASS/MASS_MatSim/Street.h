
#ifndef Street_H
#define Street_H

#include <string.h>
#include "Place.h"
#include "Vehicle.h"

using namespace std;

class Street : public Place {
public:

	static const int NUM_NEIGHBORS = 8;									//A Cluster has 8 neighbors

  // define functionId's that will 'point' to the functions they represent.
  static const int init_ = 0;
  static const int getShortestPath_ = 1;



  
  /**
   * Initialize a Cluster object by allocating memory for it.
   */
  Street( void *argument ) : Place( argument ) {
    bzero( arg, sizeof( arg ) );
    strcpy( arg, (char *)argument );
  };
  
  /**
   * the callMethod uses the function ID to determine which method to execute.
   * It is assumed the arguments passed in contain everything those 
   * methods need to run.
   */
  virtual void *callMethod( int functionId, void *argument ) {
    switch( functionId ) {
      case init_: return init( argument );
      case(getShortestPath_) : return getShortestPath(argument);
    }
    return NULL;
  };

private:
  int status; // 0 = empty, 1 = firms, 2 = with banks
  char arg[100];
  void *init( void *argument );
  void *getShortestPath(void *argument);
  
  vector<int*> cardinals;										//Vector form of cardinals
  static const int neighbor[8][2];								//Array form of cardinals
  int adjGraph[2000][2000];
  int adjGraphDist[2000][2000];

  vector<int> adjGraphVec[2000];
  vector<int> adjGraphDistVec[2000];

  int idNum;
  int size;
  int totalGridWidth;
};

#endif
