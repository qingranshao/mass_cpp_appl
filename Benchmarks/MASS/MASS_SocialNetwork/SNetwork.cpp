
#include "SNetwork.h"
#include "MASS_base.h"
#include <stdlib.h>  //rand
#include <sstream>     // ostringstream
#include <fstream>
#include <stdlib.h>
#include <unordered_set>


extern "C" Place* instantiate( void *argument ) {
  return new SNetwork( argument );
}

extern "C" void destroy( Place *object ) {
  delete object;
}


/**
 * Initializes a Life object.
 */
void *SNetwork::init( void *argument ) {
  
  //Define cardinals
  int north[2]  = {0, 1};  cardinals.push_back( north );
  int east[2] = {1, 0};  cardinals.push_back( east );
  int south[2]  = {0, -1}; cardinals.push_back( south );
  int west[2] = {-1, 0}; cardinals.push_back( west );
  int northwest[2] = { -1, 1 };  cardinals.push_back(northwest);
  int northeast[2] = { 1, 1 };  cardinals.push_back(northeast);
  int southwest[2] = { -1, -1 };  cardinals.push_back(southwest);
  int southeast[2] = { 1, -1 };  cardinals.push_back(southeast);


  for (int i = 0; i < NUM_NEIGHBORS; i++)
     neighborHealthStatus[i] = -1;

  int numNeighbors = static_cast<int>(reinterpret_cast<std::uintptr_t>(argument));
  numNeighbors--;

  for (int i = 0; i < numNeighbors; i++) {
     alive = rand() % 2;
     totalNumNeighbors.push_back(alive);
  }

  msg = index[0] * 10 + index[1];

  inMessage_size = sizeof(int);
  outMessage = NULL;
  outMessage_size = 0;
  
  return NULL;
}

  const int SNetwork::neighbor[8][2] = {{0,1}, {1,0}, {0,-1}, {-1,0}, {-1,1}, {1,1}, {-1,-1}, {1,-1}};



/** (void)
*	Set the outmessage as health of this life
*/
void *SNetwork::exchangeMessage()
{
   vector<int> *totNumNeighbors = &totalNumNeighbors;

   return totNumNeighbors;
}

/** (void)																#Visual logging confirmed 160710
*	Record the outmessage of neighbors as healthstatus
*	pre: neighborHealthStatus is initialized and contains 4 -1s
*/
void *SNetwork::getBoundaryHealthStatus()
{
   //Record neighbor population as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, North);
   neighborHealthStatus[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborHealthStatus[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborHealthStatus[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborHealthStatus[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northwest);
   neighborHealthStatus[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborHealthStatus[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborHealthStatus[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborHealthStatus[7] = (ptr == NULL) ? 0 : *ptr;


   return NULL;
}



/**
* Logs any inMessages associated with the Place.
*/
void *SNetwork::checkInMessage(void *argument) {
   //pull in every places retval (each will be their vector of neighbors. then can just
   //go through the in messages for degrees of freedom.
   ostringstream convert;

   int degreesOfSeparation = static_cast<int>(reinterpret_cast<std::uintptr_t>(argument));


   //go through totalNumNeighbors (list of first degree neighbors. the index is the person. if index is 1 (alive), 
   //go through inMessages. the index of inmessage will correspond to the index of totalNumNeighbors as that neighbors list of neighbors. 
   //will then keep going deeper down until the degree and push onto a vector whenever it reaches the end of that vector at which point will be the list of neighbors at that degree of freedom
   //have one vector, keep a counter, keep adding elements into vector while incrementing count. then once done with the degree, start at the position of count, adding each element again while incrementing count
   //for each element traversed, and continue to do so. once reach degree of freedom, the elements starting current count to the end of the vector will be the neighbors in question.
   vector<int> totalDegreeNeighbors;
   int count = 0;
   int tempCount = 0;
   for (int i = 0; i < totalNumNeighbors.size(); i++) {
      if (totalNumNeighbors.at(i) == 1) {
         totalDegreeNeighbors.push_back(i);
      }
   }

   std::unordered_set<int> setOfFriends;
   for (int i = 0; i < totalDegreeNeighbors.size(); i++) {
      setOfFriends.insert(totalDegreeNeighbors.at(i));
   }

   for (int i = 0; i < degreesOfSeparation; i++) {
      std::unordered_set<int> tempSetOfFriends(setOfFriends);
      setOfFriends.clear();

     for (int k = 0; k < totalNumNeighbors.size(); k++) {
         std::unordered_set<int>::const_iterator isFriend = tempSetOfFriends.find(k);
         if (isFriend != tempSetOfFriends.end()) {
            for (int m = 0; m < inMessages.size(); m++) {
               
            }
            vector<int> friendsTotalFriends = *(vector<int> *)inMessages[k];
            for (int j = 0; j < friendsTotalFriends.size(); j++) {
               if (friendsTotalFriends.at(j) == 1) {
                  setOfFriends.insert(j);
               } 
            }
         }
      }
   }
   convert << "SET OF FIRENDS SIZE " << "  " << setOfFriends.size() << endl;
   convert << "person[" << index[0] << "][" << index[1] << "] Friends " << degreesOfSeparation << " degrees of freedom away: ";
   for (auto it = setOfFriends.begin(); it != setOfFriends.end(); ++it)
      convert << " " << *it;
   convert << std::endl;

   MASS_base::log(convert.str());

   return NULL;
}

/** (void)
*	Set the outmessage as health of this life
*/
void *SNetwork::resetInMessages()
{
   inMessages.clear();

   return NULL;
}