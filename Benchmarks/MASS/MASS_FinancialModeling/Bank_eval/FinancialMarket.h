#ifndef FINANCIALMARKET_H
#define FINANCIALMARKET_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <set>

struct WorkerCost {
	double wages;
	int workerId;
};

struct BankInfo {
	double interestRate;
	double liquidity;
	int bankId;
	int xLoc;
	int yLoc;
};

struct BankSelectionArgs {
	double minAcceptableInterest;
	double amountNeeded;
	int k;
};

struct Loan {
	double amount;  
	double interestRate;
	int loaningBankId;
};

class FinancialMarket : public Place {
public:
	/*========================== Function IDs ===========================*/
	static const int init_ = 0;
	static const int prepBankInfoExchange_ = 1;     
	static const int exchangeBankInfo_ = 2;         
	static const int updateBankRegistry_ = 3;      
	static const int prepForIncomingTransactions_ = 4;
	static const int exchangeOutgoingTransactions_ = 5;
	static const int manageIncomingTransactions_ = 6;
  static const int resetRoundValues_ = 7;
  static const int seedRound_ = 8;
	static const int addWorker_ = 9;                // Add Workers wages
	static const int addFirm_ = 10;                 // sets residingFirm 
	static const int addBank_ = 11;                 // sets residingBank 
	static const int calculateWorkforceCost_ = 12;  // Help firms
	static const int updateBankInfo_ = 13;          // Updates bankRegistry 
	static const int selectFromKBanks_ = 14;        
	static const int addBankTransaction_ = 15;    
	static const int getIncomingTransactionAmt_ = 16;  
	static const int getNumFirms_ = 17;

	/*========================== Public Methods =========================*/
	FinancialMarket(void* arg) : Place(arg) { /**/}
  
	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case addWorker_ :
				return addWorker(argument);

			case calculateWorkforceCost_ :
				return calculateWorkforceCost();

			case addFirm_ :
				return addFirm(argument);
			
			case addBank_ :
				return addBank(argument);

			case updateBankInfo_ :
				return updateBankInfo(argument);

			case prepBankInfoExchange_ :
				return prepBankInfoExchange();

			case exchangeBankInfo_ :
				return exchangeBankInfo();

			case updateBankRegistry_ :
				return updateBankRegistry();

			case selectFromKBanks_ :
				return selectFromKBanks(argument);

			case addBankTransaction_ :
				return addBankTransaction(argument);

			case prepForIncomingTransactions_ :
				return prepForIncomingTransactions();

			case exchangeOutgoingTransactions_ :
				return exchangeOutgoingTransactions(argument);

			case manageIncomingTransactions_ :
				return manageIncomingTransactions();

			case getIncomingTransactionAmt_ :
				return getIncomingTransactionAmt();

			case getNumFirms_ :
				return getNumFirms();

      case resetRoundValues_ :
        return resetRoundValues();

      case seedRound_ :
        return seedRound(argument);
		}
		return nullptr;
	}

private:
	/*======================= Call Method Helpers =======================*/
	void* init(void* nAgents);
	void* addWorker(void* workerInfo);
	void* calculateWorkforceCost();
	void* addFirm(void* firmArg);
	void* addBank(void* bankInfo); 
	void* updateBankInfo(void* updatedInfo);
	void* prepBankInfoExchange();
	void* exchangeBankInfo();
	void* updateBankRegistry();
	void* selectFromKBanks(void* arg);
	void* addBankTransaction(void* transactionInfo);
	void* prepForIncomingTransactions();
	void* exchangeOutgoingTransactions(void* givenBankId);
	void* manageIncomingTransactions();
	void* getIncomingTransactionAmt();
	void* getNumFirms();
  void* resetRoundValues();
  void* seedRound(void* seed);

  /*====================== Private Helper Methods =====================*/
  bool uniqueAddNeighbor(const int x, const int y);

	/*=========================== Member Data ===========================*/
	double incomingTransactions; /* Sum of payments/loans for local bank */
	int numBanks;                /* Number of banks in the simulation */
  int numFirms;                /* Number of firms in the simulation */
	int residingBank;            /* ID of Bank Agent that resides here */
	int residingFirm;            /* ID of Firm Agent that resides here */
	unordered_map<int /*bankId*/, double /*Amount*/> outgoingTransactions;
	unordered_map<int, double> localWorkerWages;
	unordered_map<int, BankInfo> bankRegistry;  /* Stores info on Banks */
	ostringstream logText;       /* Used to send messages to MASS log */
};

#endif //FINANCIALMARKET_H