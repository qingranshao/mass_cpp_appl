#include "FinancialMarket.h"
#include <mutex>

using namespace std;

mutex mtx;

extern "C" Place* instantiate(void* argument) {
	return new FinancialMarket(argument);
}

extern "C" void destroy(Place* obj) {
	delete obj;
}

/*------------------------------ init -------------------------------*/
void* FinancialMarket::init(void* nAgents) {
	residingBank = residingFirm = -1;
	pair<int,int> simArgs = *(pair<int,int>*)nAgents;
  numBanks = simArgs.first;
  numFirms = simArgs.second;
  srand(0);
	return nullptr;
}

/*----------------------------- addWorker ---------------------------*/
void* FinancialMarket::addWorker(void* workerInfo) {
	WorkerCost wc = *(WorkerCost*)workerInfo;
	localWorkerWages[wc.workerId] = wc.wages;
	return nullptr;
}

/*------------------------ calculateWorkforceCost -------------------*/
void* FinancialMarket::calculateWorkforceCost() {
	double totalWages;
	for (auto worker : localWorkerWages) {
    totalWages += worker.second;
  }
	localWorkerWages.clear(); // Clear the container to free storage.
	return (void*)&totalWages;
}

/*------------------------------ addFirm ----------------------------*/
void* FinancialMarket::addFirm(void* firmArg) {
	residingFirm = *(int*)firmArg;
	return nullptr;
}

/*------------------------------ addBank ----------------------------*/
void* FinancialMarket::addBank(void* bankInfo) {
	BankInfo bInfo = *(BankInfo*)bankInfo; // Receive BankInfo argument
	residingBank = bInfo.bankId;           // Update member data
	bankRegistry[residingBank] = bInfo;    // Add info to hash map
	return nullptr;
}

/*--------------------------- updateBankInfo ------------------------*/
void* FinancialMarket::updateBankInfo(void* updatedInfo) {
	BankInfo bInfo = *(BankInfo*)updatedInfo;
	bankRegistry[bInfo.bankId] = bInfo;
	return nullptr;
}

/*------------------------- prepBankInfoExchange --------------------*/
void* FinancialMarket::prepBankInfoExchange() {
	inMessage_size = sizeof(BankInfo); // Prepare to receive a message
	outMessage_size = sizeof(char);
  char* dummyArg = new char('D');
  outMessage = &dummyArg;
	// Add places with Banks as neighbors
	cleanNeighbors();
	for (int count = 0, x = 0, y = 0; count < numBanks; count++) {
		if (y >= size[1]) {
			x++;
			y = 0;
		}
    if (x != index[0] || y != index[1]) {
      uniqueAddNeighbor(x - index[0], y - index[1]);
    }
		y++;
	}
	inMessages.clear(); // Clean inMessages
	// If a bank resides on this place element, add a message
	if (residingBank != -1) {
		BankInfo msg = bankRegistry[residingBank];
	}
	return nullptr;
}

/*--------------------------- exchangeBankInfo ----------------------*/
void* FinancialMarket::exchangeBankInfo() {
	BankInfo* msg = new BankInfo;
	if (residingBank != -1) 
		*msg = bankRegistry[residingBank];
	else 
		msg->bankId == -1;
	return msg;
}

/*--------------------------- updateBankRegistry --------------------*/
void* FinancialMarket::updateBankRegistry() {
	for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
		void* updateArg = (BankInfo*)inMessages[msgNum];
		updateBankInfo(updateArg);
	}
	return nullptr;
}

/*---------------------------- selectFromKBanks ---------------------*/
void* FinancialMarket::selectFromKBanks(void* arg) {
	BankSelectionArgs args = *(BankSelectionArgs*)arg;
	if (args.k > numBanks)
		args.k = numBanks;
	BankInfo selectedBank;
	selectedBank.bankId = -1;

	// Select k random, unique banks
	set<int> kBanks;
  //srand(residingFirm);
	for (int numSelected = 0; numSelected < args.k; numSelected++) {
		bool added = false;
		do {
			int randomBank = (int)(rand()) % numBanks;
			added = (kBanks.insert(randomBank)).second;
		} while (!added);
	}
	// Cycle through the randomly selected banks
	for (set<int>::iterator randomBank = kBanks.begin(); 
      randomBank != kBanks.end(); randomBank++) {

		unordered_map<int, BankInfo>::iterator it = bankRegistry.find(*randomBank);
		if (it != bankRegistry.end()) {
			// Find bank with lowest interest rate that can afford to offer the loan
			if (it->second.interestRate < args.minAcceptableInterest 
          && it->second.liquidity > args.amountNeeded) {
				args.minAcceptableInterest = it->second.interestRate;
				selectedBank = it->second;
			}  
		}
	}
	void* ret = &selectedBank;
	return ret;
}

/*------------------------- addBankTransaction ----------------------*/
void* FinancialMarket::addBankTransaction(void* transactionInfo) {
	pair<int,double> transaction = *(pair<int,double>*)transactionInfo;
  mtx.lock();   // Begin Critical Section
	outgoingTransactions[transaction.first] += transaction.second;
  mtx.unlock(); // End Critical Szection
	return nullptr;
}

/*--------------------- prepForIncomingTransaction ------------------*/
void* FinancialMarket::prepForIncomingTransactions() {
  cleanNeighbors();
  if (residingBank != -1) /* Have Banks add all other places */ {
    for (int count = 0, x = 0, y = 0; count < numFirms; count++) {
      if (y >= size[1]){
        x++;
        y = 0;
      }
      if (x != index[0] || y != index[1]) {
        uniqueAddNeighbor(x - index[0], y - index[1]);
      }
      y++;
    }
    // Prep to send bankID to get transactions
    outMessage_size = sizeof(int); 
    outMessage = (void*)&residingBank;  // Prepare to send bankId
    inMessage_size = sizeof(double);    // Prepare to receive amount
    inMessages.clear(); // Clear in messages
  }
  return nullptr;
}

/*--------------------- exchangeOutgoingTransactions ----------------*/
void* FinancialMarket::exchangeOutgoingTransactions(void* givenBankId) {
  int bankLookup = *(int*)givenBankId;
  unordered_map<int,double>::iterator 
      transaction = outgoingTransactions.find(bankLookup);
  double* amount = new double;
  if (transaction == outgoingTransactions.end()) {
    *amount = 0.0;     // Return 0 if no transaction was found
  }
  else {
    *amount = transaction->second;
  }
  return (void*) amount;
}

/*--------------------- manageIncomingTransactions ------------------*/
void* FinancialMarket::manageIncomingTransactions() {
  if (residingBank != -1) /* Have places w/ banks read transactions*/{
    for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
      incomingTransactions += *((double*)inMessages[msgNum]);
    }
    // Add transactions from self
    unordered_map<int,double>::iterator transactionLookup = 
      outgoingTransactions.find(residingBank);
    if (transactionLookup != outgoingTransactions.end()) {
      incomingTransactions += transactionLookup->second;
    }
  }
  return nullptr;
}

/*--------------------- getIncomingTransactionAmt ------------------*/
void* FinancialMarket::getIncomingTransactionAmt() {
  return (void*)&incomingTransactions;
}

/*---------------------------- getNumFirms --------------------------*/
void* FinancialMarket::getNumFirms() {
  return (void*)&numFirms;
}

/*------------------------- resetRoundValues ------------------------*/
void* FinancialMarket::resetRoundValues() {
  incomingTransactions = 0.0;
  outgoingTransactions.clear();
  cleanNeighbors();
  inMessages.clear();
  return nullptr;
}

/*---------------------------- seedRound ----------------------------*/
void* FinancialMarket::seedRound(void* seed) {
  srand(*(int*)seed);
  return nullptr;
}

/*------------------------- uniqueAddNeighbor -----------------------*/
bool FinancialMarket::uniqueAddNeighbor(const int x, const int y) {
  // Get neighbors and check to see if the new neighbor already exists
  bool alreadyAdded = false;
  vector<int*> myNeighbors = getNeighbors();
  for (int i = 0; i < myNeighbors.size(); i++) {
    if (myNeighbors[i][0] == x && myNeighbors[i][1] == y) {
      alreadyAdded = true;
      break;
    }
  }
  if (!alreadyAdded) {
    int newNeighbor[2] = {x, y};
    addNeighbor(newNeighbor,2);
  }
  return !alreadyAdded;
}