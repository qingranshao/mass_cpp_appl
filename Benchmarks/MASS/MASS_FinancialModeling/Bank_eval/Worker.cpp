#include "Worker.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Worker(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}

/*-------------------------------- Map ------------------------------*/
int Worker::map(int initPopulation, vector<int> size, vector<int> index,
              Place* curPlace) {
  
  void* dummyArg;
  int numFirms = *(int*)curPlace->callMethod(FinancialMarket::getNumFirms_, dummyArg);

  // compute the global linear index
  int linearIndex = 0;
  for (int i = 0; i < int(index.size()); i++) {
    if (index[i] >= 0 && size[i] > 0 && index[i] < size[i]) {
      linearIndex = linearIndex * size[i];
      linearIndex += index[i];
    }
  }
  if (linearIndex >= numFirms) {
    return 0; // No Firm resides here. Abandon mapping.
  }

  // compute the total # places
  int placeTotal = 1;
  for (int x = 0; x < int(size.size()); x++) placeTotal *= size[x];

  // compute #agents per place a.k.a. colonists
  int colonists = initPopulation / numFirms;
  int remainders = initPopulation % numFirms;
  if (linearIndex < remainders) colonists++;  // add a remainder
  return colonists;
}

/*------------------------------ init -------------------------------*/
void* Worker::init(void* initVals) {
  WorkerInitValues initArgs = *(WorkerInitValues*)initVals;
  // Set up worker's member data
  wages = initArgs.defaultWage / (initArgs.wageDivisor * ((float)rand()/RAND_MAX + 0.5));
  capital = 0;
  consumptionBudget = 0.3;
  workerId = agentId;

  // Write wages to Financial Market Place
  WorkerCost wc;
  wc.workerId = workerId;
  wc.wages = wages;
  void* workerInfo = &wc;
  place->callMethod(FinancialMarket::addWorker_, workerInfo);

  // Select a random bank
  BankSelectionArgs selArgs;
  selArgs.minAcceptableInterest = INT_MAX;
  selArgs.amountNeeded = -1;
  selArgs.k = 1;
  BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);
  myBank = selectedBank.bankId;
  return nullptr;
}

/*----------------------- timeStepInteraction -----------------------*/
bool* Worker::timeStepInteraction() {
  receiveWages();
  consume();
  depositFunds();
  return nullptr;
}

/*-------------------------- receiveWages ---------------------------*/
void Worker::receiveWages() {
  capital += wages;
}

/*----------------------------- consume -----------------------------*/
void Worker::consume() {
  double percentOfWage = (float) ( (float)rand() / RAND_MAX);
  if (percentOfWage < 0.4) {
    percentOfWage = 0.4;
  }
  consumptionBudget = wages * percentOfWage;
}

/*--------------------------- depositFunds --------------------------*/
bool Worker::depositFunds() {
  pair<int,double> deposit;
  deposit.first = myBank;
  deposit.second = wages - consumptionBudget; // Positive value
  place->callMethod(FinancialMarket::addBankTransaction_, (void*)&deposit);
  return false;
}

// End Worker.cpp