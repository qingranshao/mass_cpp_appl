#ifndef WORKER_H
#define WORKER_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

struct WorkerInitValues {
  double defaultWage; // Income
  int wageDivisor;    // Used to calculate individual wages
  bool makeLog;
};

class Worker : public Agent {
public:
  /*========================== Function IDs ===========================*/
  static const int init_ = 0;
  static const int timeStepInteraction_ = 1;

  /*========================== Public Methods =========================*/
  Worker(void* argument) : Agent(argument) {
    workerId = myBank = myFirm = 0;
    wages = capital = consumptionBudget = 0.0;
  }

  virtual int map(int initPopulation, vector<int> size, 
      vector<int> index, Place *curPlace);

  /*---------------------------- Call Method --------------------------*/
  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case timeStepInteraction_ :
        return (void*)timeStepInteraction();

    }
    return nullptr;
  }

private:
  /*======================= Call Method Helpers =======================*/
  void* init(void* initVals);
  bool* timeStepInteraction();
  void receiveWages();
  void consume();
  bool depositFunds();

  /*=========================== Member Data ===========================*/
  double wages;               // Income
  double capital;             // Saved money
  double consumptionBudget;   // Amount spend on products each step
  int workerId;               // Unique identifier for worker
  int myBank;                 // Identifies bank holding personal account
  int myFirm;                 // Identifies firm this household works for
  ostringstream logText;      // Used to make MASS logs
};

#endif // WORKER_H