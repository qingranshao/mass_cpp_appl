#ifndef FIRM_H
#define FIRM_H

#include <iostream>
#include "Agent.h"
#include "Owner.h"
#include "MASS_base.h"
#include <map>
#include "FinancialMarket.h"

struct FirmInitValues {
  double productionCost;  // Initial production costs
  double liquidity;       // Initial liquidity
  bool makeLog;
};

class Firm : public Agent {
public:
  /*========================== Function IDs ===========================*/
  static const int init_ = 0;                  
  static const int calculateWorkforceCost_ = 1;
  static const int timeStepInteraction_ = 2;   

  /*========================== Public Methods =========================*/
  Firm(void* argument) : Agent(argument) {}

  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case calculateWorkforceCost_ :
        return calculateWorkforceCost(argument);

      case timeStepInteraction_ :
        return timeStepInteraction();            

      default :
        return badFunctionId(functionId);
    }
    return nullptr;
  }

private:
  /*======================= Call Method Helpers =======================*/
  void* init(void* initVals);
  void* timeStepInteraction();
  void* calculateWorkforceCost(void* makeLog);
  void* badFunctionId(int functionId);
  void accumulateInterest();
  double calculateProducitonCosts();
  double calculateProfits();
  bool payWorkforce();
  double calculateLiquidity();
  bool getOwnerLoan();
  bool requestBankLoan(int k);
  bool payBank();
  void resetFirm();

  /*=========================== Member Data ===========================*/
  FirmInitValues resetVals;   // Stores sim args in case of firm reset
  double productionCost;      // Current round's production costs
  double liquidity;           // Ongoing liquidity
  double workforceCost;       // Cost to pay workers
  double profit;              // Current round's profits
  int firmId;                 // Unique identified for firm
  Owner* owner;               // Pointer to owner object 
  vector<Loan> debt;          // Outstanding Loans
  ostringstream logText;      // Used to write MASS logs
};

#endif // FIRM_H