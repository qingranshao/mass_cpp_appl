#include "Owner.h"

using namespace std;

Owner::Owner() : firmId{-1}, capital{0.0} {}

Owner::Owner(int firmId_, double capital_) {
    firmId = firmId_;
    capital = capital_;
}

Owner::~Owner() { /*No dynamic memory to delete.*/ }

double Owner::getCapital() const {
    return capital;
}

void Owner::setCapital(double newCapital) {
    capital = newCapital;
}

// End Owner.cpp