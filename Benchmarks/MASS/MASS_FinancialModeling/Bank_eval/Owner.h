#ifndef OWNER_H
#define OWNER_H

#include <iostream>

class Owner {
public:
  Owner();
  Owner(int firmId_, double capital_);
  virtual ~Owner();
  double getCapital() const;
  void setCapital(double newCapital);

private:
  double capital;         // Amount of savings
  int firmId;             // ID of the Firm this Owner runs
};

#endif // OWNER_H