#ifndef BANK_H
#define BANK_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

struct BankInitValues {
	double interestRate;
	double liquidity;
	bool makeLog;
};

class Bank : public Agent {
public:
	/*========================== Function IDs ===========================*/
	static const int init_ = 0;
	static const int timeStepInteraction_ = 1;
	static const int readRoundTransactions_ = 2;
	static const int checkBankruptcy_ = 3;
	static const int randomizeRate_ = 4;
	static const int reportRoundValues_ = 5;

	/*========================== Public Methods =========================*/
	Bank(void* argument) : Agent(argument) {}

	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case timeStepInteraction_ :
				return (void*)timeStepInteraction();

			case readRoundTransactions_ :
				return readRoundTransactions();

			case checkBankruptcy_ :
				return checkBankruptcy(argument);

			case randomizeRate_ :
				return randomizeRate(argument);

			case reportRoundValues_ :
				return reportRoundValues();

			default :
				return badFunctionId(functionId);
		}
		return NULL;
	}

private:
	/*======================= Call Method Helpers =======================*/
	void* init(void* initVals);
	void* timeStepInteraction();
	void* readRoundTransactions(); 
	void* checkBankruptcy(void* isBankrupt); 
	void* randomizeRate(void* round);
	void* reportRoundValues();
	void* badFunctionId(int functionId);
	bool payLoans();
	void accumulateInterest();
	bool getLoan(int k);

	/*=========================== Member Data ===========================*/
	double interestRate;            // Interest rate offered for loans.
	double liquidity;               // The Bank's assets/cash
	double totalDebt;               // Total debt from outstanding loans
	int bankId;                     // Unique bank identifier
	vector<Loan> debt;              // Outstanding loans
	ostringstream logText;          // Used to make MASS logs
};

#endif // BANK_H