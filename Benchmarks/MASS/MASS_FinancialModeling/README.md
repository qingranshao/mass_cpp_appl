# Bail-In/Bail-Out Description

The Bail-In/Bail-Out Benchmark program models a financial market and simulates interactions between three types of financial entities: banks, firms, and households (owners and workers). The purpose of this program is to monitor how these entities interact with one another leading up to a bankruptcy of one of the banks.

# Bank_eval Version

This version of Bail-In/Bail-Out is optimized to deliver the best performance results. To help the program run faster, this version does not print any log messages during execution. To ensure a more fair and accurate lines of code count, all of the unneccessary whitespace and inline comments have been removed.

# Mass_bank_debug Version

This version of Bail-In/Bail-Out is written to help with testing and debugging. This program outputs log statements during execution to provide insight on what the program is doing. Additionally, this version has block comments at the top of each class file as well as inline comments to help a viewer of the source code understand it.

# How to Run

Bail-In/Bail-Out is designed to run on UW Bothell’s eight hermes machines, using one machine as the master node and communicating with the others through sockets. To run the program in the same environment it was tested in, log in to hermes01.uwb.edu using your UW username and password. Once you are logged in to the hermes machine, use the following steps to run the program. 

1) Clone the MASS C++ library files from the Bitbucket repository at https://bitbucket.org/mass_library_developers/mass_cpp_core/src/master/.

2) In the Bail-In/Bail-Out directory, create symbolic links to the following three files in the MASS library:
	- /mass_cpp_core/ubuntu/libmass.so
	- /mass_cpp_core/ubuntu/mprocess 
	- /mass_cpp_core/ubuntu/killMProcess.sh

3) Edit line 2 of compile.sh to include the path to mass_cpp_core. By default, the mass_cpp_core will be assumed to be one directory back from the Bail-In/Bail-Out program files. The default line reads "export MASS_DIR=../mass_cpp_core".

4) Modify machinefile.txt as needed to configure it with the names of the hermes machines to be used as remote computing nodes during execution. List one machine name per line and exclude the master node from the file. 

5) Modify simArgx.txt to reflect the scale oft he financial model to simulate.

6) Execute run.sh and provide the arguments it prompts you to enter. For the most secure results, run 'setup_mpi_cluster.sh' to avoid passing your password across the computing nodes.

7) In the case that the program crashes before MASS:finish() has been called, execute killMProcess.sh to clean up the processes on remote computing nodes.

	