LD_LIBRARY_PATH=../mass_cpp_core/ubuntu
export LD_LIBRARY_PATH

echo "Number of nodes? {1, 2, 4, 8, 16}: "
read NUMNODES 

echo "Number of threads? {1, 2, 4}: "
read NUMTHREADS

echo "Number of turns? "
read NUMTURNS

echo "Port number? "
read PORT

echo "Password? "
read -s PASSWORD

rm output.txt

./main $USER $PASSWORD machinefile.txt $PORT $NUMNODES $NUMTHREADS $NUMTURNS simArgs.txt show &> output.txt

