#!/bin/sh
export MASS_DIR=../mass_cpp_core

rm Bank
rm FinancialMarket
rm Firm
rm Worker
rm main


g++ -Wall Bank.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Bank
g++ -Wall FinancialMarket.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o FinancialMarket
g++ -Wall Firm.cpp Owner.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Firm
g++ -Wall Worker.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Worker
g++ -std=c++11 -Wall main.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o main
