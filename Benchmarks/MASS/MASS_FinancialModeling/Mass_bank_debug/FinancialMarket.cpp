#include "FinancialMarket.h"
#include <mutex>

using namespace std;

mutex mtx;

extern "C" Place* instantiate(void* argument) {
	return new FinancialMarket(argument);
}

extern "C" void destroy(Place* obj) {
	delete obj;
}


/*------------------------------ init -------------------------------*/
/** Initializes member data for the Financial Market object.
  *  @param nAgents - a pair<int,int> where the first element is the 
  *         number of banks, and the second is the number of firms.  */
void* FinancialMarket::init(void* nAgents) {
	residingBank = residingFirm = -1;
	pair<int,int> simArgs = *(pair<int,int>*)nAgents;
  numBanks = simArgs.first;
  numFirms = simArgs.second;
  srand(0);
	return nullptr;
}


/*----------------------------- addWorker ---------------------------*/
/** Allows Worker agents to report their wage for the Firm to read.
*   @param workerInfo - a WorkerCost object                          */
void* FinancialMarket::addWorker(void* workerInfo) {
	WorkerCost wc = *(WorkerCost*)workerInfo;
	localWorkerWages[wc.workerId] = wc.wages;
	return nullptr;
}


/*------------------------ calculateWorkforceCost -------------------*/
/** Sums the wages of all residing workers for the Firm to read.     */
void* FinancialMarket::calculateWorkforceCost() {
  logText.str("");
  logText << "Place " << index[0] << ", " << index[1] << " calculating workforce cost" << endl;
	double totalWages;
	for (auto worker : localWorkerWages) {
		totalWages += worker.second;
    logText << "     Worker " << worker.first << " has wage " << worker.second << endl;
	}
  logText << "     Total Cost: $" << totalWages << endl;
  MASS_base::log(logText.str());
	localWorkerWages.clear(); // Clear the container to free storage.
	return (void*)&totalWages;
}


/*------------------------------ addFirm ----------------------------*/
/** Sets the residingFirm data member.															 */
void* FinancialMarket::addFirm(void* firmArg) {
	residingFirm = *(int*)firmArg;
	return nullptr;
}


/*------------------------------ addBank ----------------------------*/
/** Sets the residingBank data member and updates the bankregistry.  
 *  @param bankInfo - a BankInfo struct to store info on Banks.      */
void* FinancialMarket::addBank(void* bankInfo) {
	BankInfo bInfo = *(BankInfo*)bankInfo; // Receive BankInfo argument
	residingBank = bInfo.bankId;           // Update member data
	bankRegistry[residingBank] = bInfo;    // Add info to hash map
	return nullptr;
}


/*--------------------------- updateBankInfo ------------------------*/
/** Updates the bankregistry with the given argument.								 
 *  @param updatedInfo - a BankInfo struct reflecting updated changes
*         to the bank's information.                                 */
void* FinancialMarket::updateBankInfo(void* updatedInfo) {
	BankInfo bInfo = *(BankInfo*)updatedInfo;
	bankRegistry[bInfo.bankId] = bInfo;
	return nullptr;
}


/*------------------------- prepBankInfoExchange --------------------*/
/** Prepares Place elements to pass their BankInfo to update each 
*   element's bankRegistry.                                          */
void* FinancialMarket::prepBankInfoExchange() {
	inMessage_size = sizeof(BankInfo); // Prepare to receive a message
	outMessage_size = sizeof(char);
  char* dummyArg = new char('D');
  outMessage = &dummyArg;

	// Add places with Banks as neighbors
	cleanNeighbors();
	for (int count = 0, x = 0, y = 0; count < numBanks; count++) {
		if (y >= size[1]) {
			x++;
			y = 0;
		}
    if (x != index[0] || y != index[1]) {
      uniqueAddNeighbor(x - index[0], y - index[1]);
    }
		y++;
	}
	
	inMessages.clear(); // Clean inMessages

	// If a bank resides on this place element, add a message
	if (residingBank != -1) {
		BankInfo msg = bankRegistry[residingBank];
		
		logText.str("");
		logText << "Place " << index[0] << ", " << index[1] 
						<< " passing message: " << endl;
		logText << "     Bank ID: " << msg.bankId 
            << ", Rate: " << msg.interestRate 
            << ", Liquidity: " << msg.liquidity << endl;
		MASS_base::log(logText.str());
	}
	return nullptr;
}


/*--------------------------- exchangeBankInfo ----------------------*/
/** Passes the BankInfo object set up by each Bank in the simulation.*/
void* FinancialMarket::exchangeBankInfo() {
	BankInfo* msg = new BankInfo;
	if (residingBank != -1) 
		*msg = bankRegistry[residingBank];
	else 
		msg->bankId == -1;
	return msg;
}


/*--------------------------- updateBankRegistry --------------------*/
/** Updates the bankRegistry based on values received from other Place
 *  elements.                                             					 */
void* FinancialMarket::updateBankRegistry() {
	for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
		void* updateArg = (BankInfo*)inMessages[msgNum];
		updateBankInfo(updateArg);
	}
	return nullptr;
}


/*---------------------------- selectFromKBanks ---------------------*/
/** Helps Agents find Banks to loan money to them. Randomly approaches
 *  k banks and returns the bankId of the bank with the lowest interest
 *  rate that can affort to give the loan.                           
 *  @param arg - a BankSelectionArgs instance                        */
void* FinancialMarket::selectFromKBanks(void* arg) {
	BankSelectionArgs args = *(BankSelectionArgs*)arg;
	if (args.k > numBanks)
		args.k = numBanks;
	BankInfo selectedBank;
	selectedBank.bankId = -1;

	// Select k random, unique banks
	set<int> kBanks;
  //srand(residingFirm);
	for (int numSelected = 0; numSelected < args.k; numSelected++) {
		bool added = false;
		do {
			int randomBank = (int)(rand()) % numBanks;
			added = (kBanks.insert(randomBank)).second;
		} while (!added);
	}
	// Cycle through the randomly selected banks
	for (set<int>::iterator randomBank = kBanks.begin(); randomBank != kBanks.end(); randomBank++) {
		// Find bank in bank registry
		unordered_map<int, BankInfo>::iterator it = bankRegistry.find(*randomBank);

		if (it != bankRegistry.end()) {
			// Find bank with lowest interest rate that can afford to offer the loan
			if (it->second.interestRate < args.minAcceptableInterest && it->second.liquidity > args.amountNeeded) {
				args.minAcceptableInterest = it->second.interestRate;
				selectedBank = it->second;
			}  
		}
	}
	void* ret = &selectedBank;
	return ret;
}


/*------------------------- addBankTransaction ----------------------*/
/** Adds a new payment or loan to the outgoingTransactions data member
 *  to send to a Bank on another place.															 
 *  @param transactionInfo - a pair<int,double>. The int is the bankId,
 *         the double is the amount of the transaction.              */
void* FinancialMarket::addBankTransaction(void* transactionInfo) {
	// Convert input to a pair. first is bankId, second is amount
	pair<int,double> transaction = *(pair<int,double>*)transactionInfo;

	logText.str("");
	logText << "Place " << index[0] << ", " << index[1] << " adding transaction" << endl;
	logText << "     Bank: " << transaction.first << endl;
	logText << "     Amount: " << transaction.second << endl;

	// Add transaction amount to the outgoing transaction amount
	// Loans will have a negative amount, reducing this value
	// Payments/deposits will be positive, increasing this value

  mtx.lock();
	outgoingTransactions[transaction.first] += transaction.second;
  mtx.unlock();

	logText << "     outgoingTransactions now " << outgoingTransactions[transaction.first];
	MASS_base::log(logText.str());
	return nullptr;
}


/*--------------------- prepForIncomingTransaction ------------------*/
/** Prepares places to receive transactions sent by other places.    */
void* FinancialMarket::prepForIncomingTransactions() {
  cleanNeighbors();
  if (residingBank != -1) {
    // Have banks add all other places (except themselves)
    logText.str("");
    logText << "Bank " << residingBank << " adding all places" << endl;
    for (int count = 0, x = 0, y = 0; count < numFirms; count++) {
      if (y >= size[1]){
        x++;
        y = 0;
      }
      if (x != index[0] || y != index[1]) {
        bool added = uniqueAddNeighbor(x - index[0], y - index[1]);
        if (added) {
          logText << "  - Added firm " << count << " at " << x << ", " << y << endl;
        }
      }
      y++;
    }
    MASS_base::log(logText.str());
    // Prep to send bankID as outMessage argument
    outMessage_size = sizeof(int); // Prepare to send bankId
    outMessage = (void*)&residingBank;
    inMessage_size = sizeof(double); // Prepare to receive amount
    inMessages.clear(); // Clear in messages
  }
  return nullptr;
}


/*--------------------- exchangeOutgoingTransactions ----------------*/
/** Receives a bankId from a remote place and returns the sum of all
 *  transactions made on the place for that bank.                   
 *  @param givenBankId - the ID of the bank associated with the 
 *         transaction 																							 */
void* FinancialMarket::exchangeOutgoingTransactions(void* givenBankId) {
  int bankLookup = *(int*)givenBankId;
  unordered_map<int,double>::iterator transaction = outgoingTransactions.find(bankLookup);
  double* amount = new double;
  if (transaction == outgoingTransactions.end()) {
    *amount = 0.0;     // Return 0 if no transaction was found for that bank
  }
  else {
    *amount = transaction->second;
  }
  return (void*)amount;
}


/*--------------------- manageIncomingTransactions ------------------*/
/** Totals up all transactions received from other places and stores it
 *  for the residing bank to read in.																 */
void* FinancialMarket::manageIncomingTransactions() {
  if (residingBank != -1) {
    logText.str("");
    logText << "Place (" << index[0] << ", " << index[1] << ") received " << inMessages.size() << " messages" << endl;
    // Add Transactions from other places
    for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
      incomingTransactions += *((double*)inMessages[msgNum]);
      logText << "     " << *((double*)inMessages[msgNum]) << endl;
    }
    // Add transactions from self
    unordered_map<int,double>::iterator transactionLookup = 
      outgoingTransactions.find(residingBank);
    if (transactionLookup != outgoingTransactions.end()) {
      incomingTransactions += transactionLookup->second;
      logText << "     Self-transaction for " << transactionLookup->second << endl;
    }
    
    logText << "     Incoming Transaction Total $" << incomingTransactions << endl; 
    MASS_base::log(logText.str());
  }
  return nullptr;
}


/*--------------------- getIncomingTransactionAmt ------------------*/
/** Returns the sum of payments/loans received.                     */
void* FinancialMarket::getIncomingTransactionAmt() {
  return (void*)&incomingTransactions;
}

/*---------------------------- getNumFirms --------------------------*/
/** Returns the number of firms in the simulation.                   */
void* FinancialMarket::getNumFirms() {
  return (void*)&numFirms;
}


/*------------------------- resetRoundValues ------------------------*/
/** Resets round-specific values to prepare for another round.       */
void* FinancialMarket::resetRoundValues() {
  incomingTransactions = 0.0;
  outgoingTransactions.clear();
  cleanNeighbors();
  inMessages.clear();
  return nullptr;
}

/*---------------------------- seedRound ----------------------------*/
/** Seeds the round for testable output.                             */
void* FinancialMarket::seedRound(void* seed) {
  srand(*(int*)seed);
  return nullptr;
}


/*------------------------- uniqueAddNeighbor -----------------------*/
/** Checks if a neighbor has already been added and adds it if it was 
*   not. Returns true if the neighbor is added during this call and 
*   returns false if the neighbor was already added previously.      
*   @param x - the neighbor's x-location (column)
*   @param y - the neighbor's y-location (row)                       */
bool FinancialMarket::uniqueAddNeighbor(const int x, const int y) {
  // Get neighbors and check to see if the new neighbor already exists
  bool alreadyAdded = false;
  vector<int*> myNeighbors = getNeighbors();
  for (int i = 0; i < myNeighbors.size(); i++) {
    if (myNeighbors[i][0] == x && myNeighbors[i][1] == y) {
      alreadyAdded = true;
      break;
    }
  }
  if (!alreadyAdded) {
    int newNeighbor[2] = {x, y};
    addNeighbor(newNeighbor,2);
  }
  return !alreadyAdded;
}