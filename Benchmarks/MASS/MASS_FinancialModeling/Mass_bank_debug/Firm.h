/*=======================================================================
	Firm Class
	Author: Ian Dudder
=========================================================================
	The Firm Class is an Agent in the Bail-in/Bail-out simulation. Each
  round, it calculates its costs and profits, pays its workers and owner, 
  pays off loans, and determines if it needs a loan if it goes into debt. 
  The loan may come from its owner, or a bank. When the Firm takes out a
  loan, it sends a negative value to the bank. If it makes a payment, it
  sends a positive value to the bank.
=======================================================================*/

#ifndef FIRM_H
#define FIRM_H

#include <iostream>
#include "Agent.h"
#include "Owner.h"
#include "MASS_base.h"
#include <map>
#include "FinancialMarket.h"

/* Received by init() to set member data */
struct FirmInitValues {
  double productionCost;  // Initial production costs
  double liquidity;       // Initial liquidity
  bool makeLog;
};


class Firm : public Agent {
public:
  /*========================== Function IDs ===========================*/
  static const int init_ = 0;                  
  static const int calculateWorkforceCost_ = 1;
  static const int timeStepInteraction_ = 2;   

  /*========================== Public Methods =========================*/

  /*--------------------------- Constructors --------------------------*/
  Firm(void* argument) : Agent(argument) {}

  /*---------------------------- Call Method --------------------------*/
  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case calculateWorkforceCost_ :
        return calculateWorkforceCost(argument);

      case timeStepInteraction_ :
        return timeStepInteraction();            

      default :
        return badFunctionId(functionId);
    }
    return nullptr;
  }

private:
  /*======================= Call Method Helpers =======================*/
  
  /*------------------------------ init -------------------------------*/
	/** Initializes member data for the Firm object.
	 *  @param initVals - a FirmInitValues struct object                 */
  void* init(void* initVals);

  /*----------------------- timeStepInteraction -----------------------*/
	/** Runs the Firm's sequence of actions during a round in the 
   *  simulation loop.                                                 */
  void* timeStepInteraction();

  /*--------------------- calculateWorkforceCost ----------------------*/
	/** Makes a call to the Financial Market to get the total cost of all
   *  workers the Firm employs.                                        */
  void* calculateWorkforceCost(void* makeLog);

  /*---------------------------- badFunctionId ------------------------*/
  /** Handles bad calls to callMethod()                                */ 
  void* badFunctionId(int functionId);


  /*========================= Time Step Helpers =======================*/

  /*------------------------- accumulateInterest ----------------------*/
  /** Increases the amount of outstanding loans due to gained interest.*/ 
  void accumulateInterest();

  /*--------------------- calculateProductionCosts --------------------*/
  /** Calculates this round's production costs.                        */ 
  double calculateProducitonCosts();

  /*------------------------- calculateProfits ------------------------*/
  /** Calculates this round's profits                                  */ 
  double calculateProfits();

  /*--------------------------- payWorkforce --------------------------*/
  /** Takes money out of the Firm's profits to pay Workers and the 
   *  Owner.                                                           */
  bool payWorkforce();

  /*------------------------ calculateLiquidity -----------------------*/
  /** Adjusts the Firm's liquidity based on costs and profit.          */ 
  double calculateLiquidity();

  /*---------------------------- getOwnerLoan -------------------------*/
  /** Attempts to get a loan from the Firm's Owner, taken from the 
   *  Owner's capital.                                                 */
  bool getOwnerLoan();

  /*-------------------------- requestBankLoan ------------------------*/
  /** Attempts to get a loan from k randomly selected banks.      
   *  @param k - number of Banks to approach.                          */
  bool requestBankLoan(int k);

  /*------------------------------ payBank ----------------------------*/
  /** Attempts to pay back outstanding loans.                          */ 
  bool payBank();

  /*----------------------------- resetFirm ---------------------------*/
  /** Resets the firm to default values; occurs when the Firm goes out of
   *  business after getting a negative liquidity. Allows the Firm to 
   *  start anew and re-enter the simulation.                          */ 
  void resetFirm();


  /*=========================== Member Data ===========================*/
  FirmInitValues resetVals;   // Stores sim args in case of firm reset
  double productionCost;      // Current round's production costs
  double liquidity;           // Ongoing liquidity
  double workforceCost;       // Cost to pay workers
  double profit;              // Current round's profits
  int firmId;                 // Unique identified for firm
  Owner* owner;               // Pointer to owner object 
  vector<Loan> debt;          // Outstanding Loans
  ostringstream logText;      // Used to write MASS logs
};

#endif // FIRM_H