#include "Bank.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Bank(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}

/*------------------------------- init ------------------------------*/ 
/** Initializes the Bank Object to specified values.
 *  @param initVals - An instance of the BankInitValues struct.      
 */
void* Bank::init(void* initVals) {
  // Set member data
	BankInitValues initArgs = *(BankInitValues*)initVals;
	interestRate = initArgs.interestRate;
	liquidity = initArgs.liquidity;
	totalDebt = 0;
	bankId = agentId;
  	
	// Pass info to Place to write to BankRegistry
	BankInfo myInfo;
	myInfo.interestRate = interestRate;
	myInfo.liquidity = liquidity;
	myInfo.bankId = bankId;
	myInfo.xLoc = index[0];
	myInfo.yLoc = index[1];
	place->callMethod(FinancialMarket::addBank_, &myInfo);
	
	// Make log of the Bank's creation
	if (initArgs.makeLog) {
		logText.str("");
		logText << endl;
		logText << "Creating Bank " << bankId << " at " << index[0] << 
              ", " << index[1] << endl
						<< "Interest Rate: " << interestRate << endl
						<< "Liquidity: " << liquidity << endl << endl;

		MASS_base::log(logText.str());
	}
	return nullptr;
}

/**------------------------ timeStepInteraction ----------------------/
 * Runs the Bank's sequence of actions in the simulation loop at each
 * round.                                                            
 */
void* Bank::timeStepInteraction() {
  logText.str("");
  logText << "Bank " << bankId << " now acting..." << endl;
	// 1) Pay outstanding loans to other banks
	payLoans();

	// 2) Accumulate interest on outstanding loans
	accumulateInterest();

	// 3) If liquidity is less than 0, get a loan from another bank
	if (liquidity < 0) {
    logText << "  - Liquidity == " << liquidity << ". Getting a loan" << endl;
    getLoan(3);
	}

  MASS_base::log(logText.str());
	return nullptr;
}

/*------------------------------ payLoans ---------------------------*/
/** Attempts to pay back outstanding loans.                          */
bool Bank::payLoans() {
  logText << "  - Paying back loans..." << endl;
	bool couldPay = true;
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double amtToPay  = debt[curLoan].amount / 2;
		if (liquidity > amtToPay) {
      logText << "       > Making Payment $" << amtToPay << " to bank " << debt[curLoan].loaningBankId << endl;
			debt[curLoan].amount -= amtToPay;
			liquidity -= amtToPay;
			totalDebt -= amtToPay;
      // Pass payment to the Financial Market to forward to the bank
			pair<int,double> payment;
			payment.first = debt[curLoan].loaningBankId;
			payment.second = amtToPay; // Positive value for payment
			place->callMethod(FinancialMarket::addBankTransaction_, 
          (void*)&payment);
		}
		else {
      logText << "       > Insufficient Funds. Liquidity now negative" << endl;
			liquidity = -1;
			couldPay = false;
			break;
		}
	}
	return couldPay;
}

/*------------------------- accumulateInterest ----------------------*/
/** Increases the amount of outstanding loans due to gained interest.*/
void Bank::accumulateInterest() {
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double increase = debt[curLoan].amount * debt[curLoan].interestRate;
		debt[curLoan].amount += increase;
		totalDebt += increase;
	}
}

/*------------------------------ getLoan ----------------------------*/
/** Attempts to get a new loan from another Bank to prevent bankruptcy
*   @param k - number of banks to approach for a loan                */
bool Bank::getLoan(int k) {
	BankSelectionArgs selArgs;
	selArgs.minAcceptableInterest = 2134.0;
	selArgs.amountNeeded = liquidity * -1;
	selArgs.k = k;
	BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);
	if (selectedBank.bankId != -1) {
		// Add loan to debt
		Loan newLoan;
		newLoan.amount = liquidity * -1; // Save as a positive value
		newLoan.interestRate = selectedBank.interestRate;
		newLoan.loaningBankId = selectedBank.bankId;
		debt.push_back(newLoan);
		totalDebt += newLoan.amount;
		liquidity += newLoan.amount;
    // Send Loan to the Financial Market
		pair<int,double> loanReceipt;
		loanReceipt.first = selectedBank.bankId;
		loanReceipt.second = newLoan.amount * -1; // Send to bank as negative
		place->callMethod(FinancialMarket::addBankTransaction_, 
        (void*)&loanReceipt);
		return true;
	}
	else {
		return false;
	}
}


/*---------------------------- randomizeRate ------------------------*/
/** Randomizes the bank's interest rate for a new round, seeding it at
 *  the round's counter.           
 *  @param round - round counter in simulation loop                  */ 
void* Bank::randomizeRate(void* round) {
	//srand(*(int*)round + bankId);
	interestRate = (float) rand() / RAND_MAX;
  return nullptr;
}


/*------------------------- reportRoundValues -----------------------*/
/** Creates a new BankInfo struct (defined in FinancialMarket.h) to
  *  write to the Financial Market.                                  */
void* Bank::reportRoundValues() {
	BankInfo myInfo;
	myInfo.interestRate = interestRate;
	myInfo.liquidity = liquidity;
	myInfo.bankId = bankId;
	myInfo.xLoc = index[0];
	myInfo.yLoc = index[1];
	place->callMethod(FinancialMarket::updateBankInfo_, &myInfo);
	return nullptr;
}

/**----------------------- readRoundTransactions ---------------------/
 * Reads all payments made and loans given during this round from the 
 * Financial Market Place.                                           */
void* Bank::readRoundTransactions() {
	void* dummyArg;
	double transactionAmt = *(double*)place->callMethod
      (FinancialMarket::getIncomingTransactionAmt_, dummyArg);
	liquidity += transactionAmt;
	logText.str("");
	logText << "Bank " << bankId << " received amount " << transactionAmt
          << " now has liquidity " << liquidity << endl;
	MASS_base::log(logText.str());
	return nullptr;
}


/*--------------------------- checkBankruptcy -----------------------*/
/** Checks if this Bank has gone bankrupt based on its liquidity. 
  *  @param isBankrupt - a bool passed in by referece; set true if this 
  *         bank is bankrupt                                         */
void* Bank::checkBankruptcy(void* isBankrupt) {
	logText.str("");
	logText << "Bank " << bankId << " checking for bankruptcy" << endl;
	if ((liquidity + totalDebt < 0)) {
		*(bool*)isBankrupt = true;
		logText << "     BANKRUPT!!!" << endl;
	}
	MASS_base::log(logText.str());
	return nullptr;
}


/*---------------------------- badFunctionId ------------------------*/
/** Handles bad calls to callMethod()                                */
void* Bank::badFunctionId(int functionId) {
	logText.str("");
	logText << "Error: agent " << agentId << " ran invalid function: " 
					<< functionId << endl;
	MASS_base::log(logText.str());
	return nullptr;
}

// End Bank.cpp