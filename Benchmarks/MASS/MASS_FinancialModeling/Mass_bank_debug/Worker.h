/*=======================================================================
	Worker Class
	Author: Ian Dudder
=========================================================================
	The Worker Class is an Agent in the Bail-in/Bail-out simulation. In
  each round of the simulation, the Worker will receive wages from its
  Firm, spend wages, and deposit leftover wages in the Bank.
=======================================================================*/

#ifndef WORKER_H
#define WORKER_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

/* Received by init() to initialize member data. */
struct WorkerInitValues {
  double defaultWage; // Income
  int wageDivisor;    // Used to calculate individual wages
  bool makeLog;
};

class Worker : public Agent {
public:
  /*========================== Function IDs ===========================*/
  static const int init_ = 0;
  static const int timeStepInteraction_ = 1;

  /*========================== Public Methods =========================*/
  Worker(void* argument) : Agent(argument) {
    workerId = myBank = myFirm = 0;
    wages = capital = consumptionBudget = 0.0;
  }

  ~Worker();

  virtual int map(int initPopulation, vector<int> size, vector<int> index, Place *curPlace);


  /*---------------------------- Call Method --------------------------*/
  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case timeStepInteraction_ :
        return (void*)timeStepInteraction();

    }
    return nullptr;
  }

private:
  /*======================= Call Method Helpers =======================*/

  /*------------------------------ init -------------------------------*/
	/** Initializes member data for the Worker object and writes the wages
   *  to the Financial Market it resides on for its Firm to read.
	 *  @param initVals - a WorkerInitValues struct object               */
  void* init(void* initVals);

  /*----------------------- timeStepInteraction -----------------------*/
	/** Runs the Workers's sequence of actions during a round in the 
   *  simulation loop.                                                 */
  bool* timeStepInteraction();


  /*===================== Simulation Interaction ======================*/

  /*-------------------------- receiveWages ---------------------------*/
	/** Increase capital based on income.                                */
  void receiveWages();

  /*----------------------------- consume -----------------------------*/
	/** Spend wages.                                                     */
  void consume();

  /*--------------------------- depositFunds --------------------------*/
	/** Sends a deposit to the Worker's Bank.                            */
  bool depositFunds();

  /*=========================== Member Data ===========================*/
  double wages;               // Income
  double capital;             // Saved money
  double consumptionBudget;   // Amount spend on products each step
  int workerId;               // Unique identifier for worker
  int myBank;                 // Identifies bank holding personal account
  int myFirm;                 // Identifies firm this household works for
  ostringstream logText;      // Used to make MASS logs
};

#endif // WORKER_H