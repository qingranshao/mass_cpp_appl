#include "Owner.h"

using namespace std;

/*--------------------------- Constructors --------------------------*/
Owner::Owner() : firmId{-1}, capital{0.0} {}

Owner::Owner(int firmId_, double capital_) {
    firmId = firmId_;
    capital = capital_;
}

/*---------------------------- Destructors --------------------------*/
Owner::~Owner() { /*No dynamic memory to delete.*/ }

/*------------------------------ Getters ----------------------------*/

double Owner::getCapital() const {
    return capital;
}

/*------------------------------ Setters ----------------------------*/

void Owner::setCapital(double newCapital) {
    capital = newCapital;
}

