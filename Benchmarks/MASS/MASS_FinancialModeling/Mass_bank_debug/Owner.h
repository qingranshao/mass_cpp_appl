/*=======================================================================
	Owner Class
	Author: Ian Dudder
=========================================================================
	The Owner Class owns a single Firm in the Bail-in/Bail-out simulation.
  If the Firm's profits are positive, the Owner gets paid. When the Firm
  has a negative liquidity, the Owner will attempt to provide the Firm 
  with a loan to bring them back into the positive.
=======================================================================*/

#ifndef OWNER_H
#define OWNER_H

#include <iostream>

class Owner {
public:
    /*========================== Public Methods =========================*/

    /*--------------------------- Constructors --------------------------*/
    Owner();

    Owner(int firmId_, double capital_);

    /*---------------------------- Destructors --------------------------*/
    virtual ~Owner();
    
    /*------------------------------ Getters ----------------------------*/
    double getCapital() const;

    /*------------------------------ Setters ----------------------------*/
    void setCapital(double newCapital);


private:
    /*=========================== Member Data ===========================*/
    double capital;         // Amount of savings
    int firmId;             // ID of the Firm this Owner runs
};

#endif // OWNER_H