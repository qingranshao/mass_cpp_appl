/*=======================================================================
	Bail-in/Bail-out Simulation
	Author: Ian Dudder
=========================================================================
	The Bail-in/Bail-out Simulation models a financial market comprised of
  Households (i.e. Workers, Owners), Firms, and Banks, and observes as
  these three types of entities interact with one another until a Bank
  goes bankrupt and can't continue. This simulation models the scenario
  posed by the paper titled "To Bail-out or to Bail-in? Answers from an
  Agent-Based Model" by Peter Klimek, et al.

  This implementation leverages the MASS C++ library by Professor 
  Munehiro Fukuda at the University of Washington, Bothell.
=======================================================================*/

#include "MASS.h"
#include "Firm.h"
#include "Bank.h"
#include "Worker.h"
#include "FinancialMarket.h"
#include "Timer.h"	//Timer
#include <stdlib.h> // atoi
#include <unistd.h>
#include <vector>
#include <fstream>

using namespace std;

Timer timer;

bool readSimulationArguments(string filename, int &numWorkers, 
  int &numFirms, int &numBanks, double &initialProductionCost,
  double &initialInterest, double &initialLiquidity);


int main( int argc, char *args[] ) {
	// Ensure all Simulation Args are Present.
	if ( argc != 10 ) {
		cerr << "usage: ./main username password machinefile.txt port" 
         << "nProc nThreads numTurns simArgs.txt [show]" << endl;
		return -1;
	}
  
	// Receive Simulation Arguments
	char *arguments[4];
	arguments[0] = args[1]; 	      // username
	arguments[1] = args[2]; 	      // password
	arguments[2] = args[3]; 	      // machinefile
	arguments[3] = args[4]; 	      // port
	int nProc = atoi( args[5] );    // number of processes
	int nThr = atoi( args[6] );     // number of threads
  int numTurns = atoi ( args[7] );// number of simulation rounds 
	string simArgFile = args[8];    // Text file of simulation values
	bool show = (args[9][0] == 's');// Logging/no logging
	void* printLog = &show;
  ostringstream logText;
	
	// Initialize MASS with the machine information
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Simulation Start

	// Set up Simulation Arguments
  const int marketHandle = 1;
	const int bankHandle = 2;
	const int firmHandle = 3;
	const int workerHandle = 4;
	int numWorkers, numFirms, numBanks;
	double initialProductionCost, initialInterest, initialLiquidity;

  // Read in simulation arguments from the file
	bool successfulRead = readSimulationArguments(simArgFile, numWorkers, 
    numFirms, numBanks, initialProductionCost, initialInterest, 
    initialLiquidity);
	if (!successfulRead) {
    cerr << "Error: Unable to open argument file" << endl;
		return -1;
  }
	double defaultWage = initialProductionCost * numFirms * 0.1 * 0.8;
	char* dummyArg = "A";

	/* Create the Financial Market large enough to house one firm per element */
	float placeSize = (float)(sqrt(numFirms));

	Places *market = new Places(marketHandle, "FinancialMarket", 1, 
      dummyArg, 1, 2, (int)(placeSize + 0.5), (int)(placeSize + 1));
  pair<int,int> numAgents = {numBanks, numFirms};
	market->callAll(FinancialMarket::init_, (void*)&numAgents, sizeof(pair<int,int>));

	/* Create Banks */
	BankInitValues bankArgs;
	bankArgs.interestRate = initialInterest;
	bankArgs.liquidity = initialLiquidity;
	bankArgs.makeLog = show;
	void* bankInit = &bankArgs;

  Agents* banks = new Agents(bankHandle, "Bank", dummyArg, 1, market, numBanks);
	banks->callAll(Bank::init_, bankInit, sizeof(BankInitValues));

	// Have Banks broadcast their information
	market->callAll(FinancialMarket::prepBankInfoExchange_, dummyArg, 1);
	market->exchangeAll(marketHandle, FinancialMarket::exchangeBankInfo_);
	market->callAll(FinancialMarket::updateBankRegistry_, dummyArg, 1);

	/* Create Firms */
	FirmInitValues firmArgs;
	firmArgs.productionCost = initialProductionCost;
	firmArgs.liquidity = initialLiquidity;
	firmArgs.makeLog = show;
	void* firmInit = &firmArgs;
	
	Agents* firms = new Agents(firmHandle, "Firm", dummyArg, 1, market, numFirms);
	firms->callAll(Firm::init_, firmInit, sizeof(FirmInitValues));

	/* Create Workers */
	WorkerInitValues workerArgs;
	workerArgs.defaultWage = defaultWage;
	workerArgs.wageDivisor = numWorkers;
	workerArgs.makeLog = show;
	void* workerInit = &workerArgs;

	Agents* workers = new Agents(workerHandle, "Worker", dummyArg, 1, market, numWorkers);
	workers->callAll(Worker::init_, workerInit, sizeof(WorkerInitValues));

	// Calculate workforce cost based on workers wages
	firms->callAll(Firm::calculateWorkforceCost_, printLog, sizeof(bool));

	/* run simulation */
	bool* bankruptcy;
	*bankruptcy = false;

  for (int round = 1; !*bankruptcy && round <= numTurns; round++) {
    market->callAll(FinancialMarket::seedRound_, (void*)&round, sizeof(int));

		// Move 1: Firms act
		logText.str("");
		logText << "==================== Round " << round << " ====================" << endl << endl;
		logText << "==================== Move 1 - Firms Act ====================" << endl;
		MASS_base::log(logText.str());
		firms->callAll(Firm::timeStepInteraction_, dummyArg, 1);
		
		// Move 2: Workers act
		logText.str("");
		logText << "==================== Move 2 - Workers Act ====================" << endl;
		MASS_base::log(logText.str());
		workers->callAll(Worker::timeStepInteraction_, dummyArg, 1);

		// Move 3: Banks act
		logText.str("");
		logText << "==================== Move 3 - Banks Act ====================" << endl;
		MASS_base::log(logText.str());
		banks->callAll(Bank::timeStepInteraction_, dummyArg, 1);
    banks->callAll(Bank::checkBankruptcy_, (void*)bankruptcy, sizeof(bool));
		if (*bankruptcy) {
			logText.str("");
			logText << "Bankruptcy occurred" << endl;
			MASS_base::log(logText.str());
			break;
		}

		// Move 4: Forward all Bank Transactions to Banks
		logText.str("");
		logText << "==================== Move 4 - Market Forwards Transactions ====================" << endl;
		MASS_base::log(logText.str());
		market->callAll(FinancialMarket::prepForIncomingTransactions_, dummyArg, 1);
		market->exchangeAll(marketHandle, FinancialMarket::exchangeOutgoingTransactions_);
		market->callAll(FinancialMarket::manageIncomingTransactions_, dummyArg, 1);

		// Move 5: Banks read in round's payments and report next round's values
		logText.str("");
		logText << "==================== Move 5 - Read Round Values ====================" << endl;
		MASS_base::log(logText.str());
		banks->callAll(Bank::readRoundTransactions_, dummyArg, 1);
		banks->callAll(Bank::randomizeRate_, dummyArg, 1);
		banks->callAll(Bank::reportRoundValues_, dummyArg, 1);

		// Broadcast next round's interest rates
		logText.str("");
		logText << "==================== Move 6 - Broadcasting Round Rates ====================" << endl;
		MASS_base::log(logText.str());
		market->callAll(FinancialMarket::prepBankInfoExchange_, dummyArg, 1);
		market->exchangeAll(marketHandle, FinancialMarket::exchangeBankInfo_);
		market->callAll(FinancialMarket::updateBankRegistry_, dummyArg, 1);
    market->callAll(FinancialMarket::resetRoundValues_, dummyArg, 1);
	} 
	
	long elaspedTime_END =  timer.lap(); // Simulation End
  logText.str("");
  logText << "\nEnd of Simulation. Elapsed time using MASS framework with " << nProc
          << " processes, " << nThr << " threads, and " << numTurns << " turns: "
          << elaspedTime_END << endl; 
  MASS_base::log(logText.str());

	MASS::finish( );
}


/* Reads in the arguments from a file for this simulation */
bool readSimulationArguments(string filename, int &numWorkers, int &numFirms, int &numBanks, 
double &initialProductionCost, double &initialInterest, double &initialLiquidity) {
	bool successfulRead = false;
	string varName;
	ifstream simArgs;
  simArgs.open(filename);

  if (!simArgs) {
    successfulRead = false;
  }
	else {
		simArgs >> varName >> numWorkers;
		simArgs >> varName >> numFirms;
		simArgs >> varName >> numBanks;
		simArgs >> varName >> initialProductionCost;
		simArgs >> varName >> initialInterest;
		simArgs >> varName >> initialLiquidity;
		successfulRead = true;
	}
    
  simArgs.close();
	return successfulRead;
}