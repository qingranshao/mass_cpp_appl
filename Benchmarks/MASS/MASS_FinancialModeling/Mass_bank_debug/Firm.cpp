#include "Firm.h"
#include <time.h>

using namespace std;


extern "C" Agent* instantiate(void* argument) {
  return new Firm(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}


/*------------------------------ init -------------------------------*/
/** Initializes member data for the Firm object.
 *  @param initVals - a FirmInitValues struct object                 */
void* Firm::init(void* initVals) {
  FirmInitValues initArgs = *(FirmInitValues*) initVals;
  productionCost = resetVals.productionCost = initArgs.productionCost;
  liquidity = resetVals.liquidity = initArgs.liquidity;
  profit = 0;
  firmId = agentId;
  owner = new Owner(firmId, 0);
  place->callMethod(FinancialMarket::addFirm_, &firmId);

  if (initArgs.makeLog) {
    logText.str("");
    logText << endl;
    logText << "Creating Firm " << agentId << " at " << index[0] << 
              ", " << index [1] << endl
            << "Production Cost: " << productionCost << endl
            << "Liquidity: " << liquidity << endl << endl;
    MASS_base::log(logText.str());  
  }
  return nullptr;
}


/*--------------------- calculateWorkforceCost ----------------------*/
/** Makes a call to the Financial Market to get the total cost of all
 *  workers the Firm employs.                                        */
void* Firm::calculateWorkforceCost(void* makeLog) {
  void* dummyArg;
  workforceCost = *(double*)place->callMethod(FinancialMarket::calculateWorkforceCost_, dummyArg);
  return nullptr;
}


/*---------------------------- badFunctionId ------------------------*/
/** Handles bad calls to callMethod()                                */
void* Firm::badFunctionId(int functionId) {
  logText.str("");
  logText << "Error: agent " << agentId << " ran invalid function: " 
          << functionId << endl;
  MASS_base::log(logText.str());
  return nullptr;
}


/*----------------------- timeStepInteraction -----------------------*/
/** Runs the Firm's sequence of actions during a round in the 
 *  simulation loop.                                                 */
void* Firm::timeStepInteraction() {
  logText.str("");
  logText << "Firm " << firmId << " now acting in round..." << endl;

  // 1) Calculate Interest on outstanding loans
  accumulateInterest();
  logText << "  - Accumulated interest on outstanding loans" << endl;

  // 2) Calculate production costs
  calculateProducitonCosts();
  logText << "  - Production Costs: $" << productionCost << endl;

  // 3) Calculate Profits
  calculateProfits();
  logText << "  - Profits: $" << profit << endl;

  // 4) Pay Worker Wages
  logText << "  - Paying workforce..." << endl;
  payWorkforce();
  logText << "  - Profits now $" << profit << endl;

  // 5) Calculate liquidity
  calculateLiquidity();
  logText << "  - Liquidity: " << liquidity << endl;

  // 6) Pay Banks for outstanding loans
  logText << "  - Paying Banks..." << endl;
  payBank();
  logText << "  - Paid banks. Liquidity now " << liquidity << endl;

  bool costsCovered = (liquidity >= 0);
  
  // 7) If liquidity is < 0...
  if (!costsCovered) {
    logText << "  - Costs not covered..." << endl;
    // 7a) Try to get a loan from the owner.
    costsCovered = getOwnerLoan();
    
    // 7b) If the owner loan failed, try to get a loan from the Bank
    if (!costsCovered && debt.size() <= 0) {
      logText << "  - Failed to get owner loan" << endl;
      costsCovered = requestBankLoan(3);
      logText << "  - Requested bank loan " << endl;
    }
  }
  
  logText << "Firm " << firmId << " ending turn" << endl;
  MASS_base::log(logText.str());
  if (liquidity < 0)
    resetFirm();
  return (void*)&costsCovered;
}


/*------------------------- accumulateInterest ----------------------*/
/** Increases the amount of outstanding loans due to gained interest.*/ 
void Firm::accumulateInterest() {
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    debt[curLoan].amount = debt[curLoan].amount * (1.0 + debt[curLoan].interestRate);
  }
}


/*--------------------- calculateProductionCosts --------------------*/
/** Calculates this round's production costs.                        */ 
double Firm::calculateProducitonCosts() {
  double modifier = (float) ( (float)rand() / RAND_MAX + 0.5);
  productionCost *= modifier;
  return productionCost;
}


/*------------------------- calculateProfits ------------------------*/
/** Calculates this round's profits                                  */ 
double Firm::calculateProfits() {
  double x = (float) ( (float)rand() / RAND_MAX + 0.5);
  profit = (productionCost * x) - productionCost;
  return profit;
}

/*--------------------------- payWorkforce --------------------------*/
/** Takes money out of the Firm's profits to pay Workers and the 
 *  Owner.                                                           */
bool Firm::payWorkforce() {
  logText << "       > Paying Workers $" << workforceCost << endl;
  bool paidOwner = false;
  profit -= workforceCost;    // Pay Workers
  double dividends;           // Owner's cut        
  if (profit > 0) {
    dividends = profit * 0.2;
    profit -= dividends;
    logText << "       > Paying Owner $" << dividends << endl;
  }
  else {
    dividends = 0;
  }
  owner->setCapital(owner->getCapital() + dividends);
  return paidOwner;
}


/*------------------------ calculateLiquidity -----------------------*/
/** Adjusts the Firm's liquidity based on costs and profit.          */ 
double Firm::calculateLiquidity() {
    liquidity += profit;
    return liquidity;
}


/*------------------------------ payBank ----------------------------*/
/** Attempts to pay back outstanding loans.                          */ 
bool Firm::payBank() {
  bool couldPay = true;
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    double amtToPay = debt[curLoan].amount / 2;
    logText << "       > Current loan: $" << debt[curLoan].amount << endl;
    logText << "           - Attempting to pay: $" << amtToPay << endl;
    //if (liquidity > amtToPay) {
      debt[curLoan].amount -= amtToPay;
      liquidity -= amtToPay;
      pair<int,double> payment;
      payment.first = debt[curLoan].loaningBankId;
      payment.second = amtToPay; // Positive value
      place->callMethod(FinancialMarket::addBankTransaction_, (void*)&payment);
      logText << "           - Paid!" << endl;
    /*}   
    else {
      liquidity = -1;
      couldPay = false;
      logText << "           - Not paid!" << endl;
      break;
    }*/
  }
  return couldPay;
}


/*---------------------------- getOwnerLoan -------------------------*/
/** Attempts to get a loan from the Firm's Owner, taken from the 
 *  Owner's capital.                                                 */
bool Firm::getOwnerLoan() {
  logText << "     Seeking Owner Loan for $" << (liquidity*-1) << endl;
  // Seek loan from owner when liquidity is negative.
  // Must make sure the owner can bring the firm back into positive.
  if (owner->getCapital() > liquidity * -1) {
    logText << "     Approved!" << endl;
    double amount = liquidity * -1;
    owner->setCapital(owner->getCapital() - amount);
    liquidity += amount;
    return true;
  }
  return false;
}


/*-------------------------- requestBankLoan ------------------------*/
/** Attempts to get a loan from k randomly selected banks.      
 *  @param k - number of Banks to approach.                          */
bool Firm::requestBankLoan(int k) {
  // Select the bank with the lowest interest rate
  BankSelectionArgs selArgs;
  selArgs.minAcceptableInterest = 2134.0;
  selArgs.amountNeeded = liquidity * -1;
  selArgs.k = k;
  BankInfo selectedBank = *(BankInfo*)place->callMethod
      (FinancialMarket::selectFromKBanks_, &selArgs);

  if (selectedBank.bankId != -1) {
    // Send a loan acceptance through a bank transaction
    Loan newLoan;
    newLoan.amount = liquidity * -1; // Positive value
    newLoan.interestRate = selectedBank.interestRate;
    newLoan.loaningBankId = selectedBank.bankId;
    debt.push_back(newLoan);
    liquidity += newLoan.amount;

    /*Add loan information to Financial Market's Bank Transactions*/
    pair<int,double> loanReceipt;
    loanReceipt.first = selectedBank.bankId;
    loanReceipt.second = newLoan.amount * -1; // Negative value
    place->callMethod(FinancialMarket::addBankTransaction_, (void*)&loanReceipt);
    return true;
  }
  else {
    return false;
  }
}


/*----------------------------- resetFirm ---------------------------*/
/** Resets the firm to default values; occurs when the Firm goes out of
 *  business after getting a negative liquidity. Allows the Firm to 
 *  start anew and re-enter the simulation.                          */ 
void Firm::resetFirm() {
  logText.str("");
  logText << "Resetting Firm " << firmId << endl;
  MASS_base::log(logText.str());
  productionCost = resetVals.productionCost;
  liquidity = resetVals.liquidity;
  profit = 0;
  owner->setCapital(liquidity * ((float) rand() / RAND_MAX + 0.5));
  debt.clear();
}

// End Firm.cpp