//
// Created by sarah
//

#include "Macrophage.h"
#include "TB_Place.h"
#include "MASS_base.h"        // for logging
#include <sstream>            // ostringstream
#include "math.h"

extern "C" Agent *instantiate(void *argument) {
    return new Macrophage(argument);
}

extern "C" void destroy(Agent *object) {
    delete object;
}

/**
 * - initializes start state and migrates the initMacroNum macrophages to random Places
 * - manageAll() must be called after to update macrophage's positions
 *
 * @param whereToSpawnVec
 * @return
 */
void *Macrophage::init(void *whereToSpawnPtr) {
    int *whereToSpawn = reinterpret_cast<int *>(whereToSpawnPtr);

    //debug
    cerr << "Macrophage " << agentId << " init(), whereToSpawnVec size = " << initMacroNum;
    cerr << " , contains [";
    for (int i = 0; i < initMacroNum; i++) {
        cerr << " {" << whereToSpawn[i] << "} ";
    }
    cerr << "] "<< endl;

    // set up agent variables
    isSpawner = agentId < spawnPtNum;
    state = RESTING;
    dayInfected = -1;
    intracellularBacteria = 0;
    reqPlace = -1;

    // add state data to migratableData pointer
    addMigrationData();

    // move to initial position
    if (!isSpawner) {
        cerr << "I'm " << agentId << " and I'm choosing index " << agentId - spawnPtNum << " from the wheretoSpawn vector, sized " << sizeof(whereToSpawn) / sizeof(whereToSpawn[0]) << endl;
        //migrate(toXY(whereToSpawn[agentId - spawnPtNum], place->size[0]));
    }
       // spawners move to blood vessel Places
   else {
        vector<int> vessels = bloodVessels(place->size[0]);
        migrate(toXY(vessels[agentId], place->size[0]));
        cerr << "I'm " << agentId << " spawner and I'm moving place " << whereToSpawn[agentId - spawnPtNum] << endl;
    }
    isInitialized = true;

    return NULL;
}

// requests a new place from the current place's moore's area to move to based on highest chemokine level
// - assumes TB_Places have set out message to their current chemokine level and exchangeBoundary() has been called
void *Macrophage::requestMove() {
    if (!isSpawner) {
        int *reqPlacePtr = (int*)place->callMethod(TB_Place::findNewPlace_, (void*) defaultMSG);
        if (reqPlacePtr != nullptr) {
            reqPlace = *reqPlacePtr;
            place->callMethod(TB_Place::macroRequestMove_, &reqPlace);
        }

    }
    return NULL;
}

// must call manage all after this function
void *Macrophage::move() {

    if (!isSpawner) {
        // save data to carry along with Agent as it moves
        modifyMigrationData();

        int currPid = toId(index[0], index[1], place->size[0]);
        // check myNeighbors for approval to move there
        for (int i = 0; i < mooreNeighborNum; i++) {
            int *approvedPidPtr = (int *) place->callMethod(TB_Place::getRemotePlaceOutMessage_, myNeighbors[i]);
            int approvedPid = (approvedPidPtr != NULL) ? *approvedPidPtr : -1;

            if (approvedPid == currPid) {
                // remove self form current Place
                place->callMethod(TB_Place::setMacroFalse_, (void *) defaultMSG);
                vector<int> newXandY = toXY(reqPlace, place->size[0]);
                migrate(newXandY);
                break;
            }
        }
        // reset requested Place regardless if move happened or not
        reqPlace = -1;
    }
    return NULL;
}


void *Macrophage::react(void *day) {
    if (!isSpawner) {
        // set arrival on current Place, regardless if moved or not
        place->callMethod(TB_Place::setMacroTrue_, (void *) defaultMSG);

        int today = *(int *) day;

        bool onTCell = false;

        if (today >= tCellEntrance) {
            onTCell = *(bool *) place->callMethod(TB_Place::hasTCell_, (void *) defaultMSG);
        }

        bool onBacteria = *(bool *) place->callMethod(TB_Place::hasBacteria_, (void *) defaultMSG);

        switch (state) {
            case RESTING:
                if (onBacteria) {
                    // kill bacteria
                    place->callMethod(TB_Place::eatBacteria_, (void *) defaultMSG);
                    state = INFECTED;
                    dayInfected = today;
                    growInfectedIntraCBact(dayInfected);
                }
                break;

            case INFECTED:
                // max out and broadcast chemokine
                maxChemokineCurrPlace();
                // check if INFECTED macrophage will be activated by T-cell
                if (onTCell) {
                    state = ACTIVATED;
                    intracellularBacteria = 0;
                    break;
                }
                // grow intra-cellular bacteria at the infected macrophage rate
                growInfectedIntraCBact(today);

                // if not activated, check to see if internal bacterial level will cause this macrophage to become
                // CHRONICALLY_INFECTED
                if (intracellularBacteria >= chronicInfection) {
                    state = CHRONICALLY_INFECTED;
                }
                break;

            case ACTIVATED:// kill bacteria
                place->callMethod(TB_Place::eatBacteria_, (void *) defaultMSG);
                // max out chemokine
                maxChemokineCurrPlace();
                break;

            case CHRONICALLY_INFECTED:
                // max out chemokine
                maxChemokineCurrPlace();

                // grow intra-cellular bacteria at the chronically infected macrophage rate
                growChronicIntraCBact();
                if (intracellularBacteria >= bacterialDeath || onTCell) {
                    state = DEAD;
                    // tell curr place this macrophage has exploded, releaseing bacteria
                    place->callMethod(TB_Place::macrophageBacterialDeath_, (void *) defaultMSG);
                    kill();
                }
                break;

            default:
                break;
        }
    }
    return NULL;
}

// maxes the chemokine value for the current place this Macrophage resides on and sets the place's out message to
// chemokine == maxChemokine
void Macrophage::maxChemokineCurrPlace() {
    place->callMethod(TB_Place::maxOutChemokine_, (void *) defaultMSG);
    place->callMethod(TB_Place::broadcastChemokineLevel_, (void *) defaultMSG);
}

/**
 * Grows bacteria at the rate 2*t + 1, where t = today - day_infected
 *
 * @param today - current day (iteration number)
 */
void Macrophage::growInfectedIntraCBact(int today) {
    int t = today - dayInfected;
//    intracellularBacteria = (2 * t) + 1;
    // demo with 10*1
    intracellularBacteria = pow(10.0 * t, 2.0) + 1;
}

/**
 * Grows bacteria inside the macrophage at a constant rate of an additional 2 bacteria per time step: a much slower
 * rate than when it was in the INFECTED state
 */
void Macrophage::growChronicIntraCBact() {
    // intracellularBacteria *= 2;
    intracellularBacteria += 2;
}

// manageAll should be called after this
void *Macrophage::spawnMacro() {
    // only spawners do this
    if (isSpawner) {
        bool shouldSpawn = *(bool *) place->callMethod(TB_Place::spawnMacro_, (void *) defaultMSG);
        if (shouldSpawn) {
            spawn(1, emptyVec, vectorSizeOf(emptyVec));
            place->callMethod(TB_Place::stopSpawningMacro_, (void *) defaultMSG);
        }
    }
    return NULL;
}

void *Macrophage::addMigrationData() {
    migratableDataSize = sizeof(struct macro_migratable_data);
    migratableData = malloc(migratableDataSize);

    modifyMigrationData(); // add agent state data (isSpawner, isInitialized, state, etc.) to migratableData pointer
    return NULL;
}

void *Macrophage::modifyMigrationData() {
    ((struct macro_migratable_data *)migratableData)->_isSpawner = isSpawner;
    ((struct macro_migratable_data *)migratableData)->_isInitialized = isInitialized;

    ((struct macro_migratable_data *)migratableData)->_state = state;
    ((struct macro_migratable_data *)migratableData)->_dayInfected = dayInfected;
    ((struct macro_migratable_data *)migratableData)->_intracellularBacteria = intracellularBacteria;

    ((struct macro_migratable_data *)migratableData)->_reqPlace = reqPlace;

    return NULL;
}

void *Macrophage::newMacroInit() {
    if (!isInitialized) {
        // set up agent variables
        isSpawner = false;
        state = RESTING;
        dayInfected = -1;
        intracellularBacteria = 0;
        reqPlace = -1;
        isInitialized = true;

        // add state data to migratableData pointer
        addMigrationData();
    }
    return NULL;
}

void *Macrophage::writeMacroToXML() {
    convert.str("");
    convert << "\n<xagent>";
    convert << "\n<name>Macrophage</name>";
    convert << "\n<m_id>" << agentId << "</m_id>";
    convert << "\n<m_x>" << index[0]  << "</m_x>";
    convert << "\n<m_y>" << index[1]  << "</m_y>";
    int writeState = static_cast<int>(state);
    convert << "\n<state>" << writeState << "</state>";
    convert << "\n<intracellular_bacteria>" <<  intracellularBacteria << "</intracellular_bacteria>";
    convert << "\n</xagent>";
    MASS_base::log(convert.str());
    return NULL;
}
