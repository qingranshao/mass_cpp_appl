//
// Created by sarah
//

#ifndef TBCONSTANTS_H
#define TBCONSTANTS_H

// Environmental constants----------------------------------------------------------------------------------------------
#define initMacroNum 100

#define spawnPtNum 4

// Number of myNeighbors in Moore's area
#define mooreNeighborNum 8

// Array form of cardinals (N, NE, E, SE, S, SW, W, NW)
int myNeighbors[mooreNeighborNum][2] = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1} } ;


// Number of days between points of time when extra-cellular bacteria grows radially out by one unit
#define bacterialGrowth 10

// T-cell entrance
#define tCellEntrance 10

// Main constants----------------------------------------------------------------------------------------------

#define defaultMSG "argument\0"

#define handleTB_Place 1
#define handleMacro    2
#define handleTCell    3

// Macrophage constants ------------------------------------------------------------------------------------------------

// Max chemokine level dispersed by an INFECTED and CHRONICALLY_INFECTED agent - also how many days the signal lasts at
// the current Place
#define maxChemokine 2

// Number of internal bacteria that causes an INFECTED macrophage to become CHRONICALLY_INFECTED
#define chronicInfection 75

// Number of internal bacteria that causes a CHRONICALLY_INFECTED macrophage to die
#define bacterialDeath 100

// General Helper Functions used by Places and Agents ------------------------------------------------------------------

template <typename T>
size_t vectorSizeOf(const class vector<T> &v) {
    return sizeof(T) * v.size();
}

vector<void*> emptyVec;

// from x, y coordinates to 1D index
int toId(int x,int y,int n) {
    return ( (x * n) + y );
}

static vector<int> toXY(int placeID, int sideSize) {
    vector<int> xAndY;
    xAndY.push_back(placeID / sideSize);
    xAndY.push_back(placeID % sideSize);
    return xAndY;
}

int relativeDirToIndex( vector <int> relativeDirection) {
    for (int i = 0; i < mooreNeighborNum; i++) {
        if (myNeighbors[i][0] == relativeDirection[0] && myNeighbors[i][1] == relativeDirection[1]) {
            return i;
        }
    }
    return -1;
}

vector<int> neighborPidToRelativeDir(int pID, int neighborPID, int sideLength) {
    vector<int> neighborXandY = toXY(neighborPID, sideLength);
    vector<int> yourXandY = toXY(pID, sideLength);
    vector<int> delta;
    delta.push_back(yourXandY[0] - neighborXandY[0]);
    delta.push_back(yourXandY[1] - neighborXandY[1]);
    return delta;
}

int* neighborPiDToNeighborPtr(int pID, int neighborPID, int sideLength) {
    vector<int> delta = neighborPidToRelativeDir(pID, neighborPID, sideLength);
    int index = relativeDirToIndex(delta);
    return myNeighbors[index];
}

vector<int> bloodVessels(int sideLength) {
    int lowI = ( sideLength/2 )/2;
    int highI = sideLength - (lowI + 1);

    vector<int> vessels;
    vessels.push_back( toId(lowI, lowI, sideLength) );
    vessels.push_back( toId(lowI, highI, sideLength) );
    vessels.push_back( toId(highI, highI, sideLength) );
    vessels.push_back( toId(highI, lowI, sideLength) );

    return vessels;
}

#endif //TBCONSTANTS_H
