/**
 * @author: Sarah Panther
 * @since : 1/3/2020
 *
 * TB Place -
 * a position in the simulation grid that can contain a single bacteria, up to one T-cell and one macrophage, and an
 * chemokine (chemical trail left by INFECTED and CHRONICALLY INFECTED macrophages)
 */

#ifndef TB_PLACE_H
#define TB_PLACE_H

#include <cstring>
#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include "TBConstants.h"
#include <sstream>
#include <signal.h> // signal( )
#include <stdlib.h> // exit
#include <stdio.h> // perror
#include <iostream> // cout

using namespace std;

class TB_Place : public Place {

public:

    static void sigsev_handler( int signo ) {
        cerr << "Segmentation fault occured: " << signo << endl;
        exit(-2);
    }

    // define functionId's that will 'point' to the functions they represent.
    static const int init_ = 0;

    static const int broadcastBacteria_ = 1;
    static const int checkNeighborBacteria_ = 2;

    static const int decayChemokineAndGrowBacteria_ = 3;

    static const int cellRecruitment_ = 4;

    static const int broadcastChemokineLevel_ = 5;
    // macrophage movement
    static const int sendMacroMoveReqs_ = 6;
    static const int approveMacroMove_ = 7;
    static const int broadcastMacroApproval_ = 8;
    static const int macrophageMovement_ = 9;
    //tcell movement
    static const int sendTCellMoveReqs_ = 29;
    static const int approveTCellMove_ = 30;
    static const int broadcastTCellApproval_ = 31;
    static const int tCellMovement_ = 10;

    static const int receiveMaxedChemokineMsgs_ = 35;
    static const int updateChemokineAfterMsg_ = 36;
    static const int spreadBacteriaFromMacroDeaths_ = 11;
    static const int receiveBacteriaFromMacroDeaths_ = 24;


    // for testing
    static const int printHasMacroCorrectly_ = 12;
    static const int writeStateToXMLFile_ = 37;

    // for macrophages and t-cells
    static const int getRemotePlaceOutMessage_ = 13;

    static const int findNewPlace_ = 33;

    static const int macroRequestMove_ = 14;
    static const int setMacroFalse_ = 15;
    static const int setMacroTrue_ = 16;

    static const int tCellRequestMove_ = 32;
    static const int setTCellFalse_ = 17;
    static const int setTCellTrue_ = 18;
    static const int hasTCell_ = 19;

    static const int hasBacteria_ = 20;
    static const int eatBacteria_ = 21;

    static const int maxOutChemokine_ = 22;
    static const int macrophageBacterialDeath_ = 23;

    static const int spawnMacro_ = 26;
    static const int spawnTCell_ = 25;

    static const int stopSpawningMacro_ = 27;
    static const int stopSpawningTCell_ = 28;


    TB_Place(void *argument) : Place(argument) {
        if ( signal( SIGSEGV, sigsev_handler ) == SIG_ERR ) {
             perror( "signal function" );
             exit( -1 );
        }
    };

    virtual void *callMethod(int functionId, void *argument) {
        switch (functionId) {

            // State functions -----------------------------------------------------------------------------------------

            // when calling init(), argument is sorted list of place ID's that will be initialized with macrophages
            case init_ : return init(argument);

            case broadcastBacteria_ : return broadcastBacteria();

            case checkNeighborBacteria_ : return checkNeighborBacteria(argument);

            case decayChemokineAndGrowBacteria_ : return decayChemokineAndGrowBacteria();

            case cellRecruitment_ : return cellRecruitment(argument);

            case broadcastChemokineLevel_ : return broadcastChemokineLevel();
            // macrophage movement
            case sendMacroMoveReqs_ : return sendMacroMoveReqs();

            case approveMacroMove_ : return approveMacroMove();

            case broadcastMacroApproval_ : return broadcastMacroApproval();

            case macrophageMovement_ : return macrophageMovement();
            // tcell movement
            case sendTCellMoveReqs_ : return sendTCellMoveReqs();

            case approveTCellMove_ : return approveTCellMove();

            case broadcastTCellApproval_ : return broadcastTCellApproval();

            case tCellMovement_ : return tCellMovement();

            case receiveMaxedChemokineMsgs_ : return receiveMaxedChemokineMsgs();

            case updateChemokineAfterMsg_ : return updateChemokineAfterMsg();

            case spreadBacteriaFromMacroDeaths_ : return spreadBacteriaFromMacroDeaths();

            case receiveBacteriaFromMacroDeaths_ : return receiveBacteriaFromMacroDeaths();

            case getRemotePlaceOutMessage_ : return getRemotePlaceOutMessage(argument);

            // Test functions ------------------------------------------------------------------------------------------
            case printHasMacroCorrectly_ : return printHasMacroCorrectly();

            case writeStateToXMLFile_ : return writeStateToXMLFile();

            // Functions for macrophages and t-cells to call -----------------------------------------------------------

            case findNewPlace_ : return findNewPlace();

            // argument == neighbor pId the macrophage is requesting to move to from current place
            case macroRequestMove_ : return macroRequestMove(argument);

            case setMacroFalse_ : return setMacroFalse();

            case setMacroTrue_ : return setMacroTrue();

            case tCellRequestMove_ : return tCellRequestMove(argument);

            case setTCellFalse_ : return setTCellFalse();

            case setTCellTrue_ : return setTCellTrue();

            case hasTCell_ : return hasTCell();

            case hasBacteria_ : return hasBacteria();

            case eatBacteria_ : return eatBacteria();

            case maxOutChemokine_ : return maxOutChemokine();

            case macrophageBacterialDeath_ : return macrophageBacterialDeath();

            case spawnMacro_ : return spawnMacro();

            case spawnTCell_ : return spawnTCell();

            case stopSpawningMacro_ : stopSpawningMacro();

            case stopSpawningTCell_ : stopSpawningTCell();

            default : return NULL;
        }
    };

private:

    int pId;
    int chemokine;
    bool macrophage;
    // if macrophage has died and exploded bacteria out
    bool macrophageDeath;
    // current bacterial state
    bool bacteria;
    bool tCell;
    bool bloodVessel;

    // next state variables
    bool nextBacteria;
    bool createNewMacro;
    bool createNewTCell;
    int macroMoveReq;
    int tCellMoveReq;
    bool shouldMaxChemokine;

    int approvingMacroFromPid;
    int approvingTCellFromPid;

    enum Spawn{macroSpawn, tCellSpawn, noSpawn};

    // State functions -------------------------------------------------------------------------------------------
    void *init(void *placesToSpawnMacro);

    void *broadcastBacteria();

    void *checkNeighborBacteria(void *day);

    void *decayChemokineAndGrowBacteria();

    void *cellRecruitment(void *day);

    void *broadcastChemokineLevel();
    // Macrophage movement
    void *sendMacroMoveReqs();

    void *approveMacroMove();

    void *broadcastMacroApproval();

    void *macrophageMovement();
    // TCell movement
    void *sendTCellMoveReqs();

    void *approveTCellMove();

    void *broadcastTCellApproval();

    void *tCellMovement();

    void *receiveMaxedChemokineMsgs();

    void *updateChemokineAfterMsg();

    void *spreadBacteriaFromMacroDeaths();

    void *receiveBacteriaFromMacroDeaths();


    // Called by macrophage and t-cell functions
    void *findNewPlace();
    void *macroRequestMove(void *argument);
    void *tCellRequestMove(void *argument);

    void *setMacroFalse() {
        macrophage = false;
        return NULL;
    }

    void *setMacroTrue() {
        macrophage = true;
//        //testing
//        convert.str("");
//        convert << "Macro on place " << pId << endl;
//        MASS_base::log(convert.str());
        return NULL;
    }

    void *setTCellFalse() {
        tCell = false;
        return NULL;
    }

    void *setTCellTrue() {
        tCell = true;
        return NULL;
    }

    void *hasTCell() {
        return (void *) &tCell;
    }

    void *hasBacteria() {
        return (void *) &bacteria;
    }

    void *eatBacteria() {
        bacteria = false;
        return NULL;
    }

    void *maxOutChemokine() {
        chemokine = maxChemokine;
        return NULL;
    }

    void *macrophageBacterialDeath() {
        macrophageDeath = true;
        return NULL;
    }

    void *spawnMacro() {
        return (void *) &createNewMacro;
    }

    void *spawnTCell() {
        return (void *) &createNewTCell;
    }

    void *stopSpawningMacro() {
        createNewMacro = false;
        return NULL;
    }

    void *stopSpawningTCell() {
        createNewTCell = false;
        return NULL;
    }

    void *getRemotePlaceOutMessage(void *placeRelativeIndex);

    // Helper functions
    Spawn spawnImmuneCell(int today);
    bool isInitBacteriaSpawnPt();
    int pickOneNeighborMoveReq();

    // Test functions
    ostringstream convert;

    // - Should only be used after macrophages have been initialized and before the tcell spawner is created and
    //   initialized
    // - Should have spawnPtNum error messages from invisible macrophage spawner agents
    void *printHasMacroCorrectly(){
        if ((macrophage && agents.empty()) || (!macrophage && !agents.empty())) {
            convert.str("");
            convert << pId << " has a macrophage error :( " << endl;
            MASS_base::log(convert.str());
        }
        return NULL;
    }

    void *writeStateToXMLFile();
};

#endif //TB_PLACE_H
