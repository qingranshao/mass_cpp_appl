//
// Created by sarah
//

#include "TCell.h"
#include "MASS_base.h"		// for logging
#include "TB_Place.h"
#include <sstream>			// ostringstream

extern "C" Agent* instantiate( void *argument ) {
    return new TCell( argument );
}

extern "C" void destroy( Agent *object ) {
    delete object;
}

// only called on spawners - migrates them to the blood vessels
void *TCell::init() {
    isSpawner = true;
    isInitialized = true;
    requestedPlace = -1;
    vector<int> vessels = bloodVessels(place->size[0]);

    addMigrationData();
    migrate(toXY(vessels[agentId], place->size[0]));
    return NULL;
}

void *TCell::requestMove() {
    if (!isSpawner) {
        int *reqPlacePtr = (int*)place->callMethod(TB_Place::findNewPlace_, (void*) defaultMSG);
        requestedPlace = *reqPlacePtr;
        place->callMethod(TB_Place::tCellRequestMove_, &requestedPlace);
    }
    return NULL;
}

// must call manage all after this function
void *TCell::move() {
    if (!isSpawner) {
        // save data to carry along with Agent as it moves
        modifyMigrationData();

        int currPid = toId(index[0], index[1], place->size[0]);
        // check myNeighbors for approval to move there
        for (int i = 0; i < mooreNeighborNum; i++) {
            int *approvedPidPtr = (int *) place->callMethod(TB_Place::getRemotePlaceOutMessage_, myNeighbors[i]);
            int approvedPid = (approvedPidPtr != NULL) ? *approvedPidPtr : -1;

            if (approvedPid == currPid) {
                // remove self form current Place
                place->callMethod(TB_Place::setTCellFalse_, (void *) defaultMSG);
                vector<int> newXandY = toXY(requestedPlace, place->size[0]);
                migrate(newXandY);
                break;
            }
        }
        // reset requested Place regardless if move happened or not
        requestedPlace = -1;
    }
    return NULL;
}

void *TCell::addMigrationData() {
    migratableDataSize = sizeof(struct tCell_migratable_data);
    migratableData = malloc(migratableDataSize);

    modifyMigrationData();
    return NULL;
}

void *TCell::modifyMigrationData() {
    ((struct tCell_migratable_data *)migratableData)->_isSpawner = isSpawner;
    ((struct tCell_migratable_data *)migratableData)->_isInitialized = isInitialized;

    ((struct tCell_migratable_data *)migratableData)->_requestedPlace = requestedPlace;
    return NULL;
}

void *TCell::arrival() {
    if (!isSpawner) {
        // set arrival on current Place, regardless if moved or not
        place->callMethod(TB_Place::setTCellTrue_, (void *) defaultMSG);
    }
    return NULL;
}

void *TCell::spawnTCell() {
    // only spawners do this
    if (isSpawner) {
        bool shouldSpawn = *(bool *) place->callMethod(TB_Place::spawnTCell_, (void *) defaultMSG);
        if (shouldSpawn) {
            spawn(1, emptyVec, vectorSizeOf(emptyVec));
            place->callMethod(TB_Place::stopSpawningTCell_, (void *) defaultMSG);
        }
    }
    return NULL;
}

void *TCell::newTCellInit() {
    if (!isInitialized) {
        // set up agent variables
        isSpawner = false;
        isInitialized = true;
        requestedPlace = -1;
        // add state data to migratabData pointer
	addMigrationData();
    }
    return NULL;
}

void * TCell::writeTCellToXML() {
    convert.str("");
    convert << "\n<xagent>";
    convert << "\n<name>TCell</name>";
    convert << "\n<t_x>" << index[0]  << "</t_x>";
    convert << "\n<t_y>" << index[1]  << "</t_y>";
    convert << "\n</xagent>";
    MASS_base::log(convert.str());
    return NULL;
}
