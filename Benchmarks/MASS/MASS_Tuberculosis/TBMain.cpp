#include <iostream>
#include <algorithm>
#include "Timer.h"
#include "MASS.h"

#include "TBConstants.h"
#include "TB_Place.h"
#include "Macrophage.h"
#include "TCell.h"


using namespace std;

// static variables to help with initialization
static int macroSpawn[initMacroNum];

//************************************************* Helper Functions ***************************************************

/**
 * - assumes n total number of places
 * - assigns place ids that are initialized with macrophages to static array macroSpawn
 *
 * @param n - length of a side of the simulation space
 * @return - nothing
 */
void spawnInitialMacrophages(int n) {
    vector<int> shufflePlaces;
    for (int i = 0; i < n; i++) {
        shufflePlaces.push_back(i);
    }
    random_shuffle(shufflePlaces.begin(), shufflePlaces.end());

    vector<int> retVec(&shufflePlaces[0], &shufflePlaces[initMacroNum]);

    // copy to static array macroSpawn
    for (int i = 0; i < initMacroNum; i++) {
        macroSpawn[i] = retVec[i];
    }
}

//************************************************* MAIN ***********************************************$

int main(int argc, char *argv[]) {

    // Initialize Simulation

    if (argc != 9) {
        cerr << "Usage: ./main username password machinefile port nProc numThreads numTurns simSize"
             << endl;
        return -1;
    }

    char *arguments[4];
    arguments[0] = argv[1]; // username
    cout << "Username " << argv[1] << endl;
    arguments[1] = argv[2]; // password
    cout << "Password... "<< endl;
    arguments[2] = argv[3]; // machinefile
    cout << "machinefile " << argv[3] << endl;
    arguments[3] = argv[4]; // port
    cout << "port" << argv[4] << endl;
    int nProc = atoi(argv[5]);
    cout << "number of processes = " << argv[5] << endl;
    int numThreads = atoi(argv[6]);
    cout << "threads " << argv[6] << endl;
    int numTurns = atoi(argv[7]);
    int simSize = atoi(argv[8]);     // simulation space

    Timer timer;

    MASS::init(arguments, nProc, numThreads);

    //********************************* Initialization of Simulation Space *********************************************

    // initialize places

    char *msg = (char *) defaultMSG;

    // create places and initialize with starting population of bacteria
    // - simulation space is a square grid
    Places *places = new Places(handleTB_Place, "TB_Place", 1, msg,
                                sizeof(defaultMSG), 2, simSize, simSize);

    // initialize macroSpawn with place id's
    spawnInitialMacrophages(simSize * simSize);

    //debug
    cout << "Size of macroSpawn vector = " << sizeof(macroSpawn) / sizeof(macroSpawn[0]) << endl;

    cout << "What's in the macroSpawn vector : ";
    for (int i = 0; i < sizeof(macroSpawn) / sizeof(macroSpawn[0]); i++) {
        cout << " {" << macroSpawn[i] << "} ";
    }

    cout << endl;

    places->callAll(TB_Place::init_, (void *) macroSpawn, initMacroNum*sizeof(int));

    cout << "Places created and initialized" << endl;

    // create initial population of macrophages
    char *agentMsg = (char *) ("agentsCreation\0");

    cout << "Creating {" << initMacroNum << "} agents now..." << endl;

    // agent injection - create spawnPtNum invisible queen spawner macrophages (responsible for spawning
    // macrophages during future iterations - do not die, change state, or take up space)
    Agents *macrophages = new Agents(handleMacro, "Macrophage", agentMsg, 16, places,
                                     initMacroNum + spawnPtNum);

    cout << "Done creating {" << initMacroNum << "} agents now..." << endl;

    // initialize all macrophages and migrate them to the all places in the placesToSpawnMacro vector
    macrophages->callAll(Macrophage::init_, (void *) macroSpawn, initMacroNum*sizeof(int));
    macrophages->manageAll();

    cout << "Created " << initMacroNum + spawnPtNum << " macrophage :)" << endl;



    // create the TCell invisible queen spawners (# spawners = # spawnPts) - will start spawning on tCellEntrance
    // iteration and migrate them to the bloodVessel places
    Agents *tCells = new Agents(handleTCell, "TCell", agentMsg, 16, places, spawnPtNum);
    tCells->callAll(TCell::init_);
    tCells->manageAll();

    // Start Timer
    timer.start();

    // Run simulation for numTurns # of time ticks
    for (int day = 0; day < numTurns; day++) {
        /** Don't write to a file when timing runs
        // writes states to XML for java display program
        std::ofstream out(to_string(day) + ".xml");
        std::streambuf *cerrbuf = std::cerr.rdbuf(); //save old buf
        std::cerr.rdbuf(out.rdbuf()); //redirect std::cerr to iteration.xml
        
        string beginXML = "<states>";
        MASS_base::log(beginXML);
        **/
        //************************** Bacterial Growth & Chemokine Decay - Time Advancement *****************************
        cout << "Iteration : " << day << endl;

        // set bacterial out_message
        places->callAll(TB_Place::broadcastBacteria_);

        // update place's shadow space
        places->exchangeBoundary();

        // check neighbor places and set next bacterial state
        places->callAll(TB_Place::checkNeighborBacteria_, &day, sizeof(day));

        // decay chemokine and grow bacteria as needed
        places->callAll(TB_Place::decayChemokineAndGrowBacteria_);


        // decide whether to spawn macrophages and/or t-cells at each spawn point at end of iteration and reserve a spot
        // for them so other immune cells won't move to the place in the meantime ( to keep consistent w/ FLAME version )
        places->callAll(TB_Place::cellRecruitment_, &day, sizeof(day));

        //************************************ Macrophage and T-cell Migration *********************************************

        // Macrophage movement

        places->callAll(TB_Place::broadcastChemokineLevel_);
        places->exchangeBoundary();

        // macrophage agents : macrophage ask their curr Place to get neighbor chemokine and  occupancy info and choose to
        // migrate to empty place with highest chemokine (using their current place as a mediator)
        macrophages->callAll(Macrophage::requestMove_);
        cout << "Macrophages requested to move" << endl; // TODO: fine to here
        places->callAll(TB_Place::sendMacroMoveReqs_);
        places->exchangeBoundary();

        // places without macrophages check to see if macrophages are requesting to move here
        places->callAll(TB_Place::approveMacroMove_);
        places->callAll(TB_Place::broadcastMacroApproval_);
        places->exchangeBoundary();

        // macrophage agents : use curr place to examine requested place's approval and migrate accordingly
        macrophages->callAll(Macrophage::move_);
        macrophages->manageAll();

        // places update internal variables to reflect change
        places->callAll(TB_Place::macrophageMovement_);

        // T-cell movement

        places->callAll(TB_Place::broadcastChemokineLevel_);
        places->exchangeBoundary();

        // t-cell agents : t-cells ask their curr Place to get occupancy and other necessary info and choose to
        // migrate to appropriate place (using their current place as a mediator)
        tCells->callAll(TCell::requestMove_);
        places->callAll(TB_Place::sendTCellMoveReqs_);
        places->exchangeBoundary();

        // places without t-cells check to see if t-cells are requesting to move here
        // , then examine myNeighbors and approve one t-cell move request by broadcasting the approved pId using this
        // places outmessage
        places->callAll(TB_Place::approveTCellMove_);
        places->callAll(TB_Place::broadcastTCellApproval_);
        places->exchangeBoundary();

        // t-cell agents : use curr place to examine requested place's approval and migrate accordingly
        tCells->callAll(TCell::move_);
        tCells->manageAll();
        // mark current place to reflect t-cell presence
        tCells->callAll(TCell::arrival_);

        // places update internal variables to reflect change
        places->callAll(TB_Place::tCellMovement_);

        //******************************* Reaction to New Presence of Macrophages and T-cells **************************

        // Macrophage agents : react to new place (change state, grow bacteria, spread chemokine, etc.)
        macrophages->callAll(Macrophage::react_, &day, sizeof(day));
        // Macrophage agents : manageAll() to update state if killed
        macrophages->manageAll();

        // places : check myNeighbors to see if chemokine should spread to the current place
        places->exchangeBoundary();
        places->callAll(TB_Place::receiveMaxedChemokineMsgs_);
        places->callAll(TB_Place::updateChemokineAfterMsg_);

        // spread bacteria from macrophage deaths if necessary
        places->callAll(TB_Place::spreadBacteriaFromMacroDeaths_);
        places->exchangeBoundary();
        places->callAll(TB_Place::receiveBacteriaFromMacroDeaths_);

        //************************************* Cell Recruitment through Blood Vessels *********************************
        // invisible macrophage spawner cells spawn new macrophages if needed
        macrophages->callAll(Macrophage::spawn_);
        macrophages->manageAll();
        macrophages->callAll(Macrophage::newMacroInit_);

        // invisible t-cell spawners create new t-cells if needed at the entry points/blood vessels
        tCells->callAll(TCell::spawn_);
        tCells->manageAll();
        tCells->callAll(TCell::newTCellInit_);

        /**
        // for display XML output (comment out when timing)
        places->callAll(TB_Place::writeStateToXMLFile_);
        macrophages->callAll(Macrophage::writeMacroToXML_);
        tCells->callAll(TCell::writeTCellToXML_);
        string endXML = "</states>";
        MASS_base::log(endXML);

        std::cerr.rdbuf(cerrbuf); //reset to standard err again
        **/

    }

    long elapsedTime = timer.lap();
    cout << "\nElapsed time using MASS with " << nProc << "processes and "
         << numThreads << "thread :: " << elapsedTime << endl;
    MASS::finish();

    return 0;
}



