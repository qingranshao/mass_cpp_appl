
#include "Neuron.h"
#include "MASS_base.h"
#include <stdlib.h>  //rand
#include <sstream>     // ostringstream
#include <fstream>

//Used to toggle output for Wave2d
//const bool printOutput = false;
 //const bool printOutput = true;

extern "C" Place* instantiate( void *argument ) {
  return new Neuron( argument );
}

extern "C" void destroy( Place *object ) {
  delete object;
}


/**
 * Initializes a Life object.
 */
void *Neuron::init( void *argument ) {
  
  //Define cardinals
  int north[2]  = {0, 1};  cardinals.push_back( north );
  int east[2] = {1, 0};  cardinals.push_back( east );
  int south[2]  = {0, -1}; cardinals.push_back( south );
  int west[2] = {-1, 0}; cardinals.push_back( west );
  int northwest[2] = { -1, 1 };  cardinals.push_back(northwest);
  int northeast[2] = { 1, 1 };  cardinals.push_back(northeast);
  int southwest[2] = { -1, -1 };  cardinals.push_back(southwest);
  int southeast[2] = { 1, -1 };  cardinals.push_back(southeast);

  for (int i = 0; i < NUM_NEIGHBORS; i++) {
     neighborStageStatus[i] = -1;
     neighborTypeStatus[i] = -1;
     neighborSynapseStatus[i] = -1;
     neighborIncomingDirection[i] = -1;
  }

  currStage = 0;
  incomingDirection = -1;


  //0 = ACTIVE, 1 = INACTIVE, 2 = NEUTRAL
  //type = (Type)(rand() % 3);
  int weight = rand() % 100;
  if (weight < 10) {
     type = ACTIVE;
  }
  else if (weight < 20) {
     type = INACTIVE;
  }
  else { //weight is less than 100
     type = NEUTRAL;
  }
  
  if (type == 0) {
     stage = VISIT1;
     neuronSynapseNum = index[0]*10 + index[1];
  }
  else {
     stage = INDEF;
     neuronSynapseNum = -1;
  }

  outMessage = NULL;
  outMessage_size = 0;
  inMessage_size = sizeof(int);  // defines the size of the inMessagge

  return NULL;
}

  const int Neuron::neighbor[8][2] = { { -1,1 },{ 0,1 },{ 1,1 },{ -1,0 },{ 1,0 },{ -1,-1 },{ 0,-1 },{ 1,-1 } };



/** (void)																#Visual logging confirmed 160710
*	Record the outmessage of neighbors as healthstatus
*	pre: neighborHealthStatus is initialized and contains 4 -1s
*/
void *Neuron::sendSynapses()
{
   outMessage_size = sizeof(int);
   outMessage = new int();
   *(int *)outMessage = neuronSynapseNum;

   currStage = !currStage;

   return NULL;
}

/** (void)
*	Record inmessage data into neighborHealthStatus
*	@pre:	Inmessage must contain integers
*/
/*
*/
void *Neuron::computeSynapses()
{  

   //Record neighbor population as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, northwest);
   neighborSynapseStatus[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, North);
   neighborSynapseStatus[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborSynapseStatus[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborSynapseStatus[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborSynapseStatus[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborSynapseStatus[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborSynapseStatus[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborSynapseStatus[7] = (ptr == NULL) ? 0 : *ptr;

   int direction = -1;
   int randStartDir = rand() % 8;
   for (int i = 0; i < 8; i++) {
      if (neighborSynapseStatus[(randStartDir + i) % 8] != -1) {
         if ((neighborTypeStatus[(randStartDir + i) % 8] == NEUTRAL && neighborIncomingDirection[(randStartDir + i) % 8] == (randStartDir + i) % 8) || neighborTypeStatus[(randStartDir + i) % 8] == ACTIVE) {
            incomingDirection = (randStartDir + i) % 8;
            direction = (randStartDir + i) % 8;
            break;
         }
      }
   }

   if (direction != -1) {
      if (type == ACTIVE) {
         //growActive
         if (stage != ENACTED) {
            neuronSynapseNum = neighborSynapseStatus[direction];
            stage = ENACTED;
         }
      }
      else if (type == NEUTRAL) {
         //growNeutral
         //set to Stage of Neutral to currStage
         if (stage == INDEF) {
            neuronSynapseNum = neighborSynapseStatus[direction];
            stage = ENACTED;
         }
         else {

         }
      }
      else { //INACTIVE (inhibitory)
               //stop growth.
         if (stage == INDEF) {
            neuronSynapseNum = neighborSynapseStatus[direction];
            stage = STOPPED;
         }
         else {

         }
      }
   }


   //Debug logging: Expects neighbor populations
   //ostringstream convert;
   
   //THIS IS FOR CONSOLE OUTPUT (removed for graphical purposes) comment out for graphical output
   //convert << "recordNeighborHealthStatus:  Life[" << index[0] << "][" << index[1] << "]'s next generation position = " << stage << ", north = " << neighborStageStatus[0] << ", east = " << neighborStageStatus[1] << ", south = " << neighborStageStatus[2] << ", west = " << neighborStageStatus[3] << ", northwest = " << neighborStageStatus[4] << ", northeast = " << neighborStageStatus[5] << ", southwest = " << neighborStageStatus[6] << ", southeast = " << neighborStageStatus[7];
   
   //MASS_base::log(convert.str());
   

   //this is for graphical output (for synapseNum)
   ostringstream convert;
   if (stage == ENACTED) {
      convert << neuronSynapseNum;
   }
   else if (stage == STOPPED) {
      convert << 20;
   }
   else {
      convert << 0;
   }
   MASS_base::log(convert.str());

   return NULL;
}

/** (void)
*	Set the outmessage as health of this life
*/
void *Neuron::displayStageStatus()
{
   outMessage_size = sizeof(int);
   outMessage = new int();
   *(int *)outMessage = (int)stage;

   //Debug logging: Expected 400 or nxn number of lives have a single agent on them
   ostringstream convert;
   
   //this is for standard console output
   //convert << "displayStageAsOut:  Neuron[" << index[0] << "][" << index[1] << "] has the stage of " << *((int *)outMessage);
   
   //this is for graphical output (for stageStatus)
   //convert << *((int *)outMessage);
   //MASS_base::log( convert.str( ) );

   return NULL;
}

/** (void)
*	Set the outmessage as health of this life
*/
void *Neuron::displayTypeStatus()
{
   outMessage_size = sizeof(int);
   outMessage = new int();
   *(int *)outMessage = (int)type;

   //Debug logging: Expected 400 or nxn number of lives have a single agent on them
   //ostringstream convert;

   //this is for standard console output
   //convert << "displayTypeAsOut:  Neuron[" << index[0] << "][" << index[1] << "] has the type of " << *((int *)outMessage);
   //MASS_base::log(convert.str());

   return NULL;
}


/** (void)
*	Set the outmessage as health of this life
*/
void *Neuron::displayIncomingDirectionStatus()
{
   outMessage_size = sizeof(int);
   outMessage = new int();
   *(int *)outMessage = (int)incomingDirection;

   //Debug logging: Expected 400 or nxn number of lives have a single agent on them
   //ostringstream convert;

   //this is for standard console output
   //convert << "displayTypeAsOut:  Neuron[" << index[0] << "][" << index[1] << "] has the direction of " << *((int *)outMessage);
   //MASS_base::log(convert.str());

   return NULL;
}

/** (void)																#Visual logging confirmed 160710
*	Record the outmessage of neighbors as healthstatus
*	pre: neighborHealthStatus is initialized and contains 4 -1s
*/
void *Neuron::getBoundaryStageStatus()
{
   //Record neighbor population as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, northwest);
   neighborStageStatus[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, North);
   neighborStageStatus[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborStageStatus[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborStageStatus[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborStageStatus[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborStageStatus[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborStageStatus[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborStageStatus[7] = (ptr == NULL) ? 0 : *ptr;


   return NULL;
}

/** (void)																#Visual logging confirmed 160710
*	Record the outmessage of neighbors as healthstatus
*	pre: neighborHealthStatus is initialized and contains 4 -1s
*/
void *Neuron::getBoundaryTypeStatus()
{
   //Record neighbor status as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, northwest);
   neighborTypeStatus[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, North);
   neighborTypeStatus[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborTypeStatus[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborTypeStatus[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborTypeStatus[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborTypeStatus[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborTypeStatus[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborTypeStatus[7] = (ptr == NULL) ? 0 : *ptr;

   return NULL;
}

/** (void)																#Visual logging confirmed 160710
*	Record the outmessage of neighbors as healthstatus
*	pre: neighborHealthStatus is initialized and contains 4 -1s
*/
void *Neuron::getBoundaryIncomingDirectionStatus()
{
   //Record neighbor population as before
   int North[2] = { 0, 1 };
   int East[2] = { 1, 0 };
   int South[2] = { 0, -1 };
   int West[2] = { -1, 0 };
   int northwest[2] = { -1, 1 };
   int northeast[2] = { 1, 1 };
   int southwest[2] = { -1, -1 };
   int southeast[2] = { 1, -1 };

   int *ptr = (int *)getOutMessage(1, northwest);
   neighborIncomingDirection[0] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, North);
   neighborIncomingDirection[1] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, northeast);
   neighborIncomingDirection[2] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, West);
   neighborIncomingDirection[3] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, East);
   neighborIncomingDirection[4] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southwest);
   neighborIncomingDirection[5] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, South);
   neighborIncomingDirection[6] = (ptr == NULL) ? 0 : *ptr;
   ptr = (int *)getOutMessage(1, southeast);
   neighborIncomingDirection[7] = (ptr == NULL) ? 0 : *ptr;

   return NULL;
}